var gulp = require('gulp');

gulp.task('default', function () {
    console.log('Starting to build application!');
});

var paths = {
    general_assets: './app/Resources/assets',
    components: {
        bootstrap: './web/components/bootstrap-sass-official',
        font_awesome: './web/components/components-font-awesome',
        jquery: './web/components/jquery',
        select2: './web/components/select2',
        moment: './web/components/moment',
        datatables: './web/components/datatables'
    },
    bundle: {
        front: './web/bundles/mbagrovssparserfrontendfrontend/sass',
        fosjsrouting: './web/bundles/fosjsrouting'
    },
    dst: {
        css: './web/css/',
        fonts: './web/fonts/',
        js: './web/js/',
        img: './web/img/',
        imgBg: './web/img/bg'
    },
    watch: [
        'MBagrov/SSParser/Frontend/FrontendBundle',
        'MBagrov/SSParser/Common/CommonBundle',
        'MBagrov/SSParser/Estate/FlatEstateBundle',
    ]
};

var sass = sass = require('gulp-sass');
var copy = require('gulp-copy');
var minify = require('gulp-minify-css');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var exec = require('child_process').exec;
var clean = require('gulp-clean');
var obfuscate = require('gulp-obfuscate');

gulp.task('installAssets', function () {
    exec('php app/console assets:install', logStdOutAndErr);
});

var logStdOutAndErr = function (err, stdout, stderr) {
    console.log(stdout + stderr);
};

/**
 * Builds master.css of frontend bundle.
 * master.css includes all user scss files, so all custom styles are built here.
 */
gulp.task('app-sass', ['installAssets'], function () {
    return gulp.src(paths.bundle.front + '/master.scss')
        .pipe(sass({sourceComments: 'map'}))
        .pipe(gulp.dest(paths.dst.css));
});

/**
 * Builds font awesome style from scss.
 */
gulp.task('fa-sass', function () {
    return gulp.src(paths.components.font_awesome + '/scss/font-awesome.scss')
        .pipe(sass({sourceComments: 'map'}))
        .pipe(gulp.dest(paths.dst.css));
});

/**
 * Concatenates all third party css styles into single file and minify it.
 */
gulp.task('compose-css-lte', ['fa-sass'], function () {
    return gulp.src([
            paths.dst.css + 'font-awesome.css',
            paths.general_assets + '/css/headers/header-default.css',
            paths.general_assets + '/css/style.css',
            paths.general_assets + '/plugins/animate.css',
            paths.general_assets + '/plugins/line-icons/line-icons.css',
            paths.general_assets + '/plugins/parallax-slider/css/parallax-slider.css',

            paths.general_assets + '/plugins/owl-carousel/owl-carousel/owl.carousel.css',

            paths.general_assets + '/plugins/sky-forms-pro/skyforms/css/sky-forms.css',
            paths.general_assets + '/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css',
            paths.general_assets + '/css/pages/page_log_reg_v2.css',
            paths.general_assets + '/css/theme-colors/blue.css',
            paths.general_assets + '/css/theme-skins/dark.css',
            paths.general_assets + '/css/pages/portfolio-v1.css',
            paths.general_assets + '/css/pages/profile.css',
            paths.components.select2 + '/dist/css/select2.css',
            paths.general_assets + '/css/pages/page_404_error2.css',
            paths.general_assets + '/css/footers/footer-v2.css',
            //paths.components.datatables + '/media/css/jquery.dataTables.css'

        ])
        .pipe(concatCss("lte.min.css"))
        .pipe(minify({keepSpecialComments: 0}))
        .pipe(gulp.dest(paths.dst.css));
});

/**
 * Concatenates all application css styles in single file and minify it.
 */
gulp.task('compose-app-css', ['installAssets', 'app-sass'], function () {
    return gulp.src([
            paths.dst.css + 'master.css',
        ])
        .pipe(concatCss("app.min.css"))
        .pipe(minify({keepSpecialComments: 0}))
        .pipe(gulp.dest(paths.dst.css));
});

/**
 * Deletes all application css styles which was been concatenated into one file.
 */
gulp.task('clear-app-css', ['installAssets', 'app-sass', 'compose-app-css'], function () {
    gulp.src(paths.dst.css + 'master.css', {read: false}).pipe(clean({force: true}));
});

/**
 * Deletes all third party css styles which was been concatenated into one file.
 */
gulp.task('clear-lte-css', ['fa-sass', 'compose-css-lte'], function () {
    gulp.src(paths.dst.css + 'font-awesome.css', {read: false}).pipe(clean({force: true}));
});

/**
 * Copies all necessary fonts into fonts directory
 */
gulp.task('fonts', function () {
    return gulp.src([
            paths.components.bootstrap + '/assets/fonts/bootstrap/*',
            paths.components.font_awesome + '/fonts/*'
        ])
        .pipe(copy(paths.dst.fonts, {prefix: 7}));
});

gulp.task('images-copy', function () {
    return gulp.src([
            //paths.general_assets + '/img/themes/logo1-blue.png',
            paths.general_assets + '/plugins/leaflet/img/*.png',
            paths.general_assets + '/plugins/parallax-slider/img/arrows.png',
            paths.general_assets + '/plugins/parallax-slider/img/bg-text.png',
            paths.general_assets + '/img/blur/img2.jpg',
            paths.general_assets + '/img/icons/social/facebook.png',
            paths.general_assets + '/img/icons/social/googleplus.png',
        ])
        .pipe(copy(paths.dst.img, {prefix: 7}));
});

gulp.task('images-bg-copy', function () {
    return gulp.src([
            paths.general_assets + '/img/bg/index_slider.jpg',
            paths.general_assets + '/img/bg/2.jpg',
        ])
        .pipe(copy(paths.dst.imgBg, {prefix: 7}));
});

gulp.task('leaflet-images-copy', function () {
    return gulp.src([
            paths.general_assets + '/plugins/leaflet/css/images/*.png'
        ])
        .pipe(copy(paths.dst.css + 'images/', {prefix: 7}));
});

gulp.task('js-copy', function () {
    return gulp.src([
            paths.general_assets + '/plugins/leaflet/js/leaflet.js'
        ])
        .pipe(copy(paths.dst.js, {prefix: 7}));
});

gulp.task('css-copy', function () {
    return gulp.src([
            paths.general_assets + '/plugins/leaflet/css/leaflet.css'
        ])
        .pipe(copy(paths.dst.css, {prefix: 7}));
});

/**
 * Concatenates all third party js files into one and minify it.
 */
gulp.task('app-lte-js', function () {
    return gulp.src([
            //paths.components.jquery + '/dist/jquery.js',
            //paths.components.bootstrap + '/assets/javascripts/bootstrap/*.js',

            paths.general_assets + '/plugins/jquery/jquery.min.js',
            paths.general_assets + '/plugins/jquery/jquery-migrate.min.js',
            paths.general_assets + '/plugins/bootstrap/js/bootstrap.min.js',

            paths.general_assets + '/plugins/jquery.easing.min.js',


            paths.components.select2 + '/dist/js/select2.full.js',
            paths.components.datatables + '/media/js/jquery.dataTables.js',
            paths.general_assets + '/plugins/back-to-top.js',
            paths.general_assets + '/plugins/smoothScroll.js',
            paths.general_assets + '/plugins/layer-slider/layerslider/js/greensock.js',
            paths.general_assets + '/plugins/layer-slider/layerslider/js/layerslider.transitions.js',
            paths.general_assets + '/plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js',
            //paths.general_assets + '/plugins/parallax-slider/js/modernizr.js',
            //paths.general_assets + '/plugins/parallax-slider/js/jquery.cslider.js',
            paths.general_assets + '/plugins/owl-carousel/owl-carousel/owl.carousel.js',
            paths.general_assets + '/plugins/backstretch/jquery.backstretch.min.js',
            paths.general_assets + '/js/app.js',
            paths.general_assets + '/js/plugins/owl-carousel.js',
            //paths.general_assets + '/js/plugins/parallax-slider.js',

            paths.general_assets + '/plugins/counter/waypoints.min.js',
            paths.general_assets + '/plugins/counter/jquery.counterup.min.js',
            paths.general_assets + '/plugins/circles-master/circles.js',
            paths.general_assets + '/plugins/parallax-slider/js/jquery.cslider.js',

            paths.general_assets + '/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js',
            paths.general_assets + '/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js',
            paths.general_assets + '/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js',
            paths.general_assets + '/js/plugins/form-sliders.js',
            paths.bundle.fosjsrouting + '/js/router.js',
            paths.general_assets + '/plugins/jquery/jquery-ui-slider.js',
            paths.general_assets + '/plugins/jquery/jquery.ui.touch-punch.min.js',
        ])
        .pipe(concat('app-lte.min.js'))
        // .pipe(uglify())
        //.pipe(obfuscate())
        .pipe(gulp.dest(paths.dst.js));
});


/**
 * Concatenates all application js files, located in bundles into one and minify it.
 */
gulp.task('app-js', function () {
    return gulp.src([
            './web/bundles/*/js/**/*.js',
            './web/bundles/*/js/*.js'
        ])
        .pipe(concat('app.min.js'))
        // .pipe(uglify())
        //.pipe(obfuscate())
        .pipe(gulp.dest(paths.dst.js));
});


/**
 * Watch for file changes and rebuild application scss and js.
 */
gulp.task('watch', function () {
    var onChange = function (event) {
        console.log('File ' + event.path + ' has been ' + event.type);
    };

    for (var i = 0; i < paths.watch.length; i++) {
        gulp.watch('./src/' + paths.watch[i] + '/Resources/public/sass/**/*.scss', ['app-sass', 'compose-app-css', 'clear-app-css'])
            .on('change', onChange);
        gulp.watch('./src/' + paths.watch[i] + '/Resources/public/js/**/*.js', ['app-js'])
            .on('change', onChange);
    }

});

gulp.task('install', [
    'installAssets',
    'app-sass',
    'fa-sass',
    'compose-css-lte',
    'compose-app-css',
    'clear-app-css',
    'clear-lte-css',
    'fonts',
    'app-lte-js',
    'app-js',
    'images-copy',
    'images-bg-copy',
    'css-copy',
    'js-copy',
    'leaflet-images-copy'
], function () {
});
