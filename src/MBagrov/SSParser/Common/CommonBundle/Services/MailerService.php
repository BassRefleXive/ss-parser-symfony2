<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


class MailerService
{
    private $mailer;
    private $config;

    public function __construct(\Swift_Mailer $mailer, array $config)
    {
        $this->mailer = $mailer;
        $this->config = $config;
    }

    public function notifyAboutPaymentFail(array $messages)
    {
        $messageText = '';
        foreach ($messages as $title => $message) {
            $messageText .= $title . ': ' . $message . '<br>';
        }
        $this->sendMessage('Customer order finishing failed!', $this->config['email'], $this->config['email'], $messageText);
    }

    public function notifyAboutParseErrors(array $parseErrors) {
        $text = '';
        foreach($parseErrors as $parseError) {
            foreach($parseError as $error) {
                foreach($error as $title => $descr) {
                    $text .= $title . ': ' . $descr . '<br>';
                }
                $text .= '<br>';
            }
            $text .= '<br>';
            $text .= '<br>';
        }
        return $this->sendMessage('Parse errors occurred.', $this->config['email'], $this->config['email'], $text);
    }

    public function notifyAboutLocationsDiscovery($title, $message) {
        return $this->sendMessage($title, $this->config['email'], $this->config['email'], $message);
    }

    public function sendMessage($subject, $from, $to, $text)
    {

        $message = \Swift_Message::newInstance();
        $message->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($text, 'text/html');

        return $this->mailer->send($message);

    }
}