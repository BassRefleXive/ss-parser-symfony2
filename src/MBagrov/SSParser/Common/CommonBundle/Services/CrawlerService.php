<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


use GuzzleHttp\Client;
use MBagrov\SSParser\Common\CommonBundle\Interfaces\AdInterface;
use Symfony\Component\DomCrawler\Crawler;

abstract class CrawlerService
{

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    protected $client;
    protected $crawler;
    protected $torEndpoint;

    public function __construct(string $torEndpoint)
    {
        $this->torEndpoint = $torEndpoint;

        $this->client = new Client();
        $this->crawler = new Crawler();
    }

    public function getResourceContents($url, $method = self::METHOD_GET)
    {
        $request = $this->client->createRequest($method, $url, [
            'proxy' => $this->torEndpoint,
        ]);
        return $this->client->send($request)->getBody()->getContents();

    }

    protected function getSsUniqueId($url, \DateTime $createdAt) {
        $urlParts = explode('/', $url);
        $lastPath = array_pop($urlParts);
        $ssId = (string)explode('.', $lastPath)[0];

        return md5($ssId . $createdAt->getTimestamp());
    }

    abstract public function parse($content, AdInterface $ad = null);

    abstract public function getContentHash(AdInterface $ad);

}