<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use AppBundle\Entity\Locale;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\FacebookUser;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\GoogleUser;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\SocialUser;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class OAuthUserProvider
 * @package AppBundle\Security\Core\User
 */
class OAuthUserProvider extends BaseClass
{

    private $defaultLocale;
    private $registrationConfig;

    public function __construct(UserManagerInterface $userManager, array $properties, string $defaultLocale, array $registrationConfig)
    {
        parent::__construct($userManager, $properties);

        $this->defaultLocale = $defaultLocale;
        $this->registrationConfig = $registrationConfig;

    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $socialID = $response->getUsername();

        /** @var UserManager $userManager */
        $userManager = $this->userManager;

        /** @var User $user */
        $user = $userManager->findUserBySocialId($this->getProperty($response), $socialID);
        $email = $response->getEmail();

        if (null === $user) {
            $user = $this->userManager->findUserByEmail($email);

            if (null === $user || !$user instanceof UserInterface) {
                $user = $this->userManager->createUser();
                $user->setEmail($email);
                $user->setPlainPassword(md5(uniqid()));
                $user->setEnabled(true);
                $user->setLocale($this->defaultLocale);
                $user->setEmailNotifications($this->registrationConfig['free_email']);
                $user->setPhoneNotifications($this->registrationConfig['free_sms']);
            }

            $service = $response->getResourceOwner()->getName();

            switch ($service) {
                case 'google':
                    $user->setGoogleUser($this->buildSocialUser(new GoogleUser(), $response, $user));
                    break;
                case 'facebook':
                    $user->setFacebookUser($this->buildSocialUser(new FacebookUser(), $response, $user));
                    break;
            }
            $this->userManager->updateUser($user);
        }

        $checker = new UserChecker();
        $checker->checkPreAuth($user);

        return $user;
    }

    private function buildSocialUser(SocialUser $socialUser, UserResponseInterface $response, User $user): SocialUser
    {
        return $socialUser
            ->setSocialId($response->getUsername())
            ->setName($response->getRealName())
            ->setAvatar($response->getProfilePicture())
            ->setUser($user);
    }
}