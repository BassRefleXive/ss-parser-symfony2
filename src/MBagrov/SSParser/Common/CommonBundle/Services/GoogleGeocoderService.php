<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use GuzzleHttp\Client;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\GeocoderException;
use MBagrov\SSParser\Common\CommonBundle\Utils\Transliterator;

class GoogleGeocoderService extends GeocoderService
{
    private $config;
    private $client;
    private $notifierService;

    public function __construct(SentryNotifierService $notifierService, Client $client, array $config)
    {
        $this->config = $config;
        $this->client = $client;
        $this->notifierService = $notifierService;
    }

    public function getCoordinates(string $city, string $address, string $country, bool $logZeroResults = false)
    {
        if ($this->config['dummy_data']) {
            return [
                'lat' => round(mt_rand(-90, 90) * (mt_rand() / mt_getrandmax()), 5),
                'lng' => round(mt_rand(-180, 180) * (mt_rand() / mt_getrandmax()), 5),
            ];
        }
        $address = Transliterator::transliterate(implode(', ', [
            $address,
            $city,
            $country,
        ]));

        $address = $this->sanitizeAddress($address);

        $requestUri = $this->buildUri($address);
        $request = $this->client->createRequest('GET', $requestUri);
        $contents = $this->client->send($request)->getBody()->getContents();
        $result = [
            'lat' => 0,
            'lng' => 0,
        ];
        if (($data = json_decode($contents, true)) === null) {
            return $result;
        }

        return $this->processGeocoderResponse($data, $address, $requestUri, $logZeroResults);

    }

    private function processGeocoderResponse(array $response, $requestAddress, $requestUri, bool $logZeroResults = false)
    {
        if (!isset($response['status'])) {
            $this->notifierService->error('STATUS fiend not received from google geocoder.');
            throw new GeocoderException('Status unknown.');
        }

        switch ($response['status']) {
            case 'OK' : {
                return $this->processNormalResponse($response);
            }
            case 'ZERO_RESULTS' : {
                $logZeroResults && $this->notifierService->error([
                    'Message'                => 'No results returned from geocoder.',
                    'address'                => $requestAddress,
                    'requestUri'             => $requestUri,
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('No results returned from geocoder.');
            }
            case 'OVER_QUERY_LIMIT' : {
                $this->notifierService->error([
                    'Message'                => 'Google geocoder query limit reached.',
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('Query limit.');
            }
            case 'REQUEST_DENIED' : {
                $this->notifierService->error([
                    'Message'                => 'Google geocoder request was denied.',
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('Request denied.');
            }
            case 'INVALID_REQUEST' : {
                $this->notifierService->error([
                    'Message'                => 'Invalid request send to google geocoder.',
                    'address'                => $requestAddress,
                    'requestUri'             => $requestUri,
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('Invalid request.');
            }
            case 'UNKNOWN_ERROR' : {
                $this->notifierService->error([
                    'Message'                => 'Something strange happened with geocoder.',
                    'address'                => $requestAddress,
                    'requestUri'             => $requestUri,
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('Unknown error.');
            }
            default: {
                $this->notifierService->error([
                    'Message'                => 'Unknown status code received from geocoder.',
                    'address'                => $requestAddress,
                    'requestUri'             => $requestUri,
                    'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
                ]);
                throw new GeocoderException('Unknown status code received.');
            }
        }

    }

    private function processNormalResponse(array $response)
    {
        if (!isset($response['results'])) {
            $this->notifierService->error([
                'Message'                => 'Google fucked up. There should be results array, but it is not present.',
                'geocoder_error_message' => isset($response['error_message']) ? $response['error_message'] : '',
            ]);
            throw new GeocoderException('Google fucked up.');
        }
        if (!isset($response['results'], $response['results'][0], $response['results'][0]['geometry'], $response['results'][0]['geometry']['location'])) {
            throw new GeocoderException('No results-0-geometry-location element present.');
        }

        return $response['results'][0]['geometry']['location'];
    }

    private function buildUri($address)
    {
        return $this->config['google']['url'] . $this->config['google']['format'] . '?' . http_build_query([
            'address' => $address,
            'key'     => $this->config['google']['key'],
            'region'  => 'LV',
        ]);
    }

    private function sanitizeAddress(string $address): string
    {
        return str_replace(' i rajon', '', $address);
    }

}