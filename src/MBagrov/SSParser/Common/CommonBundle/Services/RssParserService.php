<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


use MBagrov\SSParser\Common\CommonBundle\Interfaces\AdInterface;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\AdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use Symfony\Component\DomCrawler\Crawler;

class RssParserService extends CrawlerService
{

    public function parse($content, AdInterface $ad = null)
    {
        $ads = new AdCollection();

        $this->crawler->add($content);
        $itemNodes = $this->crawler->filterXPath('//item');
        $itemNodes->each(function (Crawler $node, $i) use ($ads) {
            $ad = new Ad();
            $additionalData = [];

            $node->children()->each(function (Crawler $node, $i) use ($ad, $additionalData) {
                switch ($node->nodeName()) {

                    case 'link': {
                        $ad->setUrl($node->text());
                        break;
                    }

                    case 'pubDate': {
                        $timeArray = explode(' ', $node->text());
                        array_pop($timeArray);
                        $time = new \DateTime(implode(' ', $timeArray));
                        $ad->setCreatedAtProvider($time);
                        break;
                    }

                }
            });

            $ad->setSsUniqId($this->getSsUniqueId($ad->getUrl(), $ad->getCreatedAtProvider()));
            $ads->add($ad);

        });
        $this->crawler->clear();

        return $ads;
    }

    public function getContentHash(AdInterface $ad)
    {
        return null;
    }

}