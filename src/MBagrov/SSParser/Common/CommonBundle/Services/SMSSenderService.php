<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;

use GuzzleHttp\Client;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateServiceException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SMSSenderService
{
    const LATVIA_PHONE_CODE = '+371';

    protected $client;

    protected $url;
    protected $action;
    protected $user;
    protected $password;
    protected $sender;

    public function __construct(array $config)
    {
        $this->client = new Client();

        $this->url = $config['url'] ?? null;
        $this->action = $config['action'] ?? null;
        $this->user = $config['user'] ?? null;
        $this->password = $config['password'] ?? null;
        $this->sender = $config['sender'] ?? null;

    }

    public function send(Notification $notification, User $user)
    {
        $request = $this->client->createRequest(Request::METHOD_GET, $this->buildUrl($notification->getText(), $user->getPhone()));

        $response = $this->client->send($request);

        return (int)$response->getStatusCode() === Response::HTTP_OK
            ? $response->getBody()->getContents()
            : false;
    }

    private function buildUrl(string $text, string $phone): string
    {
        if (in_array(null, [
            $this->url,
            $this->action,
            $this->user,
            $this->password,
            $this->sender,
        ], true)) {
            throw new FlatEstateServiceException('Not all required configuration for SMSSenderService provided.');
        }

        return $this->url . '?' . http_build_query([
            'action'   => $this->action,
            'user'     => $this->user,
            'password' => $this->password,
            'sender'   => $this->sender,
            'text'     => $text,
            'phone'    => SMSSenderService::LATVIA_PHONE_CODE . $phone,
        ]);

    }
}