<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


abstract class UrlShortenerService
{
    abstract public function getShortUrl($longUrl);
}