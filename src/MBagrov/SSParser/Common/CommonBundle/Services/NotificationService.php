<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\Notification;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\CommonException;
use MBagrov\SSParser\Estate\FlatEstateBundle\Constants\NotificationEvent;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\NotificationService as FlatEstateNotificationService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification as FlatEstateNotification;

class NotificationService
{
    private $notificationRepo;
    private $flatEstateNotificationService;
    private $apiKey;

    public function __construct(
        EntityManager $em,
        FlatEstateNotificationService $flatEstateNotificationService,
        string $apiKey
    )
    {
        $this->notificationRepo = $em->getRepository(Notification::class);
        $this->flatEstateNotificationService = $flatEstateNotificationService;
        $this->apiKey = $apiKey;
    }


    public function processEmailWebHookRequest(array $requestData)
    {
        $timestamp = $requestData['timestamp'];
        $token = $requestData['token'];
        $signature = $requestData['signature'];

        $generatedSignature = hash_hmac('sha256', $timestamp . $token, $this->apiKey);

        if ($signature !== $generatedSignature) {
            throw new CommonException('Signature mismatch.', 400);
        }

        if (!isset($requestData['notificationId'], $requestData['type'])) {
            return true;
        }

        switch ($requestData['type']) {
            case NotificationTypeEnum::NOTIFICATION_TYPE_IGNORED_ADS: {
                $this->processNotificationWebHook($requestData);
                break;
            }
            case NotificationTypeEnum::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED: {
                $this->processNotificationWebHook($requestData);
                break;
            }
            case FlatEstateNotification::TYPE_EMAIL: {
                $this->flatEstateNotificationService->processEmailWebHookRequest($requestData);
                break;
            }
            default: {
                throw new CommonException('Unknown email type provided.', 200);
            }

        }

    }

    private function processNotificationWebHook(array $requestData)
    {
        $notification = $this->notificationRepo->findOneBy(['uuid' => $requestData['notificationId']]);

        if ($notification === null) {
            throw new CommonException('Unknown notification id received from mailgun.', 400);
        }

        $event = $requestData['event'];
        $eventStatus = NotificationEvent::getEventStatus($event);

        $notificationStatus = $notification->getStatus();

        if ($notificationStatus->checkStatus($eventStatus)) {
            return true;
        }

        $notification->setStatus($notificationStatus->setStatus($eventStatus));

        $this->notificationRepo->persistAndFlush($notification);

    }

}