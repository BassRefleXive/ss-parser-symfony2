<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


class SentryNotifierService
{

    private $client;

    public function __construct(\Raven_Client $client)
    {
        $this->client = $client;
    }

    public function captureException(\Exception $exception)
    {
        $this->client->captureException($exception);
    }

    public function error($data)
    {
        $this->sendMessage($this->prepareMessage($data), \Raven_Client::ERROR);
    }

    public function info($data)
    {
        $this->sendMessage($this->prepareMessage($data), \Raven_Client::INFO);
    }

    public function warning($data)
    {
        $this->sendMessage($this->prepareMessage($data), \Raven_Client::WARNING);
    }

    private function prepareMessage($message)
    {
        $result = '';

        if (is_string($message)) {
            $result = $message;
        }

        if (is_array($message)) {
            $result = implode('; ', array_map(function ($value, $key) {
                return $key . ': ' . $value;
            }, array_values($message), array_keys($message)));
        }

        return $result;
    }

    private function sendMessage(string $message, string $level)
    {
        $this->client->captureMessage($message, [], ['level' => $level], false, null);
    }

}