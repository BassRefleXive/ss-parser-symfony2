<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;

use \FOS\UserBundle\Doctrine\UserManager as FOSUserManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\SocialUserTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\SocialUser;

class UserManager extends FOSUserManager
{

    public function findUserBySocialId(string $socialSource, string $socialId)
    {
        if (!array_key_exists($socialSource, SocialUserTypeEnum::getTypes())) {
            throw new \InvalidArgumentException('Unknown social user source.');
        }

        $socialUser = $this->objectManager->getRepository(SocialUser::class)->findByTypeAndSocialId($socialSource, $socialId);

        return $socialUser instanceof SocialUser
            ? $socialUser->getUser()
            : null;

    }

}