<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\OrderException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class OrderService
{

    const TYPE_SMS = 'sms';
    const TYPE_EMAIL = 'email';

    private $orderRepository;
    private $pricingConfig;
    private $tokenStorage;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage, array $pricingConfig)
    {
        $this->orderRepository = $em->getRepository('MBagrovSSParserCommonCommonBundle:Order');
        $this->pricingConfig = $pricingConfig;
        $this->tokenStorage = $tokenStorage;
    }

    public function create(Order $order)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $order->setUser($user)
            ->setAmount($this->getSmsTotal($order) + $this->getEmailTotal($order));
        if ($order->getAmount() < $this->pricingConfig['min_order_amount']) {
            throw new OrderException('order.create.amount.too_low');
        }
        $this->orderRepository->create($order);
    }

    public function getSmsTotal(Order $order)
    {
        return $this->getExplainedPrice($order->getSmsCount(), self::TYPE_SMS)['total'];
    }

    protected function getExplainedPrice($count, $priceType, $config = null)
    {
        $pricingConfig = $config === null ? $this->pricingConfig[$priceType] : $config[$priceType];
        $current = $pricingConfig['price']['start'];
        $total = 0;

        $explainedPrice = [];

        for ($i = 1; $i <= $count; $i++) {
            $total += $current;
            if ($i % $pricingConfig['step']['count'] === 0 && $current > $pricingConfig['price']['min']) {
                $explainedPrice[] = [
                    'current' => $current,
                    'count'   => $pricingConfig['step']['count'],
                    'total'   => $total
                ];
                $current = round($current * $pricingConfig['step']['value'], 2);
            }
        }
        $explainedPrice[] = [
            'current' => $current,
            'count'   => $count - ($pricingConfig['step']['count'] * count($explainedPrice)),
            'total'   => $total
        ];

        return [
            'explained' => $explainedPrice,
            'total'     => $total
        ];
    }

    public function getEmailTotal(Order $order)
    {
        return $this->getExplainedPrice($order->getEmailCount(), self::TYPE_EMAIL)['total'];
    }

    public function getPriceExplanation(Order $order)
    {
        return [
            'sms'   => $this->getExplainedPrice($order->getSmsCount(), self::TYPE_SMS, json_decode($order->getCalcConfig(), true)),
            'email' => $this->getExplainedPrice($order->getEmailCount(), self::TYPE_EMAIL, json_decode($order->getCalcConfig(), true))
        ];
    }

    public function getUserOrders()
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->orderRepository->findByUser($user);
    }

    public function updateStatus(Order $order, $status)
    {
        $allowedStatuses = [Order::STATUS_FAIL, Order::STATUS_SUCCESS];
        if (!in_array($status, $allowedStatuses, true)) {
            throw new OrderException('Order status invalid. Must be: ' . implode(' or ', $allowedStatuses));
        }
        $order->setStatus($status);
        $this->update($order);
    }

    public function update(Order $order)
    {
        $this->orderRepository->update($order);
    }

    /**
     * @param string $uuid
     *
     * @return Order
     * @throws OrderException
     */
    public function getByUuid($uuid)
    {
        $order = $this->orderRepository->findByUuid($uuid);
        if ($order === null) {
            throw new OrderException('Order with uuid ' . $uuid . ' not found.');
        }

        return $order;
    }

}