<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;

use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Collections\ObjectCollection;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchCollection;

class ObjectService
{
    const URL_TYPE_RSS = 0;
    const URL_TYPE_HTML = 1;

    private $objectRepository;

    public function __construct(EntityManager $em)
    {
        $this->objectRepository = $em->getRepository('MBagrovSSParserCommonCommonBundle:Object');
    }

    public function findOneById($id)
    {
        return $this->objectRepository->findOneById($id);
    }

    public function findAll()
    {
        return $this->objectRepository->findAll();
    }

    public function getChildren(CommonObject $object, bool $ignoreStatus = false)
    {
        return $this->objectRepository->findBy(array_merge(
            [
                'parentId' => $object->getId(),
            ],
            $ignoreStatus
                ? []
                : ['status' => 1]
        ));
    }

    public function update(CommonObject $object)
    {
        $this->objectRepository->update($object);
    }

    public function getUniqueCities(SearchCollection $searchCollection)
    {
        return (new ObjectCollection())->getFromSearches($searchCollection);
    }

    /**
     * @param CommonObject $object
     * @param              $type
     * @param string $url
     *
     * @return string
     */
    public function buildUrl(CommonObject $object, $type, $url = '')
    {
        /**
         * @var CommonObject $parentObject
         */
        $parentObject = $this->objectRepository->findOneById($object->getParentId());


        if (strlen($url)) {
            if ($parentObject !== null) {
                $currentUrl = $this->buildUrl($parentObject, $type, ($parentObject->getPath() . ($parentObject->getType() === 0 ? 'ru/' : '') . $url));
            } else {
                $currentUrl = $url;
            }
        } else {
            if ($parentObject !== null) {
                $currentUrl = $this->buildUrl($parentObject, $type, ($parentObject->getPath() . $object->getPath() . $url));
            } else {
                $currentUrl = $url . $object->getPath();
            }
        }

        if ($parentObject === null && $type === self::URL_TYPE_RSS) {
            $currentUrl .= 'rss/';
        }

        return $currentUrl;
    }

    public function findOneByName($name)
    {
        return $this->objectRepository->findByAnyName($name);
    }

    public function findOneByPath($path)
    {
        return $this->objectRepository->findOneBy([
            'path'     => $path,
            'parentId' => 641,
        ]);
    }

}