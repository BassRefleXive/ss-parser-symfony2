<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


class ContactsService
{

    private $mailer;
    private $config;

    public function __construct(MailerService $mailer, SentryNotifierService $notifierService, array $config)
    {
        $this->mailer = $mailer;
        $this->notifierService = $notifierService;
        $this->config = $config;
    }

    public function sendMessage($fromName, $fromEmail, $text)
    {

        $this->notifierService->info([
            '[Info]'     => 'Received new inquiry from contacts form!',
            'From'       => $fromName,
            'From Email' => $fromEmail,
            'Text'       => $text,
        ]);

        $messageText = 'From: ' . $fromName . '<br><br>From Email: ' . $fromEmail . '<br><br>Text:<br>' . $text;

        return $this->mailer->sendMessage($this->config['form']['subject'], $fromEmail, $this->config['form']['recipient'], $messageText);
    }
}