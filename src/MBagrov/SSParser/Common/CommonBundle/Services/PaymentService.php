<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Entity\Payment;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\PaymentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PaymentService
{

    private $paymentRepository;
    private $tokenStorage;
    private $orderService;
    private $paymentConfig;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage, OrderService $orderService, array $paymentConfig)
    {
        $this->paymentRepository = $em->getRepository('MBagrovSSParserCommonCommonBundle:Payment');
        $this->tokenStorage = $tokenStorage;
        $this->orderService = $orderService;
        $this->paymentConfig = $paymentConfig;
    }

    public function create(Order $order, Payment $payment)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($order->getUser() !== $user) {
            throw new PaymentException('Order not belongs to you.');
        }
        if ($order->getPayment() instanceof Payment) {
            throw new PaymentException('Payment process already started. Finish it or create new order.');
        }
        $payment->setOrder($order);
        $this->paymentRepository->create($payment);
        $order->setPayment($payment);
        $this->orderService->update($order);
    }

    public function validateReturnRequest(Order $order, array $requestData)
    {
        $secret = $this->paymentConfig['demo'] ? $this->paymentConfig['secret']['demo'] : $this->paymentConfig['secret']['live'];
        $accountId = $this->paymentConfig['demo'] ? $this->paymentConfig['account_id']['demo'] : $this->paymentConfig['account_id']['live'];
        $orderNumber = $this->paymentConfig['demo'] ? 1 : $requestData['order_number'];
        if (mb_strtoupper(md5($secret . $accountId . $orderNumber . number_format($order->getAmount(), 2, '.', ''))) !== $requestData['key']) {
            throw new PaymentException('Error checking data consistency. Probably data was modified.');
        }
    }

    public function updateSuccessfulPayment(Payment $payment, $requestData)
    {
        $payment->setOrderNumber($requestData['order_number'])
            ->setInvoiceId($requestData['invoice_id']);
        $this->paymentRepository->update($payment);
    }

}