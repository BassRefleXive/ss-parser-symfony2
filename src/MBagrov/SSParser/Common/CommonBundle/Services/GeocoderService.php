<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;

use MBagrov\SSParser\Common\CommonBundle\Entity\Object;

abstract class GeocoderService
{
    abstract public function getCoordinates(string $city, string $address, string $country, bool $logZeroResults = false);
}