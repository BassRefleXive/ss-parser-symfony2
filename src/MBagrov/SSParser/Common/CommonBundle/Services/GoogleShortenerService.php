<?php


namespace MBagrov\SSParser\Common\CommonBundle\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Message\RequestInterface;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\UrlShortenerExteption;

class GoogleShortenerService extends UrlShortenerService
{
    private $config;
    private $client;
    private $notifierService;

    public function __construct(SentryNotifierService $notifierService, Client $client, array $config)
    {
        $this->config = $config;
        $this->client = $client;
        $this->notifierService = $notifierService;
    }

    public function getShortUrl($longUrl)
    {
        if ($this->config['dummy_data']) {
            return 'https://dummy.url.io/' . str_shuffle('aAbBcCdDeEfFhHiIjJ');
        }
        $request = $this->client->createRequest('POST', $this->buildUri(), [
            'headers' => $this->buildRequestHeaders(),
            'body'    => $this->buildRequestBody($longUrl),
        ]);
        $contents = $this->client->send($request)->getBody()->getContents();

        if (($data = json_decode($contents, true)) === null) {
            $this->notifierService->error([
                'Message'  => 'Google url shortener response is not json.',
                'response' => $contents,
            ]);
            throw new UrlShortenerExteption('Google response is not json.');
        }

        return $this->processResponse($data, $request);
    }

    private function buildUri()
    {
        return $this->config['google']['url'] . '?' . http_build_query([
            'key' => $this->config['google']['key'],
        ]);
    }

    private function buildRequestHeaders()
    {
        return [
            'Content-Type' => 'application/json',
        ];
    }

    private function buildRequestBody($longUri)
    {
        return json_encode([
            'longUrl' => $longUri,
        ]);
    }

    private function processResponse(array $data, RequestInterface $request)
    {
        if (isset($data['error'])) {
            $this->processErrorResponse($data, $request);
        } elseif (isset($data['status'])) {
            switch ($data['status']) {
                case 'OK' : {
                    return $this->processNormalResponse($data);
                }
                default: {
                    $this->notifierService->error([
                        'Message'         => 'Unknown status code received from google url shortener.',
                        'data_received'   => json_encode($data),
                        'request_url'     => $request->getUrl(),
                        'request_body'    => $request->getBody(),
                        'request_headers' => json_encode($request->getHeaders()),
                    ]);
                    throw new UrlShortenerExteption('Unknown status code received.');
                }
            }
        } else {
            return $this->processNormalResponse($data);
        }
    }

    private function processErrorResponse(array $data, RequestInterface $request)
    {
        $messageParts = ['Message' => 'Error response received from google url shortener.'];
        isset($data['code']) && ($messageParts[] = 'Code: ' . $data['code']);
        isset($data['message']) && ($messageParts[] = 'Message: ' . $data['message']);
        isset($data['errors']) && ($messageParts[] = 'Errors: ' . json_encode($data['errors']));

        $this->notifierService->error($messageParts);

        throw new UrlShortenerExteption('Error response received.');
    }

    private function processNormalResponse(array $data)
    {
        if (!isset($data['id'])) {
            $this->notifierService->error([
                'Message'  => 'Google fucked up. No short url provided.',
                'response' => $data,
            ]);
            throw new UrlShortenerExteption('Google fucked up.');
        }

        return $data['id'];
    }

}