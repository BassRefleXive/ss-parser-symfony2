<?php

namespace MBagrov\SSParser\Common\CommonBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;

class UserService
{

    private $userRepository;

    public function __construct(EntityManager $em)
    {
        $this->userRepository = $em->getRepository('MBagrovSSParserCommonCommonBundle:User');
    }

    public function findForProfileOverall(User $user)
    {
        return $this->userRepository->fetchForProfileOverall($user);
    }

    public function updateAfterPayment(User $user, Order $order)
    {
        $user->setEmailNotifications($user->getEmailNotifications() + $order->getEmailCount())
            ->setPhoneNotifications($user->getPhoneNotifications() + $order->getSmsCount());
        $this->userRepository->update($user);
    }

    public function updateAvailableEmailNotificationsCount(User $user)
    {
        if (($notificationsCount = $user->getEmailNotifications()) > 0) {
            $user->setEmailNotifications($notificationsCount - 1);
            $this->userRepository->update($user);
        }
    }

    public function updateAvailablePhoneNotificationsCount(User $user)
    {
        if (($notificationsCount = $user->getPhoneNotifications()) > 0) {
            $user->setPhoneNotifications($notificationsCount - 1);
            $this->userRepository->update($user);
        }
    }

    public function update(User $user)
    {
        $this->userRepository->update($user);
    }

    public function getById($id)
    {
        return $this->userRepository->findOneBy(['id' => $id]);
    }

    public function findByPhone(string $phone)
    {
        return $this->userRepository->findBy(['phone' => $phone]);
    }

}