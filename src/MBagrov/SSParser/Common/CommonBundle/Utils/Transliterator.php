<?php

namespace MBagrov\SSParser\Common\CommonBundle\Utils;

class Transliterator
{

    const LGN_LV = 'lv';
    const LGN_EN = 'en';
    const LGN_CYR = 'ru';

    static $cyr = [
        'ж', 'ч', 'щ', 'ш', 'ю', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'э', 'ы', 'а', 'Е', 'г', 'и', 'к', 'л', 'н', 'у', 'Ё',
        'Ж', 'Ч', 'Щ', 'Ш', 'Ю', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я', 'Э', 'Ы', 'А', 'е', 'Г', 'И', 'К', 'Л', 'Н', 'У', 'ё',];

    static $lat = [
        'ž', 'č', 'sh', 'š', 'ju', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', '', '', 'ja', 'e', 'i', 'ā', 'ē', 'ģ', 'ī', 'ķ', 'ļ', 'ņ', 'ū', 'Jo',
        'Ž', 'Č', 'Sh', 'Š', 'Ju', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', '', '', 'Ja', 'E', 'I', 'Ā', 'Ē', 'Ģ', 'Ī', 'Ķ', 'Ļ', 'Ņ', 'Ū', 'jo', ];

    static $eng = [
        'z', 'c', 'sh', 's', 'ju', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', '', '', 'ja', 'e', 'i', 'a', 'e', 'g', 'i', 'k', 'l', 'n', 'u', 'Jo',
        'Z', 'C', 'Sh', 'S', 'Ju', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', '', '', 'Ja', 'E', 'I', 'A', 'E', 'G', 'I', 'K', 'L', 'N', 'U', 'jo',];


    public static function transliterate($text = null, $from = self::LGN_CYR, $to = self::LGN_LV)
    {
        $map = [
            self::LGN_CYR => [
                self::LGN_LV => [self::$cyr, self::$lat, $text],
                self::LGN_EN => [self::$cyr, self::$eng, $text],
            ],
            self::LGN_LV  => [
                self::LGN_CYR => [self::$lat, self::$cyr, $text],
                self::LGN_EN  => [self::$lat, self::$eng, $text],
            ],
            self::LGN_EN  => [
                self::LGN_CYR => [self::$eng, self::$cyr, $text],
                self::LGN_LV  => [self::$eng, self::$lat, $text],
            ],
        ];

        return isset($map[$from][$to]) ? call_user_func_array("str_replace", $map[$from][$to]) : $text;
    }
}