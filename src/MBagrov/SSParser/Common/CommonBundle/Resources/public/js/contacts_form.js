var ContactsForm = (function () {

        var validation = function () {

            $('.contacts-form').validate({

                rules: {
                    'contacts[name]': {
                        required: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                    'contacts[email]': {
                        required: true,
                        email: true
                    },
                    'contacts[message]': {
                        required: true,
                        minlength: 3,
                        maxlength: 1000,
                    }
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
        };


        return {
            initValidation: function () {
                validation();
            }
        }

    }()
);