var ProfileOverall = (function () {

    var circles = function () {

        var totalNotifications = $('#notifications-count-val').val();
        var smsNotifications = $('#sms-notifications-count-val').val();
        var emailNotifications = $('#email-notifications-count-val').val();


        Circles.create({
            id:         'notifications-count',
            percentage: 100,
            radius:     70,
            width:      2,
            number:     totalNotifications,
            text:       '',
            colors:     ['#eee', '#3498db'],
            duration:   1300
        });

        Circles.create({
            id:         'sms-notifications-count',
            percentage: (smsNotifications / totalNotifications) * 100,
            radius:     70,
            width:      2,
            number:     smsNotifications,
            text:       '',
            colors:     ['#eee', '#3498db'],
            duration:   1500
        });

        Circles.create({
            id:         'email-notifications-count',
            percentage: (emailNotifications / totalNotifications) * 100,
            radius:     70,
            width:      2,
            number:     emailNotifications,
            text:       '',
            colors:     ['#eee', '#3498db'],
            duration:   1700
        });
    };

    var counters = function () {
        $('.counter').counterUp({
            delay: 50,
            time: 700
        });
    };

    return {
        initCircles: function () {
            circles();
        },
        initCounters: function () {
            counters();
        }
    }

}());