var OrderTable = (function () {

    var tooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    var dataTable = function () {
        var table = $('.orders-table');
        table.dataTable({
            "processing": false,
            "serverSide": false,
            "searching": false,
            "lengthChange": false,
            "pageLength": 10,
            "orderMulti": true,
            "info": false,
            "dom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-right"p>>>',
            "language": {
                "processing": null,
                "paginate": {
                    "previous": '',
                    "next": ''
                }
            },
            "order": [],
            "columnDefs": [
                {"orderable": false, "targets": 0},
                {"orderable": false, "targets": 0},
                {"orderable": false, "targets": 1},
                {"width": "1%", "targets": 0},
                {"className": "text-center", "targets": 0}
            ],
            initComplete: function () {
                $('.datatable_block').fadeIn();
                $('.loader').hide();
            }
        });

        table.on('error.dt', 'none');

    };

    var formExpandListen = function () {
        $(document).on('click', '.payment-begin', function (ev) {
            ev.preventDefault();
            $('.payment-form-wrapper').fadeIn();
            $('.order-info-wrapper').hide();
            $(ev.target).hide();
        });
    };

    var onPageLoad = function (isSubmitted, isValid) {
        if (isSubmitted) {
            if (!isValid) {
                $('.payment-begin-wrapper').hide();
                $('.order-info-wrapper').hide();
                $('.payment-form-wrapper').show();
            } else {
                $('.payment-processing-form-wrapper').show();
                $('.order-info-wrapper').hide();
                $('.payment-form-wrapper').hide();
                initPayment();
            }
        } else {
            $('.payment-begin-wrapper').show();
            $('.order-info-wrapper').show();
            $('.payment-form-wrapper').hide();
        }
    };

    var initPayment = function () {
        $('#payment-start').submit();
    };

    var validation = function () {
        $('.payment-form').validate({
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },

            rules: {
                'payment[cardHolderName]': {
                    maxlength: 128,
                    minlength: 3,
                },
                'payment[streetAddress1]': {
                    maxlength: 64,
                    minlength: 3,
                },
                'payment[streetAddress2]': {
                    maxlength: 64,
                    minlength: 1,
                },
                'payment[city]': {
                    maxlength: 64,
                    minlength: 3,
                },
                'payment[state]': {
                    maxlength: 64,
                    minlength: 3,
                },
                'payment[zip]': {
                    maxlength: 16,
                    minlength: 3,
                },
                'payment[country]': {
                    maxlength: 64,
                    minlength: 3,
                },
            },
        });
    };

    return {
        initTooltips: function () {
            tooltips();
        },

        initDataTable: function () {
            dataTable();
        },

        initFormExpandListen: function () {
            formExpandListen();
        },

        initPageState: function (isSubmitted, isValid) {
            onPageLoad(isSubmitted, isValid);
        },

        initValidation: function () {
            validation();
        },

    }

}());