var registration = (function () {

    $.validator.addMethod(
        'regex',
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        'Please check your input.'
    );

    var validation = function () {
        $('.fos_user_registration_register').validate({

            rules: {
                'fos_user_registration_form[phone]': {
                    regex: '^2[0-9]{7}$'
                },
                'fos_user_registration_form[plainPassword][first]' : {
                    minlength : 5
                },
                'fos_user_registration_form[plainPassword][second]' : {
                    minlength : 5,
                    equalTo : "#fos_user_registration_form_plainPassword_first"
                }
            },

            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            }
        });
    };

    var tooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    return {
        initValidation: function () {
            validation();
        },
        initTooltips: function () {
            tooltips();
        }
    }

}());