var ProfileEdit = (function () {


    var validation = function () {
        $('.fos_user_change_password').validate({

            rules: {
                'fos_user_change_password_form[plainPassword][first]': {
                    minlength: 5
                },
                'fos_user_change_password_form[plainPassword][second]': {
                    minlength: 5,
                    equalTo: "#fos_user_change_password_form_plainPassword_first"
                }
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
        $('.phone_form').validate({

            rules: {
                'phone': {
                    regex: '^2[0-9]{7}$',
                },
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
    };

    var backToTab = function () {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href=#' + url.split('#')[1] + ']').tab('show');
        }

        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
    };

    // var stripe = function () {
    //     $(function ($) {
    //         $('#payment_card_form').submit(function (event) {
    //             var $form = $(this);
    //
    //             // Disable the submit button to prevent repeated clicks
    //             $form.find('button').prop('disabled', true);
    //
    //             Stripe.card.createToken($form, stripeResponseHandler);
    //
    //             // Prevent the form from submitting with the default action
    //             return false;
    //         });
    //     });
    // };

    var stripeResponseHandler = function (status, response) {
        console.log(response);
        var $form = $('#payment_card_form');

        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
            // and submit
            //$form.get(0).submit();
        }
    };

    return {
        initValidation: function () {
            validation();
        },
        initBackToTab: function () {
            backToTab();
        },
        initTooltips: function() {
            $('[data-toggle="tooltip"]').tooltip();
        },
        // ititPaymentMethodForm: function () {
        //     stripe();
        // }
    }

}());