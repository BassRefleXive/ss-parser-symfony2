var passwrd_reset = (function () {

    var validation = function () {
        $('.fos_user_resetting_request').validate({

            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            }
        });

        $('.fos_user_resetting_reset').validate({

            rules: {
                'fos_user_resetting_form[plainPassword][first]' : {
                    minlength : 5
                },
                'fos_user_resetting_form[plainPassword][second]' : {
                    minlength : 5,
                    equalTo : "#fos_user_resetting_form_plainPassword_first"
                }
            },

            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            }
        });

    };

    return {
        initValidation: function () {
            validation();
        }
    }

}());