var OrderCreate = (function () {

    var number_format = function( number, decimals, dec_point, thousands_sep ) {
        var i, j, kw, kd, km;
        if( isNaN(decimals = Math.abs(decimals)) ){
            decimals = 2;
        }
        if( dec_point == undefined ){
            dec_point = ",";
        }
        if( thousands_sep == undefined ){
            thousands_sep = ".";
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
        if( (j = i.length) > 3 ){
            j = j % 3;
        } else{
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : "");
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
        return km + kw + kd;
    };



    var totalCount = $('#totalCount');
    var totalCost = $('#totalCost');

    var updateTotals = function() {
        var smsTotalCost = parseFloat($('#smsTotalCost').text());
        var emailTotalCost = parseFloat($('#emailTotalCost').text());
        totalCost.text(number_format(Math.round((smsTotalCost + emailTotalCost) * 100) / 100, 2, '.', ''));
        var smsTotalCount = parseInt($('#smsCount').text());
        var emailTotalCount = parseInt($('#emailCount').text());
        totalCount.text(smsTotalCount + emailTotalCount);
    };

    var updatePrice = function (data) {

        var config = data.config;
        var current = data.current;

        var total = 0;
        var currentPrice = data.config.price.start;

        for (var i = 1; i <= current.count; i++) {
            total += currentPrice;
            if (i % config.step.count === 0 && currentPrice > config.price.min) {
                currentPrice = Math.round((currentPrice * config.step.value) * 100) / 100;
            }
        }

        if (data.hasOwnProperty('callback') && typeof data.callback !== 'undefined') {
            data.callback({
                current: Math.round(currentPrice * 100) / 100,
                total: Math.round(total * 100) / 100
            });
        }

    };

    var notificationsSelector = function (type, maxCount) {

        var pricesCalculatorTable = $('#prices-calculator');
        var count = $('#order_' + type + 'Count');
        var displayCount = $('#' + type + 'Count');
        var displayPerOne = $('#' + type + 'PerOne');
        var displayTotalCost = $('#' + type + 'TotalCost');

        var pricingConfig = {
            price: {
                start: pricesCalculatorTable.data(type + '-start_price'),
                min: pricesCalculatorTable.data(type + '-min_price')
            },
            step: {
                value: pricesCalculatorTable.data(type + '-step_value'),
                count: pricesCalculatorTable.data(type + '-step_count')
            }
        };

        updatePrice({
            config: pricingConfig,
            current: {
                count: count.val()
            },
            callback: function (result) {
                displayPerOne.text(number_format(result.current, 2, '.', ''));
                displayTotalCost.text(number_format(result.total, 2, '.', ''));
            }
        });

        displayCount.text(count.val());

        $('#' + type + '_notifications-slider').slider({
            min: 0,
            max: maxCount,
            values: [count.val()],
            slide: function (event, ui) {
                displayCount.text(ui.value);
                count.val(ui.value);

                updatePrice({
                    config: pricingConfig,
                    current: {
                        count: ui.value
                    },
                    callback: function (result) {
                        displayPerOne.text(number_format(result.current, 2, '.', ''));
                        displayTotalCost.text(number_format(result.total, 2, '.', ''));
                        updateTotals();
                    }
                });
            }
        });


    };


    return {
        initSliders: function () {
            notificationsSelector('sms', 100);
            notificationsSelector('email', 200);
            updateTotals();
        }
    }

}());