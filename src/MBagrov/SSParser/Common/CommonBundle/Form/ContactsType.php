<?php


namespace MBagrov\SSParser\Common\CommonBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactsType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min'        => 3,
                        'max'        => 20,
                        'minMessage' => $this->translator->trans('contacts.name.min', [], 'common'),
                        'maxMessage' => $this->translator->trans('contacts.name.max', [], 'common'),
                    ]),
                ],
            ])
            ->add('email', 'email', [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('message', 'textarea', [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min'        => 3,
                        'max'        => 1000,
                        'minMessage' => $this->translator->trans('contacts.message.min', [], 'common'),
                        'maxMessage' => $this->translator->trans('contacts.message.max', [], 'common'),
                    ])
                ],
            ]);
    }

    public function getName()
    {
        return 'contacts';
    }
}