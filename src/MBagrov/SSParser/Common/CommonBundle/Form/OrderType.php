<?php

namespace MBagrov\SSParser\Common\CommonBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class OrderType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('smsCount', 'integer')
            ->add('emailCount', 'integer');
    }

    public function getName()
    {
        return 'order';
    }
}