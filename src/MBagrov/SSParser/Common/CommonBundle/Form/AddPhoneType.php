<?php

namespace MBagrov\SSParser\Common\CommonBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AddPhoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('phone', 'text', [
            'constraints' => [
                new Assert\NotBlank(),
                new Assert\Length([
                    'min'        => 8,
                    'max'        => 8,
                ]),
                new Assert\Regex([
                    'pattern' => '/^2[0-9]{7}$/'
                ]),
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection'    => false,
        ]);
    }

    public function getName()
    {
        return 'common_add_phone';
    }

}