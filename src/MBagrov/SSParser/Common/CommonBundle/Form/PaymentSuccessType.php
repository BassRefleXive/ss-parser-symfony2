<?php


namespace MBagrov\SSParser\Common\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Uuid;

class PaymentSuccessType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order_number', 'number', [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('merchant_order_id', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Uuid(),
                ],
            ])
            ->add('invoice_id', 'number', [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('credit_card_processed', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new EqualTo([
                        'value' => 'Y'
                    ]),
                ],
            ])
            ->add('total', 'number', [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('key', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 32,
                        'max' => 32,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection'    => false,
            'allow_extra_fields' => true,
        ]);
    }

    public function getName()
    {
        return 'payment_success';
    }

}