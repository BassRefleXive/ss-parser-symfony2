<?php


namespace MBagrov\SSParser\Common\CommonBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EmailNotificationWebHookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timestamp', 'integer', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('token', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 50,
                        'max' => 50,
                    ]),
                ]
            ])
            ->add('signature', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('event', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    public function getName()
    {
        return 'notificationWebHookType';
    }
}