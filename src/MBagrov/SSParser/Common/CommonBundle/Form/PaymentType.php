<?php


namespace MBagrov\SSParser\Common\CommonBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class PaymentType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardHolderName', 'text', [
                //'data' => 'MIHAILS BAGROVS'
            ])
            ->add('streetAddress1', 'text', [
                //'data' => 'Rivas iela 2'
            ])
            ->add('streetAddress2', 'text', [
                //'data' => '27'
            ])
            ->add('city', 'text', [
                //'data' => 'Riga'
            ])
            ->add('state', 'text', [
                //'data' => 'Riga'
            ])
            ->add('zip', 'text', [
                //'data' => 'LV-1010'
            ])
            ->add('country', 'text', [
                //'data' => 'Latvia'
            ]);
    }

    public function getName()
    {
        return 'payment';
    }
}