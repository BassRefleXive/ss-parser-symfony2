<?php

namespace MBagrov\SSParser\Common\CommonBundle\Command;


use Collection\AbstractSequence;
use Collection\Map;
use Collection\Sequence;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification as FlatEstateNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\NotificationsEndedNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\CommonException;
use MBagrov\SSParser\Common\CommonBundle\Repositories\NotificationRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\NotificationRepository as FlatEstateNotificationRepository;
use MBagrov\SSParser\Common\CommonBundle\Repositories\UserRepository;
use MBagrov\SSParser\Common\CommonBundle\Services\SentryNotifierService;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\NotificationsEndedMailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;

class NotifyEndedNotificationsCommand extends ContainerAwareCommand
{
    /** @var  NotificationsEndedMailer */
    private $notificationsEndedMailer;
    /** @var  NotificationRepository */
    private $notificationRepo;
    /** @var  FlatEstateNotificationRepository */
    private $flatEstateNotificationRepo;
    /** @var  UserRepository */
    private $userRepo;
    /** @var  SentryNotifierService */
    private $sentryNotifier;
    /** @var  Translator */
    private $translator;

    protected function configure()
    {
        $this
            ->setName('common:notifications:notifications:ended')
            ->setDescription('Send notifications about not viewed ads.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->bootstrapCommand();

            $this->findNotifications()
                ->map(function (NotificationsEndedNotification $notification) {
                    $this->notificationRepo->persistAndFlush($notification);
                    $this->translator->setLocale($notification->getUser()->getLocale());
                    $this->notificationsEndedMailer->setNotificationsEndedNotification($notification)->send();
                });
        } catch (CommonException $e) {
            $this->sentryNotifier->error($e->getMessage());
            $this->sentryNotifier->captureException($e);
        }

    }

    private function findNotifications(): AbstractSequence
    {

        $result = new Sequence();

        $users = new Sequence($this->userRepo->findBy([
            'emailNotifications' => 0,
            'status' => 1,
        ]));

        $users = $users
            ->filter(function (User $user) {
                /** @var FlatEstateNotification $lastFlatEstateNotification */
                $lastFlatEstateNotification = $this->flatEstateNotificationRepo->findLastEmailByUser($user);
                if (!$lastFlatEstateNotification) return false;

                /** @var Notification $lastNotificationsEndedNotification */
                $lastNotificationsEndedNotification = $this->notificationRepo->findLastByUser($user);
                if (!$lastNotificationsEndedNotification) return true;

                return $lastNotificationsEndedNotification->getCreatedAt() < $lastFlatEstateNotification->getCreatedAt();
            });

        $users->map(function (User $user) use ($result) {
            $result->add(
                (new NotificationsEndedNotification())->setUser($user)
            );
        });

        return $result;
    }


    private function bootstrapCommand()
    {
        /**
         * @var ContainerInterface $container
         */
        $container = $this->getContainer();

        $this->notificationsEndedMailer = $container->get('mailer.services.mailer_factory')->getNotificationsEndedMailer();
        $this->notificationRepo = $container->get('doctrine.orm.common_entity_manager')->getRepository(Notification::class);
        $this->flatEstateNotificationRepo = $container->get('doctrine.orm.common_entity_manager')->getRepository(FlatEstateNotification::class);
        $this->userRepo = $container->get('doctrine.orm.common_entity_manager')->getRepository(User::class);
        $this->sentryNotifier = $container->get('common.sentry_notifier');

        $this->translator = $container->get('translator');

    }
}