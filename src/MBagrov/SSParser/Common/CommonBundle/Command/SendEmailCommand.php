<?php

namespace MBagrov\SSParser\Common\CommonBundle\Command;

use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('email:send')
            ->setDescription('Send any email command.')
            ->addArgument('u', InputArgument::REQUIRED, 'User id.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container = $this->getContainer();
//echo $input->getArgument('u') . PHP_EOL;exit;
        /** @var EntityManager $em */
        $em =  $container->get('doctrine.orm.common_entity_manager');

        $user = $em->find(User::class, $input->getArgument('u'));

        $mf = $container->get('mailer.services.mailer_factory');

        $mf->getRegisterMailer()->setUser($user)->send();


    }


}