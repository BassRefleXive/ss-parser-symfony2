<?php

namespace MBagrov\SSParser\Common\CommonBundle\Command;


use Collection\AbstractSequence;
use Collection\Map;
use Collection\Sequence;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\Notification;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\CommonException;
use MBagrov\SSParser\Common\CommonBundle\Repositories\NotificationRepository;
use MBagrov\SSParser\Common\CommonBundle\Repositories\UserRepository;
use MBagrov\SSParser\Common\CommonBundle\Services\SentryNotifierService;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\AdsIgnoredMailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;

class NotifyIgnoredAdsCommand extends ContainerAwareCommand
{
    /** @var  AdsIgnoredMailer */
    private $ignoredAdsMailer;
    /** @var  NotificationRepository */
    private $notificationRepo;
    /** @var  UserRepository */
    private $userRepo;
    /** @var  SentryNotifierService */
    private $sentryNotifier;
    /** @var  Translator */
    private $translator;

    private $threshold;
    private $period;
    private $limit;
    private $min;

    protected function configure()
    {
        $this
            ->setName('common:notifications:ads:ignored')
            ->setDescription('Send notifications about not viewed ads.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->bootstrapCommand();

            $this->findNotifications()
                ->map(function (IgnoredAdsNotification $notification) {
                    $this->notificationRepo->persistAndFlush($notification);
                    $this->translator->setLocale($notification->getUser()->getLocale());
                    $this->ignoredAdsMailer->setIgnoredAdsNotification($notification)->send();
                });
        } catch (CommonException $e) {
            $this->sentryNotifier->error($e->getMessage());
            $this->sentryNotifier->captureException($e);
        }

    }

    private function findNotifications(): AbstractSequence
    {

        $result = new Sequence();

        /** @var Sequence $data */
        $this->userRepo->findWithIgnoredAds()->map(function ($item) use ($result) {
            $item = new Map($item);

            $result->add(
                (new IgnoredAdsNotification())
                    ->setIgnoredAds($item->get('ignored')->getOrThrow(new CommonException('"ignored" index bot found in SQL result')))
                    ->setTotalAds($item->get('total')->getOrThrow(new CommonException('"total" index bot found in SQL result')))
                    ->setUser($item->get('user')->getOrThrow(new CommonException('"user" index bot found in SQL result')))
            );

        });

        $result = $result
            ->filter(function (IgnoredAdsNotification $notification) {
                return (
                    $notification->getPercentDifference() >= $this->threshold &&
                    $notification->getTotalAds() > $this->min
                );
            });


        $notificationsByUsers = $this->notificationRepo->findIgnoredAdsNotificationsByUsers(
            $result->map(function (IgnoredAdsNotification $notification) {
                return $notification->getUser();
            })->all(),
            (new \DateTime())->sub(new \DateInterval('PT' . $this->limit * ($this->period + 1)  . 'H'))
        );

        $result = $result
            ->filter(function (IgnoredAdsNotification $notification) use ($notificationsByUsers) {
                if (!$notificationsByUsers->containsKey($notification->getUser()->getId())) {
                    return true;
                }

                /** @var Sequence $userNotifications */
                $userNotifications = $notificationsByUsers->get($notification->getUser()->getId())->get();
                /** @var IgnoredAdsNotification $lastNotification */
                $lastNotification = $userNotifications->head();


                $diff = (new \DateTime())->diff($lastNotification->getCreatedAt(), true);

                return (
                    $userNotifications->count() <= $this->limit &&
                    $diff->days * 24 + $diff->h >= $this->period
                );

            });

        return $result;
    }


    private function bootstrapCommand()
    {
        /**
         * @var ContainerInterface $container
         */
        $container = $this->getContainer();

        $this->ignoredAdsMailer = $container->get('mailer.services.mailer_factory')->getAdsIgnoredMailer();
        $this->notificationRepo = $container->get('doctrine.orm.common_entity_manager')->getRepository(Notification::class);
        $this->userRepo = $container->get('doctrine.orm.common_entity_manager')->getRepository(User::class);
        $this->sentryNotifier = $container->get('common.sentry_notifier');

        $this->translator = $container->get('translator');

        $config = new Map($container->getParameter('ignored_ads'));

        $this->threshold = $config->get('threshold')->getOrThrow(new CommonException('Threshold not provided in config.'));
        $this->period = $config->get('period')->getOrThrow(new CommonException('Period not provided in config.'));
        $this->limit = $config->get('limit')->getOrThrow(new CommonException('Limit not provided in config.'));
        $this->min = $config->get('min')->getOrThrow(new CommonException('min not provided in config.'));

    }
}