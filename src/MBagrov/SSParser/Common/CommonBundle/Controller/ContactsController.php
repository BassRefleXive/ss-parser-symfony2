<?php


namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class ContactsController extends Controller
{

    public function indexAction(Request $request)
    {

        $defaultData = [
            'name'    => '',
            'email'   => '',
            'message' => '',
        ];

        $form = $this->createForm('contacts', $defaultData);


        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            if ($this->get('common.services.contacts')->sendMessage($data['name'], $data['email'], $data['message'])) {

                return $this->redirectToRoute('common.contacts.message.sent');

            }

            $form->addError(new FormError($this->get('translator')->trans('contacts.message.delivery_failed', [], 'common')));

        }

        return $this->render('@MBagrovSSParserCommonCommon/Contacts/index.html.twig', [
            'form'     => $form->createView(),
            'contacts' => $this->container->getParameter('contacts')
        ]);


    }

    public function sentAction()
    {
        return $this->render('@MBagrovSSParserCommonCommon/Contacts/sent.html.twig');
    }

}