<?php

namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MBagrovSSParserCommonCommonBundle:Default:index.html.twig', array('name' => $name));
    }
}
