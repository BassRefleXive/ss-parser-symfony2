<?php

namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Form\AddPhoneType;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProfileController extends BaseController
{
    /**
     * Show the user
     */
    public function showAction()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $userService = $this->get('common.services.user');
        $userData = $userService->findForProfileOverall($user);
        if (!count($userData)) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $user = $userData[0];

        $flatEstateSearches = $user->getFlatEstateSearches();

        $timeNow = new \DateTime();
        $daysCount = 0;
        $adsCount = 0;
        $smsNotificationCount = 0;
        $emailNotificationCount = 0;

        /**
         * @var Search $search
         */
        foreach ($flatEstateSearches as $search) {
            $searchAds = $search->getSearchesAds();

            $adsCount += $searchAds->count();
            $daysCount += (int)$timeNow->diff($search->getCreatedAt())->format('%a');

            /**
             * @var Notification $notification
             * @var SearchesAd $searchAd
             */
            foreach ($searchAds as $searchAd) {
                foreach ($searchAd->getNotifications() as $notification) {
                    $notification->getType() === Notification::TYPE_SMS ? $smsNotificationCount++ : $emailNotificationCount++;
                }
            }

        }
        $avgAdsPerDay = number_format($adsCount / ($daysCount ?: 1), 2, '.', '');
        $avgAdsPerSearch = number_format($adsCount / ($flatEstateSearches->count() ?: 1), 2, '.', '');


        return $this->render('FOSUserBundle:Profile:show.html.twig', [
            'user'                   => $user,
            'searches'               => $flatEstateSearches,
            'adsCount'               => $adsCount,
            'avgAdsPerDay'           => $avgAdsPerDay,
            'avgAdsPerSearch'        => $avgAdsPerSearch,
            'smsNotificationCount'   => $smsNotificationCount,
            'emailNotificationCount' => $emailNotificationCount,
        ]);
    }

    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createForm(new AddPhoneType())->submit($request->request->all());

        if ($form->isValid()) {

            $userService = $this->get('common.services.user');
            $phone = $form->getData()['phone'];

            if (!count($userService->findByPhone($phone))) {
                $user->setPhone($phone);
                $userService->update($user);
            }

        }

        return $this->redirectToRoute('fos_user_change_password');

    }
}