<?php

namespace MBagrov\SSParser\Common\CommonBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MBagrov\SSParser\Common\CommonBundle\Form\EmailNotificationWebHookType;

class NotificationController extends Controller
{

    public function webhookEmailAction(Request $request)
    {

        $response = [
            'status' => 400,
            'msg'    => 'Bad request',
        ];

        $form = $this->createForm(new EmailNotificationWebHookType(), null, [
            'method' => 'POST',
        ]);

        $form->handleRequest($request);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            try {

                $notificationService = $this->get('common.services.notification');
                $notificationService->processEmailWebHookRequest($request->request->all());


                $response = [
                    'status' => 200,
                    'msg'    => 'Ok',
                ];

            } catch (\Exception $e) {
                $response = [
                    'status' => $e->getCode(),
                    'msg'    => $e->getMessage(),
                ];
            }
        }

        if ($response['status'] !== 200) {
            $errors = '';
            foreach ($form->getErrors(true) as $error) {
                $errors .= $error->getMessage() . PHP_EOL;
            }
            $this->get('common.sentry_notifier')->error([
                'Message'         => 'Email notification webhook error.',
                'status'          => $response['status'],
                'responseMessage' => $response['msg'],
                'submitted'       => $form->isSubmitted(),
                'errors'          => $errors,
                'postData'        => json_encode($request->request->all()),
            ]);
        }


        return new JsonResponse($response, $response['status']);

    }
}