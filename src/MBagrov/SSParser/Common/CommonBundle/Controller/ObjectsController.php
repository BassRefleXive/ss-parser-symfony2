<?php

namespace MBagrov\SSParser\Common\CommonBundle\Controller;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ObjectsController extends Controller
{
    /**
     * @var ObjectService
     */
    private $objectService;

    public function childrenAction($id) {
        $this->objectService = $this->get('common.services.object');
        $object = $this->objectService->findOneById($id);
        $childObjects = $this->objectService->getChildren($object);
        return $this->render('@MBagrovSSParserCommonCommon/Objects/childs.select.html.twig', [
            'objects' => $childObjects
        ]);
    }
}