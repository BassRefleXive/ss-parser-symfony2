<?php


namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Entity\Payment;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\PaymentException;
use MBagrov\SSParser\Common\CommonBundle\Form\PaymentSuccessType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PaymentController extends Controller
{
    public function successAction(Request $request) {


        $form = $this->createForm(new PaymentSuccessType(), $request->query->all());

        $form->handleRequest($request);

        $formData = $form->getData();

        $orderService = $this->get('common.services.order');

        if ($form->submit($formData) && $form->isValid()) {
            try {
                $order = $orderService->getByUuid($formData['merchant_order_id']);

                switch ($order->getStatus()) {
                    case Order::STATUS_NEW: {
                        $paymentService = $this->get('common.services.payment');
                        $paymentService->validateReturnRequest($order, $formData);
                        $payment = $order->getPayment();
                        $paymentService->updateSuccessfulPayment($payment, $formData);
                        $user = $order->getUser();
                        $this->get('common.services.user')->updateAfterPayment($user, $order);
                        $orderService->updateStatus($order, Order::STATUS_SUCCESS);
                        break;
                    }
                    case Order::STATUS_FAIL: {
                        throw new PaymentException('Order of this payment is failed.');
                    }
                }

                return $this->render('MBagrovSSParserCommonCommonBundle:Payment:success.html.twig', [
                    'order' => $order,
                ]);
            } catch (\Exception $e) {
                $this->get('common.sentry_notifier')->error([
                    'Error message' => $e->getMessage(),
                    'Order ID' => (isset($order) && $order instanceof Order) ? $order->getUuid() : 'Order unknown',
                    'Payment ID' => (isset($payment) && $payment instanceof Payment) ? $payment->getUuid() : 'Payment unknown',
                    'Request string' => '<pre>' . print_r($formData, true) . '</pre>',
                ]);
                $this->get('common.services.mailer')->notifyAboutPaymentFail([
                    'Error message' => $e->getMessage(),
                    'Order ID' => (isset($order) && $order instanceof Order) ? $order->getUuid() : 'Order unknown',
                    'Payment ID' => (isset($payment) && $payment instanceof Payment) ? $payment->getUuid() : 'Payment unknown',
                    'Request string' => '<pre>' . print_r($formData, true) . '</pre>',
                ]);
            }
        }

        if (isset($order) && $order instanceof Order) {
            $orderService->updateStatus($order, Order::STATUS_FAIL);
        }

        return $this->render('MBagrovSSParserCommonCommonBundle:Payment:fail.html.twig');

    }
}