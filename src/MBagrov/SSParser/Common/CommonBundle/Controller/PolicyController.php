<?php


namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PolicyController extends Controller
{
    public function indexAction() {
        return $this->render('MBagrovSSParserCommonCommonBundle:Policy:index.html.twig');
    }
}