<?php

namespace MBagrov\SSParser\Common\CommonBundle\Controller;

use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Entity\Payment;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\OrderException;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\PaymentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class OrderController extends Controller
{

    public function createAction(Request $request)
    {

        $order = new Order();

        $form = $this->createForm('order', $order);

        $pricingConfig = $this->container->getParameter('pricing_config');

        $form->handleRequest($request);

        if ($form->isValid()) {

            /**
             * @var Order $orderData
             */
            $orderData = $form->getData();

            $orderData->setCalcConfig(json_encode($pricingConfig));
            try {
                $this->get('common.services.order')->create($orderData);

                return $this->redirectToRoute('common.order.payment.start', [
                    'id' => $orderData->getUuid()
                ]);
            } catch (OrderException $e) {
                $form->addError(new FormError($this->get('translator')->trans(
                    $e->getMessage(), [
                    'limit' => number_format($pricingConfig['min_order_amount'], 2, '.', '')
                ], 'common')));
            }


        }

        return $this->render('MBagrovSSParserCommonCommonBundle:Profile:buy_notifications.html.twig', [
            'form'   => $form->createView(),
            'prices' => $pricingConfig
        ]);

    }

    /**
     * @ParamConverter("order", class="MBagrov\SSParser\Common\CommonBundle\Entity\Order",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Order   $order
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paymentAction(Order $order, Request $request)
    {

        $payment = new Payment();

        $form = $this->createForm('payment', $payment);

        $form->handleRequest($request);

        if ($form->isValid()) {

            try {
                $this->get('common.services.payment')->create($order, $form->getData());
            } catch (PaymentException $e) {
                $form->addError(new FormError($this->get('translator')->trans(
                    $e->getMessage(), [
                ], 'common')));
            }

        }

        $orderService = $this->get('common.services.order');

        $priceExplanation = $orderService->getPriceExplanation($order);

        return $this->render('MBagrovSSParserCommonCommonBundle:Order:payment_start.html.twig', [
            'paymentConfig'  => $this->getParameter('payment'),
            'paymentForm'    => $form->createView(),
            'order'          => $order,
            'explainedPrice' => $priceExplanation,
            'isSubmitted'    => (int)$form->isSubmitted(),
            'isValid'        => (int)$form->isValid()
        ]);
    }

    public function listAction(Request $request)
    {
        $orderService = $this->get('common.services.order');
        $orders = $orderService->getUserOrders();

        $resultData = [];

        foreach ($orders as $order) {
            $resultData[] = [
                'object'         => $order,
                'explainedPrice' => $orderService->getPriceExplanation($order)
            ];
        }

        return $this->render('MBagrovSSParserCommonCommonBundle:Profile:orders_show.html.twig', [
            'orders' => $resultData
        ]);
    }

}