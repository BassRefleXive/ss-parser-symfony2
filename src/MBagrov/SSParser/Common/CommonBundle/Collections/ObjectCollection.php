<?php

namespace MBagrov\SSParser\Common\CommonBundle\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;

class ObjectCollection extends ArrayCollection
{
    public function getFromSearches(SearchCollection $collection)
    {
        $filtered = [];

        $cities = new self;

        foreach($collection as $search) {
            /**
             * @var ParseObject $city
             */
            $city = $search->getCity();
            if (!in_array(($cityId = $city->getId()), $filtered, true)) {
                $filtered[] = $cityId;
                $cities->add($city->getObject());
            }
        }

        return $cities;
    }
}