<?php

namespace MBagrov\SSParser\Common\CommonBundle;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\SocialUserTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationStatusType;

class MBagrovSSParserCommonCommonBundle extends Bundle
{
    public function boot()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.common_entity_manager');

        Type::hasType('enum_social_user_type') ?: Type::addType('enum_social_user_type', SocialUserTypeEnum::class);
        Type::hasType('enum_notification_type') ?: Type::addType('enum_notification_type', NotificationTypeEnum::class);
        Type::hasType('notification_status') ?: Type::addType('notification_status', NotificationStatusType::class);

        $dbPlatform = $entityManager->getConnection()->getDatabasePlatform();
        $dbPlatform->registerDoctrineTypeMapping('int', 'notification_status');
    }

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
