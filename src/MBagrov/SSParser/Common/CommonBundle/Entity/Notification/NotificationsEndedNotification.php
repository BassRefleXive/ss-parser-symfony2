<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Notification;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;

/** @ORM\Entity() */
class NotificationsEndedNotification extends Notification
{
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User", inversedBy="notificationsEndedNotifications")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    protected $user;
}