<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Notification;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;

/** @ORM\Entity() */
class DisabledSearchNotification extends Notification
{
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User", inversedBy="disabledSearchesNotifications")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", name="total_ads", unique=false, nullable=false)
     */
    private $totalAds;

    /**
     * @ORM\Column(type="integer", name="ignored_ads", unique=false, nullable=false)
     */
    private $ignoredAds;

    public function __construct()
    {
        parent::__construct();

        $this->totalAds = 0;
        $this->ignoredAds = 0;
    }

    public function getTotalAds(): int
    {
        return $this->totalAds;
    }

    public function setTotalAds(int $totalAds): DisabledSearchNotification
    {
        $this->totalAds = $totalAds;

        return $this;
    }

    public function getIgnoredAds(): int
    {
        return $this->ignoredAds;
    }

    public function setIgnoredAds(int $ignoredAds): DisabledSearchNotification
    {
        $this->ignoredAds = $ignoredAds;

        return $this;
    }

    public function getPercentDifference(): int
    {
        return 100 - ceil((1 - ($this->getIgnoredAds() / $this->getTotalAds())) * 100);
    }

}