<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Notification;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Notifiable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;

/**
 * Notification
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Notification",
 *      schema="common",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_UNIQUE", columns={"uuid"})
 *      },
 *      indexes={
 *          @ORM\Index(name="fk_Notification_1_idx", columns={"User_id"})
 *      }
 *  )
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\NotificationRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_id", type="enum_notification_type")
 * @ORM\DiscriminatorMap({
 *     NotificationTypeEnum::NOTIFICATION_TYPE_IGNORED_ADS = "MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification",
 *     NotificationTypeEnum::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED = "MBagrov\SSParser\Common\CommonBundle\Entity\Notification\NotificationsEndedNotification",
 *     NotificationTypeEnum::NOTIFICATION_TYPE_DISABLED_SEARCH = "MBagrov\SSParser\Common\CommonBundle\Entity\Notification\DisabledSearchNotification"
 * })
 */
abstract class Notification
{
    use Timestampable, Uuidable, Notifiable;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    protected $user;

    public function __construct()
    {
        $this->status = new NotificationStatus();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Notification
    {
        $this->user = $user;

        return $this;
    }

}