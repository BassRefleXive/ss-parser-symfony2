<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\DisabledSearchNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\FacebookUser;
use MBagrov\SSParser\Common\CommonBundle\Entity\Social\GoogleUser;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search as FlatEstateSearch;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\NotificationsEndedNotification;

/**
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\UserRepository")
 *
 * @ORM\Table(name="`User`",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="UNIQ_2DA1797792FC23A8", columns={"username_canonical"}),
 *          @ORM\UniqueConstraint(name="UNIQ_2DA17977A0D96FBF", columns={"email_canonical"}),
 *          @ORM\UniqueConstraint(name="UNIQ_2DA17977444F97DD", columns={"phone"}),
 *          @ORM\UniqueConstraint(name="UNIQ_FACEBOOK_ID", columns={"facebook_id"}),
 *          @ORM\UniqueConstraint(name="UNIQ_GOOGLE_ID", columns={"google_id"})
 *     }
 * )
 */
class User extends BaseUser
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="phone", length=8, unique=true, nullable=true)
     */
    protected $phone;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="email_notifications", unique=false, nullable=false)
     */
    protected $emailNotifications;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="phone_notifications", unique=false, nullable=false)
     */
    protected $phoneNotifications;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="status", unique=false, nullable=false)
     */
    protected $status;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="type", unique=false, nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="locale", length=6, unique=false, nullable=false)
     */
    protected $locale;

    /**
     * @var ArrayCollection|FlatEstateSearch
     *
     * @ORM\OneToMany(targetEntity="\MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search", mappedBy="user")
     */
    protected $flatEstateSearches;

    /**
     * @var ArrayCollection|Order[]
     *
     * @ORM\OneToMany(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Order", mappedBy="user")
     */
    protected $orders;

    /**
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Social\FacebookUser", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="facebook_id", referencedColumnName="id")
     */
    protected $facebookUser;

    /**
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Social\GoogleUser", inversedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="google_id", referencedColumnName="id")
     */
    protected $googleUser;

    /**
     * @var ArrayCollection|IgnoredAdsNotification[]
     *
     * @ORM\OneToMany(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification", mappedBy="user")
     */
    protected $ignoredAdsNotifications;

    /**
     * @var ArrayCollection|NotificationsEndedNotification[]
     *
     * @ORM\OneToMany(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Notification\NotificationsEndedNotification", mappedBy="user")
     */
    protected $notificationsEndedNotifications;

    /**
     * @var ArrayCollection|DisabledSearchNotification[]
     *
     * @ORM\OneToMany(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Notification\DisabledSearchNotification", mappedBy="user")
     */
    protected $disabledSearchesNotifications;

    /**
     * User Entity constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->emailNotifications = 0;
        $this->phoneNotifications = 0;
        $this->status = 1;
        $this->type = 0;
        $this->flatEstateSearches = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->ignoredAdsNotifications = new ArrayCollection();
        $this->notificationsEndedNotifications = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return int
     */
    public function getEmailNotifications()
    {
        return $this->emailNotifications;
    }

    /**
     * @param int $emailNotifications
     *
     * @return $this
     */
    public function setEmailNotifications($emailNotifications)
    {
        $this->emailNotifications = $emailNotifications;

        return $this;
    }

    /**
     * @return int
     */
    public function getPhoneNotifications()
    {
        return $this->phoneNotifications;
    }

    /**
     * @param int $phoneNotifications
     *
     * @return $this
     */
    public function setPhoneNotifications($phoneNotifications)
    {
        $this->phoneNotifications = $phoneNotifications;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }


    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }

    /**
     * @param $flatEstateSearches
     *
     * @return $this
     */
    public function setFlatEstateSearches($flatEstateSearches)
    {
        $this->flatEstateSearches = $flatEstateSearches;

        return $this;
    }

    /**
     * @return ArrayCollection|FlatEstateSearch[]
     */
    public function getFlatEstateSearches()
    {
        return $this->flatEstateSearches;
    }

    /**
     * @return ArrayCollection|Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param ArrayCollection|Order[] $orders
     *
     * @return $this
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);

        return $this;
    }


    public function setFacebookUser(FacebookUser $facebookUser): User
    {
        $this->facebookUser = $facebookUser;

        return $this;
    }

    public function getFacebookUser()
    {
        return $this->facebookUser;
    }


    public function setGoogleUser(GoogleUser $googleUser): User
    {
        $this->googleUser = $googleUser;

        return $this;
    }

    public function getGoogleUser()
    {
        return $this->googleUser;
    }

    public function getIgnoredAdsNotifications(): ArrayCollection
    {
        return $this->ignoredAdsNotifications;
    }

    public function setIgnoredAdsNotifications(ArrayCollection $ignoredAdsNotifications): User
    {
        $this->ignoredAdsNotifications = $ignoredAdsNotifications;

        return $this;
    }

    public function addIgnoredAdsNotification(IgnoredAdsNotification $ignoredAdsNotification): User
    {
        $this->ignoredAdsNotifications->add($ignoredAdsNotification);

        return $this;
    }

    public function getNotificationsEndedNotifications(): ArrayCollection
    {
        return $this->notificationsEndedNotifications;
    }

    public function setNotificationsEndedNotifications(ArrayCollection $notificationsEndedNotifications): User
    {
        $this->notificationsEndedNotifications = $notificationsEndedNotifications;

        return $this;
    }

    public function addNotificationsEndedNotification(IgnoredAdsNotification $notificationsEndedNotification): User
    {
        $this->notificationsEndedNotifications->add($notificationsEndedNotification);

        return $this;
    }

    public function getDisabledSearchNotifications(): ArrayCollection
    {
        return $this->disabledSearchesNotifications;
    }

    public function setDisabledSearchNotifications(ArrayCollection $disabledSearchesNotifications): User
    {
        $this->disabledSearchesNotifications = $disabledSearchesNotifications;

        return $this;
    }

    public function addDisabledSearchNotifications(IgnoredAdsNotification $disabledSearchesNotifications): User
    {
        $this->disabledSearchesNotifications->add($disabledSearchesNotifications);

        return $this;
    }


}