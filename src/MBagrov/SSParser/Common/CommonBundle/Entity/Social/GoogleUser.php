<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Social;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity() */
class GoogleUser extends SocialUser
{
    /**
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User", mappedBy="googleUser")
     */
    protected $user;
}