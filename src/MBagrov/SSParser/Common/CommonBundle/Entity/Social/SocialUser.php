<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Social;

use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\SocialUserTypeEnum;

/**
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\SocialUserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="SocialUser")
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_id", type="enum_social_user_type")
 * @ORM\DiscriminatorMap({
 *     SocialUserTypeEnum::SOCIAL_USER_TYPE_FACEBOOK = "MBagrov\SSParser\Common\CommonBundle\Entity\Social\FacebookUser",
 *     SocialUserTypeEnum::SOCIAL_USER_TYPE_GOOGLE = "MBagrov\SSParser\Common\CommonBundle\Entity\Social\GoogleUser"
 * })
 */
abstract class SocialUser
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", name="social_id", unique=true, nullable=false)
     */
    protected $socialId;

    /**
     * @ORM\Column(type="string", name="name", length=150, unique=false, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="avatar", length=255, unique=false, nullable=true)
     */
    protected $avatar;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): SocialUser
    {
        $this->name = $name;

        return $this;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar): SocialUser
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getSocialId()
    {
        return $this->socialId;
    }

    public function setSocialId($socialId): SocialUser
    {
        $this->socialId = $socialId;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user): SocialUser
    {
        $this->user = $user;

        return $this;
    }

}