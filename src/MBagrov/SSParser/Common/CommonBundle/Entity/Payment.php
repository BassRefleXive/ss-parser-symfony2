<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\PaymentRepository")
 * @ORM\Table(name="Payment",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})
 *     },
 *     indexes={
 *          @ORM\Index(name="fk_Payment_1_idx", columns={"Order_id"})
 *     }
 * )
 */
class Payment
{
    use Timestampable, Uuidable;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="cardholder_name", length=128, unique=false, nullable=false)
     */
    protected $cardHolderName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="street_address", length=64, unique=false, nullable=false)
     */
    protected $streetAddress1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="street_address2", length=64, unique=false, nullable=false)
     */
    protected $streetAddress2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="city", length=64, unique=false, nullable=false)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="state", length=64, unique=false, nullable=false)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="zip", length=16, unique=false, nullable=false)
     */
    protected $zip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="country", length=64, unique=false, nullable=false)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="order_number", length=45, unique=false, nullable=true)
     */
    protected $orderNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="invoice_id", length=45, unique=false, nullable=true)
     */
    protected $invoiceId;


    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Order", inversedBy="payment")
     * @ORM\JoinColumn(name="Order_id", referencedColumnName="id")
     */
    protected $order;

    public function __construct()
    {
        $this->cardHolderName = '';
        $this->streetAddress1 = '';
        $this->streetAddress2 = '';
        $this->city = '';
        $this->state = '';
        $this->zip = '';
        $this->country = '';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getCardHolderName()
    {
        return $this->cardHolderName;
    }

    /**
     * @param string $cardHolderName
     *
     * @return $this
     */
    public function setCardHolderName($cardHolderName)
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetAddress1()
    {
        return $this->streetAddress1;
    }

    /**
     * @param string $streetAddress1
     *
     * @return $this
     */
    public function setStreetAddress1($streetAddress1)
    {
        $this->streetAddress1 = $streetAddress1;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetAddress2()
    {
        return $this->streetAddress2;
    }

    /**
     * @param string $streetAddress2
     *
     * @return $this
     */
    public function setStreetAddress2($streetAddress2)
    {
        $this->streetAddress2 = $streetAddress2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     *
     * @return $this
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param string $invoiceId
     *
     * @return $this
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

}