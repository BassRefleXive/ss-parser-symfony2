<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;

class NotificationStatusType extends Type
{

    const STATUS = 'notification_status';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'int';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new NotificationStatus($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->getStatus();
    }

    public function getName()
    {
        return self::STATUS;
    }

    public function canRequireSQLConversion()
    {
        return true;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return $sqlExpr;
    }
}