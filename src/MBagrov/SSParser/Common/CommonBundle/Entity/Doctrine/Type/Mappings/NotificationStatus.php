<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings;

class NotificationStatus
{

    const STATUS_INITIAL_SUCCESS = 0;
    const STATUS_INITIAL_FAIL = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_REJECTED = 4;
    const STATUS_DELIVERED = 8;
    const STATUS_FAILED = 16;
    const STATUS_OPENED = 32;
    const STATUS_CLICKED = 64;
    const STATUS_UN_SUBSCRIBED = 128;
    const STATUS_COMPLAINED = 256;
    const STATUS_STORED = 512;
    const STATUS_DROPPED = 1024;
    const STATUS_BOUNCED = 2048;

    private $status;

    public function __construct($value = null)
    {
        $this->status = $value ?? NotificationStatus::STATUS_INITIAL_SUCCESS;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): NotificationStatus
    {
        $this->status = ($this->status | $status);

        return $this;
    }

    public function unsetStatus($status)
    {
        $this->status = ($this->status & ~$status);

        return $this;
    }

    public function checkStatusesAny(array $bits)
    {
        $result = false;
        foreach ($bits as $bit) {
            $result = $result || ((bool)((int)$this->status & (int)$bit));
            if ($result) {
                break;
            }
        }

        return $result;
    }

    public function isInitialSuccess()
    {
        return $this->checkStatus(self::STATUS_INITIAL_SUCCESS);
    }

    /**
     * @param $bit
     *
     * @return bool
     */
    public function checkStatus($bit)
    {
        return (bool)((int)$this->status & (int)$bit);
    }

    public function isInitialFail()
    {
        return $this->checkStatus(self::STATUS_INITIAL_FAIL);
    }

    public function isAccepted()
    {
        return $this->checkStatus(self::STATUS_ACCEPTED);
    }

    public function isRejected()
    {
        return $this->checkStatus(self::STATUS_REJECTED);
    }

    public function isDelivered()
    {
        return $this->checkStatus(self::STATUS_DELIVERED);
    }

    public function isFailed()
    {
        return $this->checkStatus(self::STATUS_FAILED);
    }

    public function isOpened()
    {
        return $this->checkStatus(self::STATUS_OPENED);
    }

    public function isClicked()
    {
        return $this->checkStatus(self::STATUS_CLICKED);
    }

    public function isUnSubscribed()
    {
        return $this->checkStatus(self::STATUS_UN_SUBSCRIBED);
    }

    public function isComplained()
    {
        return $this->checkStatus(self::STATUS_COMPLAINED);
    }

    public function isStored()
    {
        return $this->checkStatus(self::STATUS_STORED);
    }

    public function isDropped()
    {
        return $this->checkStatus(self::STATUS_DROPPED);
    }

    public function isBounced()
    {
        return $this->checkStatus(self::STATUS_BOUNCED);
    }
}