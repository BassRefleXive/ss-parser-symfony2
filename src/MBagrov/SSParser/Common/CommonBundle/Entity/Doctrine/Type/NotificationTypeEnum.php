<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type;


class NotificationTypeEnum extends Enum
{
    const NOTIFICATION_TYPE_IGNORED_ADS = 'IGN';
    const NOTIFICATION_TYPE_NOTIFICATIONS_ENDED = 'EN';
    const NOTIFICATION_TYPE_DISABLED_SEARCH = 'SD';

    protected $name = 'enum_notification_type';
    protected $values = [
        self::NOTIFICATION_TYPE_IGNORED_ADS,
        self::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED,
        self::NOTIFICATION_TYPE_DISABLED_SEARCH,
    ];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given notification type not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::NOTIFICATION_TYPE_IGNORED_ADS => 'Ignored ads',
            self::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED   => 'Notifications ended',
            self::NOTIFICATION_TYPE_DISABLED_SEARCH   => 'Searches disabled',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given notification type is not supported.');
        }

        return $types[$key];
    }
}