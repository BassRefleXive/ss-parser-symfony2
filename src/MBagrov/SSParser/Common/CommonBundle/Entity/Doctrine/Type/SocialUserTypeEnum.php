<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type;


class SocialUserTypeEnum extends Enum
{
    const SOCIAL_USER_TYPE_FACEBOOK = 'F';
    const SOCIAL_USER_TYPE_GOOGLE = 'G';

    protected $name = 'enum_social_user_type';
    protected $values = [self::SOCIAL_USER_TYPE_FACEBOOK, self::SOCIAL_USER_TYPE_GOOGLE];


    static function getKeyByName($name)
    {
        $name = strtolower($name);

        $types = static::getTypes();
        $keys = array_flip($types);
        $keys = array_change_key_case($keys, CASE_LOWER);

        if (!array_key_exists($name, $keys)) {
            throw new \InvalidArgumentException('Given social user type name is not supported.');
        }

        return $keys[$name];
    }

    static function getTypes()
    {
        return [
            self::SOCIAL_USER_TYPE_FACEBOOK => 'Facebook',
            self::SOCIAL_USER_TYPE_GOOGLE   => 'Google',
        ];
    }

    static function getNameByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new \InvalidArgumentException('Given social user type is not supported');
        }

        return $types[$key];
    }
}