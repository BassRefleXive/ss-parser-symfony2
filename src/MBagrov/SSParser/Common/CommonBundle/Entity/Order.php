<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\OrderRepository")
 * @ORM\Table(name="`Order`",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})
 *     },
 *     indexes={
 *          @ORM\Index(name="fk_Order_1_idx", columns={"User_id"})
 *     }
 * )
 */
class Order
{
    use Timestampable, Uuidable;

    const STATUS_NEW = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAIL = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(type="float", name="amount", unique=false, nullable=false)
     */
    protected $amount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="sms_count", unique=false, nullable=false)
     */
    protected $smsCount;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="email_count", unique=false, nullable=false)
     */
    protected $emailCount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="calc_config", length=255, unique=false, nullable=true)
     */
    protected $calcConfig;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="status", unique=false, nullable=false)
     */
    protected $status;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var Payment
     *
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Payment", mappedBy="order")
     */
    protected $payment;

    public function __construct()
    {
        $this->amount = 0;
        $this->smsCount = 0;
        $this->emailCount = 0;
        $this->calcConfig = '';
        $this->status = self::STATUS_NEW;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return int
     */
    public function getSmsCount()
    {
        return $this->smsCount;
    }

    /**
     * @param int $smsCount
     *
     * @return $this
     */
    public function setSmsCount($smsCount)
    {
        $this->smsCount = $smsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getEmailCount()
    {
        return $this->emailCount;
    }

    /**
     * @param int $emailCount
     *
     * @return $this
     */
    public function setEmailCount($emailCount)
    {
        $this->emailCount = $emailCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getCalcConfig()
    {
        return $this->calcConfig;
    }

    /**
     * @param string $calcConfig
     *
     * @return $this
     */
    public function setCalcConfig($calcConfig)
    {
        $this->calcConfig = $calcConfig;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     *
     * @return $this
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;

        return $this;
    }

    public function validate (ExecutionContextInterface $context) {
        if (!$this->getSmsCount() && !$this->getEmailCount()) {
            $context->buildViolation('common.order.at_least_one')
                ->addViolation();
        }
    }


}