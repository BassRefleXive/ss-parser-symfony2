<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Common\CommonBundle\Repositories\ObjectRepository")
 * @ORM\Table(name="Objects",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})
 *     },
 *     indexes={
 *          @ORM\Index(name="is_final_idx", columns={"is_final"}),
 *          @ORM\Index(name="type_idx", columns={"type"}),
 *          @ORM\Index(name="parent_id_idx", columns={"parent_id"}),
 *          @ORM\Index(name="name_ru", columns={"name_ru"}),
 *          @ORM\Index(name="name_lv", columns={"name_lv"})
 *      }
 *   )
 */
class Object
{
    const TYPE_CITY = 4;

    use Timestampable, Uuidable;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="parent_id", unique=false, nullable=false)
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="type", unique=false, nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="is_final", unique=false, nullable=false)
     */
    private $isFinal;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="status", unique=false, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="path", length=45, unique=false, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name_ru", length=45, unique=false, nullable=true)
     */
    private $nameRu;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name_lv", length=45, unique=false, nullable=true)
     */
    private $nameLv;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="trans_const", length=100, unique=false, nullable=true)
     */
    private $transConst;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="class_name", length=45, unique=false, nullable=true)
     */
    private $className;

    /**
     * @var ParseObject
     *
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject", mappedBy="object")
     */
    private $parseObject;

    /**
     *
     */
    public function __construct()
    {
        $this->flatEstateTypes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     *
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsFinal()
    {
        return $this->isFinal;
    }

    /**
     * @param int $isFinal
     *
     * @return $this
     */
    public function setIsFinal($isFinal)
    {
        $this->isFinal = $isFinal;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * @param string $nameRu
     *
     * @return $this
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameLv()
    {
        return $this->nameLv;
    }

    /**
     * @param string $nameLv
     *
     * @return $this
     */
    public function setNameLv($nameLv)
    {
        $this->nameLv = $nameLv;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransConst()
    {
        return $this->transConst;
    }

    /**
     * @param string $transConst
     *
     * @return $this
     */
    public function setTransConst($transConst)
    {
        $this->transConst = $transConst;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param string $className
     *
     * @return $this
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    public function __toString()
    {
        return $this->getTransConst();
    }

    /**
     * @return ParseObject
     */
    public function getParseObject()
    {
        return $this->parseObject;
    }

    /**
     * @param ParseObject $parseObject
     *
     * @return $this
     */
    public function setParseObject(ParseObject $parseObject)
    {
        $this->parseObject = $parseObject;

        return $this;
    }

}