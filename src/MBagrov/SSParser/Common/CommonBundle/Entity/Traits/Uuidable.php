<?php


namespace MBagrov\SSParser\Common\CommonBundle\Entity\Traits;


use Rhumsaa\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;

trait Uuidable
{

    /**
     * @ORM\Column(name="uuid", type="string", nullable=false, length=36, unique=true, options={"fixed" = true})
     */
    private $uuid;

    /**
     * @ORM\PrePersist
     */
    public function prePersistUuid()
    {
        if ($this->uuid === null) {
            $this->uuid = (string)Uuid::uuid4();
        }
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }
}