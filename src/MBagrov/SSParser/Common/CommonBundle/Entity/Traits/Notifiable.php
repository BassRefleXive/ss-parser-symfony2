<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Traits;


use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;

trait Notifiable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="notification_status", nullable=true)
     */
    private $status;

    public function getStatus(): NotificationStatus
    {
        if ($this->status === null) {
            $this->status = new NotificationStatus();
        }

        return clone $this->status;
    }

    public function setStatus(NotificationStatus $status)
    {
        $this->status = $status;

        return $this;
    }

}