<?php

namespace MBagrov\SSParser\Common\CommonBundle\Entity\Traits;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

trait Serializable
{
    function jsonSerialize()
    {
        $jmsContext = SerializationContext::create();

        $jsonContent = SerializerBuilder::create()
            ->build()
            ->serialize($this, 'json', $jmsContext);

        return json_decode($jsonContent, true);
    }
}