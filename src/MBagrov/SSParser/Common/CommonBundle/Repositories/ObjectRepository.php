<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;

class ObjectRepository extends EntityRepository
{

    public function update(CommonObject $order)
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function findByAnyName(string $name) {
        $qb = $this->createQueryBuilder('o');

        $qb
            ->select('o')
            ->andWhere('o.nameRu = :NAME')
            ->orWhere('o.nameLv = :NAME')
            ->setParameter('NAME', $name)
            ->setMaxResults(1);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (ORMException $e) {
            return null;
        }

    }

}