<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;

use Collection\Sequence;
use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;

class UserRepository extends EntityRepository
{
    public function fetchForProfileOverall(User $user)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u, fes, fasads, fasadsnotifications')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->leftJoin('u.flatEstateSearches', 'fes')
            ->leftJoin('fes.searchesAds', 'fasads')
            ->leftJoin('fasads.notifications', 'fasadsnotifications');

        return $qb->getQuery()->getResult();
    }

    public function update(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush($user);
    }

    public function findWithIgnoredAds(): Sequence
    {

        $qb = $this->createQueryBuilder('u');

        $qb
            ->select('u as user')
            ->addSelect('COUNT(sad) as ignored')
            ->leftJoin('u.flatEstateSearches', 's')
            ->leftJoin('s.searchesAds', 'sad')
            ->andWhere('s.status = :SEARCH_ACTIVE')
            ->andWhere('BIT_AND(sad.status, :OPENED_EMAIL) = 0')
            ->andWhere('BIT_AND(sad.status, :OPENED_WEB) = 0')
            ->andWhere('BIT_AND(sad.status, :RECEIVED_SMS) = 0')
            ->setParameters([
                'OPENED_EMAIL' => SearchesAd::STATUS_OPENED_MAIL,
                'OPENED_WEB'   => SearchesAd::STATUS_OPENED_ON_SITE,
                'RECEIVED_SMS' => SearchesAd::STATUS_RECEIVED_SMS,
                'SEARCH_ACTIVE' => Search::STATUS_ENABLED,
            ])
            ->groupBy('u.id')
            ->addSelect(
                '(SELECT COUNT(sad1)
                FROM
                    MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search s1,
                    MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd sad1
                WHERE
                    u = s1.user AND sad1.search = s1) as total'
            );


        return new Sequence($qb->getQuery()->getResult());

    }

}