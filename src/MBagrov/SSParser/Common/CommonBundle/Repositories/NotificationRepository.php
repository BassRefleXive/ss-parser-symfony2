<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;

use Collection\Map;
use Collection\Sequence;
use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\Notification;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;

class NotificationRepository extends EntityRepository
{

    public function findIgnoredAdsNotificationsByUsers(array $users, \DateTime $minDate): Map
    {
        $qb = $this->createQueryBuilder('n');

        $qb->select('n')
            ->andWhere('n INSTANCE OF :TYPE_IGNORED')
            ->andWhere('n.user IN (:USERS)')
            ->andWhere('n.createdAt > :MIN_DATE')
            ->setParameters([
                'TYPE_IGNORED' => NotificationTypeEnum::NOTIFICATION_TYPE_IGNORED_ADS,
                'USERS'        => $users,
                'MIN_DATE'     => $minDate,
            ])
            ->orderBy('n.createdAt', 'DESC');

        $toReturn = new Map();

        /** @var IgnoredAdsNotification $result */
        foreach ($qb->getQuery()->getResult() as $result) {

            $notifications = $toReturn->get($result->getUser()->getId())->getOrElse(new Sequence());
            $notifications->add($result);

            $toReturn->set($result->getUser()->getId(), $notifications);
        }

        return $toReturn;

    }

    public function findLastByUser(User $user) {
        $qb = $this->createQueryBuilder('n');

        $qb
            ->select('n')
            ->andWhere('n.user = :USER')
            ->andWhere('n INSTANCE OF :TYPE')
            ->setParameters([
                'USER' => $user,
                'TYPE' => NotificationTypeEnum::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED,
            ])
            ->orderBy('n.createdAt', 'DESC')
            ->setMaxResults(1);

        $result = $qb->getQuery()->getResult();

        return count($result)
            ? $result[0]
            : false;
    }

    public function persistAndFlush(Notification $notification)
    {
        $this->_em->persist($notification);
        $this->_em->flush($notification);
    }

}