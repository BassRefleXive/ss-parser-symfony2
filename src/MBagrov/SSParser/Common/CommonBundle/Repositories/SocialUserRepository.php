<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\SocialUserTypeEnum;

class SocialUserRepository extends EntityRepository
{
    public function findByTypeAndSocialId(string $type, string $socialId)
    {

        $qb = $this->createQueryBuilder('u')
            ->andWhere('u INSTANCE OF :provider')
            ->andWhere('u.socialId = :socialId')
            ->setParameter('provider', $type)
            ->setParameter('socialId', $socialId);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (ORMException $e) {
            return null;
        }

    }

    public function findGoogleUser(string $socialId)
    {
        return $this->findByTypeAndSocialId(SocialUserTypeEnum::SOCIAL_USER_TYPE_GOOGLE, $socialId);
    }

    public function findFacebookUser(string $socialId)
    {
        return $this->findByTypeAndSocialId(SocialUserTypeEnum::SOCIAL_USER_TYPE_FACEBOOK, $socialId);
    }
}