<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;


use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\Order;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;

class OrderRepository extends EntityRepository
{

    public function create(Order $order)
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function update(Order $order)
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function findByUuid($uuid)
    {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }

    public function findByUser(User $user)
    {
        $qb = $this->createQueryBuilder('o')
            ->select('o')
            ->andWhere('o.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }


}