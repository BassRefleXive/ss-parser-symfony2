<?php

namespace MBagrov\SSParser\Common\CommonBundle\Repositories;


use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\Payment;

class PaymentRepository extends EntityRepository
{

    public function create(Payment $payment)
    {
        $this->_em->persist($payment);
        $this->_em->flush();
    }

    public function update(Payment $payment)
    {
        $this->_em->persist($payment);
        $this->_em->flush();
    }

    public function findByUuid($uuid)
    {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }


}