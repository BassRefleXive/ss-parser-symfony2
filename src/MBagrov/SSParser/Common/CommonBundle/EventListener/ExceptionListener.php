<?php

namespace MBagrov\SSParser\Common\CommonBundle\EventListener;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

class ExceptionListener
{

    /**
     * @var KernelInterface
     */
    protected $kernel;
    protected $templating;

    public function __construct(TwigEngine $templating, KernelInterface $kernel)
    {
        $this->templating = $templating;
        $this->kernel = $kernel;
    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        if ($this->kernel->getEnvironment() === 'prod') {

            $exception = $event->getException();

            $response = new Response();

            if ($event->getException() instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            switch($response->getStatusCode()) {
                case 404: {
                    $response->setContent($this->templating->render('@MBagrovSSParserCommonCommon/Error/not_found.html.twig'));
                    break;
                }
                default: {
                    $response->setContent($this->templating->render('@MBagrovSSParserCommonCommon/Error/general.html.twig'));
                }
            }

            $event->setResponse($response);
        }

    }

}