<?php

namespace MBagrov\SSParser\Common\CommonBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class RegistrationListener implements EventSubscriberInterface
{

    private $session;
    private $registrationConfig;
    private $defaultLocale;

    public function __construct(Session $session, array $registrationConfig, string $defaultLocale)
    {
        $this->session = $session;
        $this->registrationConfig = $registrationConfig;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
            FOSUserEvents::REGISTRATION_SUCCESS   => 'onRegistrationSuccess',
        ];
    }

    public function onRegistrationCompleted()
    {
        $this->session->getFlashBag()->clear();
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        /**
         * @var User $formData
         */
        $formData = $event->getForm()->getData();
        $formData->setEmailNotifications($this->registrationConfig['free_email']);
        $formData->setPhoneNotifications($this->registrationConfig['free_sms']);
        $formData->setLocale($this->defaultLocale);
    }

}