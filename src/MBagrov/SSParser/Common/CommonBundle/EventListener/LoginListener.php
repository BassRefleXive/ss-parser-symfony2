<?php


namespace MBagrov\SSParser\Common\CommonBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginListener implements EventSubscriberInterface
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onFosLogin',
            SecurityEvents::INTERACTIVE_LOGIN      => 'onInteractiveLogin',
        ];
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->session->set('_locale', $event->getAuthenticationToken()->getUser()->getLocale());
    }

    public function onFosLogin(UserEvent $event)
    {
        $this->session->set('_locale', $event->getUser()->getLocale());
    }

}