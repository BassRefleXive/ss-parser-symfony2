<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Repositories;


use Collection\Sequence;
use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;

class NotificationRepository extends EntityRepository
{

    public function create(Notification $notification)
    {
        $this->_em->persist($notification);
        $this->_em->flush();
    }

    public function update(Notification $notification)
    {
        $this->_em->persist($notification);
        $this->_em->flush();
    }

    public function findByUuid($uuid)
    {
        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;
    }

    public function findLastEmailByUser(User $user)
    {
        $qb = $this->createQueryBuilder('n');

        $qb
            ->select('n')
            ->leftJoin('n.searchesAd', 'sad')
            ->leftJoin('sad.search', 's')
            ->leftJoin('s.user', 'u')
            ->andWhere('u = :USER')
            ->andWhere('n.type = :TYPE')
            ->setParameters([
                'USER' => $user,
                'TYPE' => Notification::TYPE_EMAIL,
            ])
            ->orderBy('n.createdAt', 'DESC')
            ->setMaxResults(1);

        $result = $qb->getQuery()->getResult();

        return count($result)
            ? $result[0]
            : false;

    }

}