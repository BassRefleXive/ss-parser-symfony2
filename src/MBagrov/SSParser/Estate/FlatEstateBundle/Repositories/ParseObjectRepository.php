<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Repositories;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use Metadata\ClassMetadata;
use Symfony\Component\Translation\TranslatorInterface;

class ParseObjectRepository extends EntityRepository
{
    public function getParseObjectChoices()
    {
        /**
         * @var ArrayCollection|ParseObject
         * @var ParseObject $parseObject
         */
        $allParseObjects = $this->findAll();
        $choices = [];
        foreach ($allParseObjects as $parseObject) {
            $choices[$parseObject->getObject()->getId()] = $parseObject->getObject()->getTransConst();
        }

        return $choices;
    }

    public function findByUuid($uuid) {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }
}