<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Repositories;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;

class AdRepository extends EntityRepository
{

    public function findByUuid($uuid)
    {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }

    public function findBySSUniqueIdList(array $ids)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->andWhere('a.ssUniqId IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function storeBatch(ArrayCollection $collection)
    {
        foreach ($collection as $entity) {
            $this->_em->persist($entity);
        }
        $this->_em->flush();
    }

    public function findSimilarAds(Ad $ad)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.contentHash = :contentHash')
            ->orderBy('a.createdAtProvider', 'DESC')
            ->setParameter('contentHash', $ad->getContentHash());

        return $qb->getQuery()->getResult();
    }

    public function findLatestAdWithHistory()
    {
        try {
            $q = $this->createQueryBuilder('a')
                ->select('a')
                ->leftJoin(Ad::class, 'a1', 'WITH', 'a.contentHash = a1.contentHash')
                ->where('a.id != a1.id')
                ->orderBy('a.id', 'DESC')
                ->setMaxResults(100);

            $result = $q->getQuery()->getResult();

            return count($result)
                ? $result
                : null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function update(Ad $ad)
    {
        $this->_em->persist($ad);
        $this->_em->flush();
    }

}