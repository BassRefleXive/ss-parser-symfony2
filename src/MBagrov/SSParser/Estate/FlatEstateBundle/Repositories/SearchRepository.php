<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Repositories;


use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;

class SearchRepository extends EntityRepository
{
    public function create(Search $search)
    {
        $this->_em->persist($search);
        $this->_em->flush();
    }

    public function update(Search $search)
    {
        $this->_em->flush();
    }

    public function getForDataTable(User $user, $queryParams)
    {
        $columns = [
            '',
            'searchesAdsCount',
            's.type',
            's.city',
            'districtsCount',
            'estateTypesCount',
            's.floorMin',
            's.roomsMin',
            's.areaMin',
            's.priceMin',
            ''
        ];

        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.objects', 'objects')
            ->addSelect('COUNT(objects) as districtsCount')
            ->leftJoin('s.estateTypes', 'estateTypes')
            ->addSelect('COUNT(estateTypes) as estateTypesCount')
            ->leftJoin('s.searchesAds', 'searchesAds')
            ->addSelect('COUNT(searchesAds) as searchesAdsCount')
            ->where('s.user = :user')
            ->setParameter('user', $user)
            ->groupBy('s.id');


        if (isset($queryParams['start']) && $queryParams['length'] != '-1') {
            $qb->setFirstResult((int)$queryParams['start'])
                ->setMaxResults((int)$queryParams['length']);
        }

        // Check that columns are sent in request
        if (isset($queryParams['columns']) && is_array($queryParams['columns'])) {
            // Loop over all sent columns
            foreach ($queryParams['columns'] as $colIdx => $column) {
                // If column is known in server side
                if (isset($columns[$colIdx])) {
                    /**
                     * Ordering.
                     * Check that column is orderable. If it is, then check if ordering data for that column is in request,
                     * if ordering data is in request - add ordering data in query builder.
                     */
                    if (isset($queryParams['order']) && is_array($queryParams['order'])) {
                        foreach ($queryParams['order'] as $order) {
                            if ($order['column'] == $colIdx) {
                                $qb->orderBy($columns[(int)$colIdx], $order['dir']);
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (!isset($queryParams['order'])) {
            $qb->orderBy('s.createdAt', 'desc');
        }

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($qb, true);
        $paginator->setUseOutputWalkers(false);

        $rResult = $paginator->getQuery()->getResult();

        $result = [];
        foreach ($rResult as $tmpResult) {
            $result[] = $tmpResult[0];
        }

        return $result;
    }

    public function getTotalCount()
    {
        $aResultTotal = $this
            ->createQueryBuilder('s')
            ->select('COUNT(s.id)')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $aResultTotal[0][1];
    }

    public function findByUuid($uuid)
    {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }

    public function getForParse()
    {
        $qb = $this->createQueryBuilder('s')
            ->innerJoin('s.user', 'u')
            ->innerJoin('s.city', 'c')
            ->andWhere('s.status = 1')
            ->andWhere('u.status = 1');

        return $qb->getQuery()->getResult();
    }


}