<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;

class SearchesAdRepository extends EntityRepository
{

    private static $Columns = [
        0 => [
            'sort'   => '',
            'search' => ''
        ],
        1 => [
            'sort'   => '',
            'search' => ''
        ],
        2 => [
            'sort'   => 'BIT_AND(s.status, 2)',
            'search' => 'BIT_AND(s.status, :arg_2) = :arg_2'
        ],
        3 => [
            'sort'   => 'ad.type',
            'search' => 'ad.type = :arg_3'
        ],
        4 => [
            'sort'   => 'ad.object',
            'search' => 'po.uuid NOT IN (:arg_4)'
        ],
        5 => [
            'sort'   => 'ad.estateType',
            'search' => 'et.uuid NOT IN (:arg_5)'
        ],
        6 => [
            'sort'   => 'ad.floor',
            'search' => 'ad.floor = :arg_6'
        ],
        7 => [
            'sort'   => 'ad.roomsCount',
            'search' => 'ad.roomsCount = :arg_7'
        ],
        8 => [
            'sort'   => 'ad.area',
            'search' => 'ad.area = :arg_8'
        ],
        9 => [
            'sort'   => 'ad.price',
            'search' => 'ad.price = :arg_9'
        ],
    ];

    public function getAvailableEntitiesCount(Search $search)
    {
        return count($this->getBasicQueryBuilderForAvailableEntities($search)->getQuery()->getResult());
    }

    protected function getBasicQueryBuilderForAvailableEntities(Search $search)
    {

        return $this->createQueryBuilder('s')
            ->select('s', 'ad', 'po', 'et')
            ->leftJoin('s.ad', 'ad')
            ->leftJoin('ad.object', 'po')
            ->leftJoin('ad.estateType', 'et')
            ->andWhere('s.search = :search')
            ->andWhere('BIT_AND(s.status, 1) != 1')
            ->setParameter('search', $search);

    }

    public function getForDataTable(Search $search, $queryParams)
    {

        $qb = $this->getBasicQueryBuilderForAvailableEntities($search);


        if (isset($queryParams['start']) && $queryParams['length'] != '-1') {
            $qb->setFirstResult((int)$queryParams['start'])
                ->setMaxResults((int)$queryParams['length']);
        }

        // Check that columns are sent in request
        if (isset($queryParams['columns']) && is_array($queryParams['columns'])) {
            // Loop over all sent columns
            foreach ($queryParams['columns'] as $colIdx => $column) {
                // If column is known in server side
                if (isset(self::$Columns[$colIdx])) {
                    /**
                     * Ordering.
                     * Check that column is orderable. If it is, then check if ordering data for that column is in request,
                     * if ordering data is in request - add ordering data in query builder.
                     */
                    if (isset($queryParams['order']) && is_array($queryParams['order'])) {
                        foreach ($queryParams['order'] as $order) {
                            if ($order['column'] == $colIdx) {
                                $qb->orderBy(self::$Columns[(int)$colIdx]['sort'], $order['dir']);
                                break;
                            }
                        }
                    }
                    if (isset($column['searchable']) && $column['searchable'] && isset($column['search']) && is_array($column['search'])) {

                        $value = $column['search']['value'];
                        if ((int)$colIdx === 4 || (int)$colIdx === 5) {
                            $value = json_decode($value);
                        }

                        if (
                            (is_string($value) && mb_strlen($value)) ||
                            (is_array($value) && count($value))
                        ) {
                            $qb->andWhere(self::$Columns[(int)$colIdx]['search'])
                                ->setParameter('arg_' . $colIdx, $value);
                        }
                    }
                }
            }
        }

        if (!isset($queryParams['order'])) {
            $qb->orderBy('BIT_AND(s.status, 2)', 'desc');
            $qb->addOrderBy('ad.createdAtProvider', 'desc');
        }

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($qb, true);
        $paginator->setUseOutputWalkers(false);

        $rResult = $paginator->getQuery()->getResult();

        $result = [];
        foreach ($rResult as $tmpResult) {
            $result[] = $tmpResult;
        }

        return [
            'result' => $result,
            'total'  => count($qb->setMaxResults(null)->setFirstResult(0)->getQuery()->getResult()),
        ];
    }

    public function findByUuid($uuid)
    {

        $entity = $this->findBy(['uuid' => $uuid]);

        return count($entity) ? $entity[0] : null;

    }

    public function update(SearchesAd $searchesAd)
    {
        $this->_em->persist($searchesAd);
        $this->_em->flush($searchesAd);
    }

    public function create(SearchesAd $searchesAd)
    {
        $this->_em->persist($searchesAd);
        $this->_em->flush($searchesAd);
    }

}