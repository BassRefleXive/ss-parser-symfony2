<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Command;

use GuzzleHttp\Client;
use MBagrov\SSParser\Common\CommonBundle\Collections\ObjectCollection;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Common\CommonBundle\Services\MailerService;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use MBagrov\SSParser\Common\CommonBundle\Utils\Transliterator;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DiscoverLocationsCommand extends ContainerAwareCommand
{

    const REAL_ESTATE_OBJECT_ID = 641;

    const TYPE_CITY = 'city';
    const TYPE_DISTRICT = 'district';

    static $IGNORED_CITIES = [
        'За границей Латвии',
        'Другой',
        'Другое',
    ];

    static $IGNORED_DISTRICTS = [
        'Все объявления',
        'Другой',
        'Другое',
    ];

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ObjectService
     */
    protected $objectService;

    protected $crawlerConfig;

    protected $sentryNotifier;

    protected $logData;

    protected $newDistrictsCount;

    protected $newCitiesCount;


    protected function configure()
    {
        $this
            ->setName('discover:locations')
            ->setDescription('Discover all cities and districts for flate estate.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {


            $this->container = $this->getContainer();
            /**
             * @var MailerService $mailer
             */
            $mailer = $this->container->get('common.services.mailer');
            $this->objectService = $this->container->get('common.services.object');

            /**
             * @var CommonObject $flatEstateObject
             */
            $flatEstateObject = $this->objectService->findOneById(self::REAL_ESTATE_OBJECT_ID);

            $this->client = new Client();
            $this->crawlerConfig = $this->container->getParameter('crawler');
            $this->sentryNotifier = $this->container->get('common.sentry_notifier');

            $this->getCities($flatEstateObject);

            if ($this->newCitiesCount || $this->newDistrictsCount) {
                $this->sentryNotifier->info('New objects found!' . PHP_EOL . sprintf('Found new %s cities and %s districts.', $this->newCitiesCount, $this->newDistrictsCount));
                $mailer->notifyAboutLocationsDiscovery('New objects found!', sprintf('Found new %s cities and %s districts.', $this->newCitiesCount, $this->newDistrictsCount));
            }

            foreach ($this->logData as $logMessage) {
                switch ($logMessage['level']) {
                    case 'Info': {
                        $this->sentryNotifier->info($logMessage['msg']);
                        break;
                    }
                    case 'Error': {
                        $this->sentryNotifier->error($logMessage['msg']);
                        break;
                    }
                    default: {
                        $this->sentryNotifier->warning([
                            'error'     => 'UNKNOWN LOG LEVEL!!! ',
                            'level'     => $logMessage['level'],
                            'Message: ' => $logMessage['msg'],
                        ]);
                    }
                }
            }

        } catch (\Exception $e) {
            $mailer->notifyAboutLocationsDiscovery('Error occurred.', 'Error in location discovery has occurred. Error: ' . $e->getMessage());
            throw $e;
        }

    }

    protected function getCities(CommonObject $flatEstateObject)
    {

        $citiesCrawler = new Crawler();
        $citiesCrawler->add($this->getData($flatEstateObject));

        $nodes = $citiesCrawler->filterXPath('//*[@class="a_category"]');
        $this->logData[] = [
            'level' => 'Info',
            'msg'   => sprintf('Total found %s cities in html.', $nodes->count()),
        ];

        $currentCities = [];

        if ($nodes->count()) {
            $currentCities = $this->objectService->getChildren($flatEstateObject, true);
        }
        $currentCities = new ObjectCollection($currentCities);

        $this->newCitiesCount = 0;
        $this->newDistrictsCount = 0;

        $nodes->each(function (Crawler $node, $i) use ($currentCities, $flatEstateObject) {

            if (!in_array($nameRu = trim($node->text()), self::$IGNORED_CITIES, true)) {

                $citiesKnown = $currentCities->filter(function (CommonObject $knownCity) use ($nameRu) {
                    return $knownCity->getNameRu() === $nameRu;
                });

                if ($citiesKnown->count() === 1) {
                    $city = $citiesKnown->first();
                } elseif ($citiesKnown->count()) {
                    $this->logData[] = [
                        'level' => 'Error',
                        'msg'   => sprintf('More than one city in our database matches discovered city! Name: %s', $nameRu),
                    ];
                    throw new FlatEstateException(sprintf('More than one city in our database matches discovered city! Name: %s', $nameRu));
                } else {
                    $city = new Object();

                    $city
                        ->setNameRu($nameRu)
                        ->setParentId($flatEstateObject->getId())
                        ->setType(CommonObject::TYPE_CITY)
                        ->setIsFinal(0)
                        ->setStatus(0)
                        ->setPath($this->getLastPathSegment($node->attr('href')))
                        ->setTransConst($this->getTransConst($city, self::TYPE_CITY));


                    $this->logData[] = [
                        'level' => 'Info',
                        'msg'   => sprintf('FOUND NEW CITY! Name: %s', $nameRu),
                    ];
                    $this->newCitiesCount++;
                }

                $this->objectService->update($city);
                $this->loadCityDistricts($city);
            }


        });

        $this->logData[] = [
            'level' => 'Info',
            'msg'   => sprintf('Totally found %s new cities.', $this->newCitiesCount),
        ];
    }

    protected function getData(CommonObject $object)
    {
        if ($this->crawlerConfig['estate']['flats']['demo']) {
            $fileName = $this->crawlerConfig['estate']['flats']['demo_data_dir'] . 'flat_estate_locations/' . $object->getUuid() . '.html';
            if (file_exists($fileName)) {
                $data = file_get_contents($fileName);
            } else {
                $baseUrl = $this->objectService->buildUrl($object, ObjectService::URL_TYPE_HTML);
                $data = $this->getResourceContents($baseUrl);
                file_put_contents($fileName, $data);
            }
        } else {
            $baseUrl = $this->objectService->buildUrl($object, ObjectService::URL_TYPE_HTML);
            $data = $this->getResourceContents($baseUrl);
        }

        return $data;
    }

    protected function getResourceContents($url)
    {
        $request = $this->client->createRequest('GET', $url);

        return $this->client->send($request)->getBody()->getContents();
    }

    protected function getLastPathSegment($path)
    {
        return ($count = count(($parts = explode('/', $path)))) > 1 ? $parts[$count - 2] . '/' : '';
    }

    protected function loadCityDistricts(CommonObject $city)
    {
        $districtsCrawler = new Crawler();
        $districtsCrawler->add($this->getData($city));

        $nodes = $districtsCrawler->filterXPath('//*[@class="a_category"]');
        $this->logData[] = [
            'level' => 'Info',
            'msg'   => sprintf('Total found %s districts in %s city html.', $nodes->count(), $city->getNameRu()),
        ];

        $currentDistricts = [];

        if ($nodes->count()) {
            $currentDistricts = $this->objectService->getChildren($city, true);
        }
        $currentDistricts = new ObjectCollection($currentDistricts);

        $nodes->each(function (Crawler $node, $i) use ($currentDistricts, $city) {
            if (!in_array($nameRu = trim($node->text()), self::$IGNORED_DISTRICTS, true)) {

                $districtsKnown = $currentDistricts->filter(function (CommonObject $knownDistrict) use ($nameRu) {
                    return $knownDistrict->getNameRu() === $nameRu;
                });

                if ($districtsKnown->count() > 1) {
                    $this->logData[] = [
                        'level' => 'Error',
                        'msg'   => sprintf('More than one district in our database matches discovered district. District: %s; City: %s.', $nameRu, $city->getNameRu()),
                    ];
                    throw new FlatEstateException(sprintf('More than one district in our database matches discovered district. District: %s; City: %s.', $nameRu, $city->getNameRu()));
                } elseif (!$districtsKnown->count()) {
                    $district = new Object();

                    $district
                        ->setNameRu($nameRu)
                        ->setParentId($city->getId())
                        ->setType($city->getType() + 1)
                        ->setIsFinal(1)
                        ->setStatus(0)
                        ->setPath($this->getLastPathSegment($node->attr('href')))
                        ->setTransConst($this->getTransConst($district, self::TYPE_DISTRICT));

                    $this->logData[] = [
                        'level' => 'Info',
                        'msg'   => sprintf('FOUND NEW DISTRICT %s IN %s CITY!', $nameRu, $city->getNameRu()),
                    ];
                    $this->newDistrictsCount++;

                    $this->objectService->update($district);
                }

            }
        });

        $this->logData[] = [
            'level' => 'Info',
            'msg'   => sprintf('Totally found %s new districts in %s city.', $this->newDistrictsCount, $city->getNameRu()),
        ];


    }

    protected function getTransConst(CommonObject $object, $type)
    {
        $transliterated = Transliterator::transliterate($object->getNameRu(), Transliterator::LGN_CYR, Transliterator::LGN_EN);
        $transliterated = mb_strtolower(str_replace([' ', '.'], ['_', ''], $transliterated));
        if ($type === self::TYPE_CITY) {
            return 'search.city.names.' . $transliterated;
        } elseif ($type === self::TYPE_DISTRICT) {
            return 'search.district.names.' . $transliterated;
        }

        return $transliterated;
    }

}