<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Command;

use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\AdParserService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FlatEstateAdsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ads:search:estate:flats')
            ->setDescription('Finds new flat estate ads.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var ContainerInterface $container
         */
        $container = $this->getContainer();

        $searchService = $container->get('flat_estate.services.search');
        $adService = $container->get('flat_estate.services.ad');
        $mailerService = $container->get('common.services.mailer');
        $notificationService = $container->get('flat_estate.services.notification');
        $translator = $container->get('translator');
        $searchesAdService = $container->get('flat_estate.services.search.ads');

        $searches = $searchService->findForParse();

        $ads = $searchService->searchNewAds($searches);

        /**
         * @var Ad $ad
         */
        foreach ($ads as $ad) {
            if (($adService->processNewAd($ad)) === AdParserService::STATUS_AD_NOT_APPROPRIATE) {
                $ads->removeElement($ad);
            }
        }


        $adService->storeCollection($ads);

        $searchesAds = $searchService->addSearchesToAds($searches, $ads);

        /**
         * @var SearchesAd $searchesAd
         */
        foreach ($searchesAds as $searchesAd) {

            $searchesAdService->create($searchesAd);

            $search = $searchesAd->getSearch();

            if ($search->getIsSms() || $search->getIsEmail()) {

                $user = $searchesAd->getSearch()->getUser();

                if ($search->getIsEmail() && $user->getEmailNotifications()) {
                    $translator->setLocale($user->getLocale());
                    $notification = new Notification();
                    $notification->setType(Notification::TYPE_EMAIL)
                        ->setDestination($searchesAd->getSearch()->getUser()->getEmail())
                        ->setSearchesAd($searchesAd);


                    $notificationService->notifyClient($notification);

                }

                if ($search->getIsSms() && $user->getPhoneNotifications() && $searchesAd->getSearch()->getUser()->getPhone()) {
                    $translator->setLocale($user->getLocale());
                    $notification = new Notification();
                    $notification->setType(Notification::TYPE_SMS)
                        ->setDestination($searchesAd->getSearch()->getUser()->getPhone())
                        ->setSearchesAd($searchesAd);


                    $notificationService->notifyClient($notification);

                }
            }
        }
    }
}