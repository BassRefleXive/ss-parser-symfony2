<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Form\EventListener\AddDistrictFieldSubscriber;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\ParseObjectService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

class SearchesAdType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('toggleStar', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleTrash', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleContactSeller', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleContactedSeller', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleGoView', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleViewLiked', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ])
            ->add('toggleViewDisliked', 'choice', [
                'choices' => [
                    'true'  => 'true',
                    'false' => 'false',
                ],
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    public function getName()
    {
        return 'searchesAd';
    }
}