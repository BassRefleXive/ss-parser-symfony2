<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Form\EventListener\AddDistrictFieldSubscriber;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\ParseObjectService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SearchType extends AbstractType
{

    private $parseObjectService;
    private $translator;

    public function __construct(ParseObjectService $parseObjectService, TranslatorInterface $translator)
    {
        $this->parseObjectService = $parseObjectService;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', [
                'choices'  => [
                    -1 => $this->translator->trans('search.create.type_price.type.any', [], 'flat_estate'),
                    0  => $this->translator->trans('search.create.type_price.type.rent', [], 'flat_estate'),
                    1  => $this->translator->trans('search.create.type_price.type.sell', [], 'flat_estate'),
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true
            ])
            ->add('priceMin', 'integer', [
                'attr' => [
                    'placeholder' => $this->translator->trans('search.create.type_price.price.from', [], 'flat_estate')
                ]
            ])
            ->add('priceMax', 'integer', [
                'attr' => [
                    'placeholder' => $this->translator->trans('search.create.type_price.price.to', [], 'flat_estate')
                ]
            ])
            ->add('city', 'entity', [
                'class'    => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject',
                'label'    => false,
                'required' => true,
                "query_builder" => function(EntityRepository $er){
                    return $er->createQueryBuilder("po")->where("po.status = 1")->orderBy("po.id", "ASC");
                },
            ])
            ->add('floorMin', 'integer', [
                'empty_data' => 2,
            ])
            ->add('floorMax', 'integer', [
                'empty_data' => 8,
            ])
            ->add('estateTypes', 'entity', [
                'class'    => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\EstateType',
                'label'    => false,
                'required' => true,
                'multiple' => true,
                "query_builder" => function(EntityRepository $er){
                    return $er->createQueryBuilder("et")->where("et.status = 1")->orderBy("et.id", "ASC");
                },
            ])
            ->add('roomsMin', 'integer', [
                'empty_data' => 2,
            ])
            ->add('roomsMax', 'integer', [
                'empty_data' => 3,
            ])
            ->add('areaMin', 'integer', [
                'empty_data' => 30,
            ])
            ->add('areaMax', 'integer', [
                'empty_data' => 80,
            ])
            ->add('isSms', 'checkbox', [
                'required' => false,
            ])
            ->add('isEmail', 'checkbox', [
                'required' => false,
            ])
            ->add('status', 'checkbox', [
                'required' => false,
            ])
            ->add('save', 'submit');

        $subscriber = new AddDistrictFieldSubscriber($builder->getFormFactory(), $this->parseObjectService);

        $builder->addEventSubscriber($subscriber);

    }

    public function getName()
    {
        return 'search';
    }
}