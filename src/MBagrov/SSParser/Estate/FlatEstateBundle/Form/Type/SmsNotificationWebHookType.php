<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SmsNotificationWebHookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('smssender', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('smsid', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('status', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('text', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ])
            ->add('phone', 'text', [
                'required'    => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    public function getName()
    {
        return 'notificationWebHookType';
    }
}