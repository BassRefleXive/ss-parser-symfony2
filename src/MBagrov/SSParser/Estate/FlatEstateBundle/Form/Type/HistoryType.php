<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type;

use MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints\SSUrl;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints;

class HistoryType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', 'url', [
                'required' => false,
                'label' => false,
                'attr' => array(
                    'placeholder' => 'https://www.ss.lv/msg/ru/real-estate/flats/riga/centre/gflic.html',
                ),
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Length([
                        'min' => 35,
                        'max' => 100,
                    ]),
                    new SSUrl(),
                ],
            ])
            ->add('save', 'submit');

    }

    public function getName()
    {
        return 'history';
    }

}