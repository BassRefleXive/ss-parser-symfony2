<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Form\EventListener;

use Doctrine\ORM\EntityRepository;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\ParseObjectService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class AddDistrictFieldSubscriber implements EventSubscriberInterface
{

    private $parseObjectService;
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory, ParseObjectService $parseObjectService)
    {
        $this->parseObjectService = $parseObjectService;
        $this->formFactory = $formFactory;
    }


    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        ];
    }

    public function addDistrictsField(FormInterface $form, $cityId)
    {
        $form->add('objects', 'entity', [
            'class'         => 'MBagrov\SSParser\Common\CommonBundle\Entity\Object',
            'query_builder' => function (EntityRepository $repository) use ($cityId) {
                return $repository->createQueryBuilder('objects')
                    ->where('objects.parentId = :cityId')
                    ->setParameter('cityId', $cityId);
            },
            'multiple'      => true,
            'required'      => false
        ]);
    }

    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        /**
         * @var Search $search
         */
        $search = $event->getData();
        $city = $search->getCity();
        $cityId = -1;
        if ($city !== null) {
            if ($city instanceof ParseObject) {
                $cityId = $city->getObject()->getId();
            }
        }
        $this->addDistrictsField($form, $cityId);
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        /**
         * @var Search $search
         */
        $search = $event->getData();
        $cityId = -1;
        if (isset($search['city'])) {
            $parseObject = $this->parseObjectService->findOneById($search['city']);
            if ($parseObject instanceof ParseObject) {
                $cityId = $parseObject->getObject()->getId();
            }
        }
        $this->addDistrictsField($form, $cityId);
    }

}