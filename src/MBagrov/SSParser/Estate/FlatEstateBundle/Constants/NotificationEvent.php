<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Constants;

use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateException;

class NotificationEvent
{

    const EVENT_OPENED = 'opened';
    const EVENT_CLICKED = 'clicked';
    const EVENT_UNSUBSCRIBED = 'unsubscribed';
    const EVENT_COMPLAINED = 'complained';
    const EVENT_BOUNCED = 'bounced';
    const EVENT_DROPPED = 'dropped';
    const EVENT_DELIVERED = 'delivered';

    const SMS_EVENT_DELIVERED = 'DELIVRD';
    const SMS_EVENT_SENT = 'SENT';

    private static $EventToStatus = [
        self::EVENT_DELIVERED     => NotificationStatus::STATUS_DELIVERED,
        self::EVENT_DROPPED       => NotificationStatus::STATUS_DROPPED,
        self::EVENT_BOUNCED       => NotificationStatus::STATUS_BOUNCED,
        self::EVENT_COMPLAINED    => NotificationStatus::STATUS_COMPLAINED,
        self::EVENT_UNSUBSCRIBED  => NotificationStatus::STATUS_UN_SUBSCRIBED,
        self::EVENT_CLICKED       => NotificationStatus::STATUS_CLICKED,
        self::EVENT_OPENED        => NotificationStatus::STATUS_OPENED,
        self::SMS_EVENT_DELIVERED => NotificationStatus::STATUS_DELIVERED,
        self::SMS_EVENT_SENT      => NotificationStatus::STATUS_INITIAL_SUCCESS,
    ];

    private static $StatusToEvent = [
        NotificationStatus::STATUS_DELIVERED     => self::EVENT_DELIVERED,
        NotificationStatus::STATUS_DROPPED       => self::EVENT_DROPPED,
        NotificationStatus::STATUS_BOUNCED       => self::EVENT_BOUNCED,
        NotificationStatus::STATUS_COMPLAINED    => self::EVENT_COMPLAINED,
        NotificationStatus::STATUS_UN_SUBSCRIBED => self::EVENT_UNSUBSCRIBED,
        NotificationStatus::STATUS_CLICKED       => self::EVENT_CLICKED,
        NotificationStatus::STATUS_OPENED        => self::EVENT_OPENED,
    ];

    public static function getEventStatus($event)
    {
        if (!isset(self::$EventToStatus[$event])) {
            throw new FlatEstateException('Unknown notification event.', 500);
        }

        return self::$EventToStatus[$event];
    }

    public static function getStatusEvent($status)
    {
        if (!isset(self::$StatusToEvent[$status])) {
            throw new FlatEstateException('Unknown notification event.', 500);
        }

        return self::$StatusToEvent[$status];
    }

}