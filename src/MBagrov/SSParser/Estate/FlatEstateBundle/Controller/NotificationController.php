<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;


use MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type\SmsNotificationWebHookType;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class NotificationController extends Controller
{

    public function webhookSmsAction(Request $request)
    {

        $form = $this->createForm(new SmsNotificationWebHookType());

        $form->handleRequest($request);
        $form->submit($request->query->all());

        if (!$form->isValid()) {
            $errors = '';
            foreach ($form->getErrors(true) as $error) {
                $errors .= $error->getMessage() . PHP_EOL;
            }
            $this->get('common.sentry_notifier')->error([
                'Message'   => 'SMS notification webhook error.',
                'submitted' => $form->isSubmitted(),
                'errors'    => $errors,
                'postData'  => json_encode($request->request->all()),
            ]);

            throw new BadRequestHttpException('Not all required parameters provided.');
        }

        try {
            $notificationService = $this->get('flat_estate.services.notification');
            $notificationService->processSmsWebHookRequest($form->getData());
        } catch (\Exception $e) {
            throw new InternalErrorException();
        }

        return new JsonResponse(['accepted' => true]);
    }
}