<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use MBagrov\SSParser\Estate\FlatEstateBundle\Form\Type\SearchesAdType;
use Symfony\Component\HttpFoundation\Request;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SearchesAdController extends Controller
{

    /**
     * @ParamConverter("searchesAd", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param SearchesAd $searchesAd
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function showAction(SearchesAd $searchesAd, Request $request)
    {
        $this->get('flat_estate.services.security')->isAllowed($searchesAd, true);

        if (!$searchesAd->checkStatus(SearchesAd::STATUS_OPENED_ON_SITE)) {
            $searchesAd->setStatus(SearchesAd::STATUS_OPENED_ON_SITE);
            $this->get('flat_estate.services.search.ads')->update($searchesAd);
        }

        $adService = $this->get('flat_estate.services.ad');

        if ($searchesAd->isHidden()) {
            throw $this->createNotFoundException();
        }

        $ad = $searchesAd->getAd();

        if ($request->isXmlHttpRequest()) {
            return $this->render('@MBagrovSSParserEstateFlatEstate/Ad/show.xhr.html.twig', [
                'ad'            => $ad,
                'notifications' => $searchesAd->getNotifications(),
                'similarAds'    => $adService->getSimilarAds($ad),
            ]);
        } else {
            return $this->render('@MBagrovSSParserEstateFlatEstate/Ad/show.full.html.twig', [
                'ad'            => $ad,
                'notifications' => $searchesAd->getNotifications(),
                'sAd'           => $searchesAd,
                'similarAds'    => $adService->getSimilarAds($ad),
            ]);
        }
    }

    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Search  $search
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listAction(Search $search, Request $request)
    {
        $securityService = $this->get('flat_estate.services.security');
        $securityService->isAllowed($search, true);

        $adService = $this->get('flat_estate.services.search.ads');
        $result = $adService->dataTable($search, $request->query->all());

        return new JsonResponse($result);
    }

    /**
     * @ParamConverter("searchesAd", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param SearchesAd $searchesAd
     *
     * @param Request    $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(SearchesAd $searchesAd, Request $request)
    {
        $this->get('flat_estate.services.security')->isAllowed($searchesAd, true);

        if (!$searchesAd->checkStatus(SearchesAd::STATUS_OPENED_ON_SITE)) {
            $searchesAd->setStatus(SearchesAd::STATUS_OPENED_ON_SITE);
            $this->get('flat_estate.services.search.ads')->update($searchesAd);
        }

        $form = $this->createForm(new SearchesAdType(), null, [
            'method' => 'PUT'
        ]);

        $response = [
            'status' => 200,
            'msg'    => 'Ok'
        ];

        $form->handleRequest($request);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            try {
                $response = [
                    'status' => 200,
                    'data'   => $this->get('flat_estate.services.search.ads')->updateStatus($searchesAd, $form->getData()),
                ];
            } catch (\Exception $e) {
                $response = [
                    'status' => 500,
                    'msg'    => $e->getMessage()
                ];
            }
        }

        return new JsonResponse($response, $response['status']);

    }

}