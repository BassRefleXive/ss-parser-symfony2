<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MBagrovSSParserEstateFlatEstateBundle:Default:index.html.twig', array('name' => $name));
    }
}
