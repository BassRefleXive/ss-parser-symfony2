<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HistoryController extends Controller
{
    public function indexAction(Request $request)
    {

        $data = [
            'url' => ''
        ];

        $form = $this->createForm('history', $data);


        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return $this->render('MBagrovSSParserEstateFlatEstateBundle:History:index.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        $result = $form->isValid()
            ? $this->get('flat_estate.services.ad')->processHistoryRequest($form->getData()['url'])
            : $this->get('flat_estate.services.ad')->processFakeHistoryRequest();

        if ($result) {
            $response = $this->render('MBagrovSSParserEstateFlatEstateBundle:History:list.html.twig', [
                'data' => $result,
            ]);
        } else {
            $response = $this->render('MBagrovSSParserEstateFlatEstateBundle:History:error.html.twig');
        }

        return $response;

    }
}