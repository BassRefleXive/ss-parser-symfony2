<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{

    public function indexResultsAction(Request $request)
    {
        $searchService = $this->get('flat_estate.services.search');
        $result = $searchService->dataTable($request->query->all());

        return new JsonResponse($result);
    }

    public function indexAction()
    {
        $tokenStorage = $this->get('security.token_storage');
        /**
         * @var User $user
         */
        $user = $tokenStorage->getToken()->getUser();

        return $this->render('@MBagrovSSParserEstateFlatEstate/Search/list.html.twig', [
            'areSearches' => $user->getFlatEstateSearches()->count() > 0
        ]);
    }

    public function createAction(Request $request)
    {
        $search = new Search();
        $search->setType(-1)
            ->setEstateTypes($this->get('flat_estate.services.estate_type')->findAll());

        $form = $this->createForm('search', $search);


        $form->handleRequest($request);

        if ($form->isValid()) {
            $search = $this->get('flat_estate.services.search')->create($form->getData());

            return $this->redirectToRoute('flat_estate.search.created', [
                'id' => $search->getUuid(),
            ]);
        }

        return $this->render('MBagrovSSParserEstateFlatEstateBundle:Search:new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Search  $search
     * @param Request $request
     *
     * @return Response
     */
    public function createdAction(Search $search, Request $request)
    {
        return $this->render('MBagrovSSParserEstateFlatEstateBundle:Search:created.html.twig', [
            'search' => $search,
            'user'   => $this->get('security.token_storage')->getToken()->getUser(),
        ]);
    }

    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Search  $search
     * @param Request $request
     *
     * @return Response
     */
    public function updatedAction(Search $search, Request $request)
    {
        return $this->render('MBagrovSSParserEstateFlatEstateBundle:Search:updated.html.twig', [
            'search' => $search,
            'user'   => $this->get('security.token_storage')->getToken()->getUser(),
        ]);
    }

    public function cityDistrictsAction(Request $request, $searchId, $cityId)
    {
        $parseObjectsService = $this->get('flat_estate.services.parse_object');
        $object = $parseObjectsService->findOneById($cityId)->getObject();
        $objectService = $this->get('common.services.object');
        $childObjects = $objectService->getChildren($object);

        $result = [];
        $selectedObjects = [];

        if ($searchId > 0) {
            $searchService = $this->get('flat_estate.services.search');
            $search = $searchService->findOneById($searchId);
            $selectedObjects = $search->getObjects();
        }

        foreach ($childObjects as $childObject) {
            if ($childObject->getStatus()) {
                $tmp = [
                    'object'   => $childObject,
                    'selected' => false
                ];
                if (count($selectedObjects)) {
                    foreach ($selectedObjects as $selectedObject) {
                        if ($childObject->getId() === $selectedObject->getId()) {
                            $tmp['selected'] = true;
                            break;
                        }
                    }
                }
                $result[] = $tmp;
            }
        }

        return $this->render('@MBagrovSSParserEstateFlatEstate/City/_partials/districts.html.twig', [
            'objects' => $result
        ]);
    }

    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Search  $search
     * @param int     $type
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function toggleNotificationAction(Search $search, $type, Request $request)
    {
        $result = [
            'status' => 200,
            'msg'    => 'Ok'
        ];
        try {
            $this->get('flat_estate.services.search')->toggleNotification($search, $type);
        } catch (FlatEstateException $e) {
            $result = [
                'status' => $e->getCode(),
                'msg'    => $e->getMessage()
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     * @param Search  $search
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Search $search, Request $request)
    {
        $form = $this->createForm('search', $search, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('flat_estate.services.search')->update($form->getData());

            return $this->redirectToRoute('flat_estate.search.updated', [
                'id' => $search->getUuid(),
            ]);
        }

        return $this->render('MBagrovSSParserEstateFlatEstateBundle:Search:edit.html.twig', [
            'search' => $search,
            'form'   => $form->createView(),
        ]);
    }


    /**
     * @ParamConverter("search", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Search $search
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Search $search)
    {

        $filterDistricts = $search->getObjects();

        if (!$filterDistricts->count()) {
            $filterDistricts = $this->get('common.services.object')->getChildren($search->getCity()->getObject());
        }

        return $this->render('@MBagrovSSParserEstateFlatEstate/Search/show.html.twig', [
            'search'          => $search,
            'filterDistricts' => $filterDistricts,
            'count'           => $this->get('flat_estate.services.search.ads')->getAvailableCount($search),
        ]);
    }

}