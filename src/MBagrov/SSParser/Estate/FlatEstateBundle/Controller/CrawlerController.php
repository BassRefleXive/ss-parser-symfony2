<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use MBagrov\SSParser\Common\CommonBundle\Utils\Transliterator;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\AdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchesAdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use MBagrov\SSParser\Estate\FlatEstateBundle\Services\AdParserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CrawlerController extends Controller
{
    public function indexAction()
    {
        try {

            $searchService = $this->get('flat_estate.services.search');
            $adService = $this->get('flat_estate.services.ad');
            $mailerService = $this->get('common.services.mailer');
            $notificationService = $this->get('flat_estate.services.notification');
            $userService = $this->get('common.services.user');
            $mf = $this->get('mailer.services.mailer_factory');

            $searchesAd = $this->get('doctrine')->getRepository(SearchesAd::class)->findOneBy(['id' => 37]);

            $user = $searchesAd->getSearch()->getUser();
            //$user->setEmail('mihails.bagrovs@gmail.com');
//dump();exit;

            $translator = $this->get('translator');
            $translator->setLocale('ru');
//            /**
//             * @var Notification $notification
//             */
//            $notification = $searchesAd->getNotifications()->first();
//
//            $qb = $this->get('doctrine.orm.entity_manager')->getRepository(Ad::class)->createQueryBuilder('a');
//            $res = $qb->select('COUNT(a)')
//                ->getQuery()
//                ->getResult();
//
//            $id = 94;
//            $lengths = [];
//            for ($i = 0; $i < $res[0][1]; $i++) {
//                $ad = $this->get('doctrine.orm.entity_manager')->getRepository(Ad::class)->findOneBy(['id' => $id + $i]);
//
//                if ($ad !== null) {
//                    $text = $this->get('flat_estate.services.sms_notifier')->createText($ad);
//                    $lengths[] = [
//                        'text' => $text,
//                        'len' => mb_strlen($text),
//                    ];
//
//                    if ($i > 1000) {
//                        break;
//                    }
//                }
//            }
//
//            $moreThanMax = 0;
//            $l = 0;
//            foreach($lengths as $length) {
//                if ($length['len'] > 160) {
//                    $moreThanMax++;
//                }
//                $l += $length['len'];
//            }
//
//            dump($lengths);
//
//            dump([count($lengths), $moreThanMax, ($l / count($lengths))]);
//
//            exit;




            $this->get('flat_estate.services.sms_notifier')->send($notification);
            exit;
//            $mf->getFlatEstateAdMailer()
//                ->setNotification($notification)
//                ->setAdHistory($adService->getSimilarAds($notification->getSearchesAd()->getAd()))
//                ->send();

            //$notification = new Notification();
            //$notification->setType(Notification::TYPE_EMAIL)
            //    ->setDestination($searchesAd->getSearch()->getUser()->getEmail())
            //    ->setSearchesAd($searchesAd);
            //if ($notificationService->notifyClient($notification)) {
            //    //$user = $searchesAd->getSearch()->getUser();
            //    //$userService->updateAfterEmailSent($user);
            //}

            exit;

//
//            dump($searchesAd);exit;
//            $data = [];
//
//
//
//            $searches = $searchService->findForParse();
//
//            $ads = $searchService->searchNewAds($searches);
//
//
//            $parseErrors = [];
//            /**
//             * @var Ad $ad
//             */
//            foreach ($ads as $ad) {
//                if (($adService->processNewAd($ad)) === AdParserService::STATUS_AD_NOT_APPROPRIATE) {
//                    $ads->removeElement($ad);
//                }
//                $errors = $adService->getParserErrors();
//                if (count($errors)) {
//                    $parseErrors[] = $errors;
//                }
//            }
//            dump($ads);exit;
//
//
//            if (count($parseErrors)) {
//                $mailerService->notifyAboutParseErrors($parseErrors);
//            }
//
//
//            $adService->storeCollection($ads);

            $ads = new AdCollection([$adService->findOneById(2286)]);
            $searches = new SearchCollection([$searchService->findOneById(12)]);
            $searchesAds = $searchService->addSearchesToAds($searches, $ads);
            dump($searchesAds);
            exit;

            /**
             * @var SearchesAd $searchesAd
             */
//            foreach($searchesAds as $searchesAd) {
//
////                if ($ad->getSearches()->count()) {
//
//                    $search = $searchesAd->getSearch();
//
//                    if ($search->getIsSms() || $search->getIsEmail()) {
//
//                        $user = $searchesAd->getSearch()->getUser();
//
//                        if ($search->getIsEmail() && $user->getEmailNotifications()) {
//                            $notification = new Notification();
//                            $notification->setType(Notification::TYPE_EMAIL)
//                                ->setDestination($searchesAd->getSearch()->getUser()->getEmail())
//                                ->setSearchesAd($searchesAd);
//                            if ($notificationService->notifyClient($notification)) {
//                                $userService->updateAfterEmailSent($user);
//                            }
//
//                        }
//
////                    }
//
//                }
//
//            }

//            return $this->render('@MBagrovSSParserEstateFlatEstate/Crawler/index.html.twig', [
//                'data' => $data
//            ]);

        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage());
        }
    }

}