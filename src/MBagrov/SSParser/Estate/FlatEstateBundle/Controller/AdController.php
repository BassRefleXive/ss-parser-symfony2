<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Controller;

use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AdController extends Controller
{
    /**
     * @ParamConverter("ad", class="MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad",
     *                           options={"repository_method" = "findByUuid"})
     *
     * @param Ad      $ad
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Ad $ad, Request $request)
    {
        if (!$ad->isInactive()) {
            $adService = $this->get('flat_estate.services.ad');

            if ($adService->processAdUrlRequest($ad)) {
                return $this->redirect($ad->getUrl());
            }

            $ad->setStatus(Ad::STATUS_INACTIVE);

            $adService->update($ad);
        }

        return $this->render('@MBagrovSSParserEstateFlatEstate/Ad/inactive.html.twig');

    }
}
