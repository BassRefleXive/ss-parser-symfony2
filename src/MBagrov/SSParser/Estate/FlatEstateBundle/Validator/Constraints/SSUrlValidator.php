<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;

use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class SSUrlValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {

        if ($value === null) {
            $this->context->addViolation('flat_estate.history.url.unset');
        }
        if (!is_string($value)) {
            $this->context->addViolation('flat_estate.history.url.not_string');
        }
        $parts = parse_url($value);

        if (!$parts || !is_array($parts)) {
            $this->context->addViolation('flat_estate.history.url.not_valid_url');
        }

        if (!isset($parts['scheme'], $parts['host'], $parts['path'])) {
            $this->context->addViolation('flat_estate.history.url.missing_url_parts');
        }

        if (!in_array($parts['scheme'] ?? '', ['http', 'https'], true)) {
            $this->context->addViolation('flat_estate.history.url.invalid_scheme');
        }

        if (!in_array($parts['host'] ?? '', ['www.ss.lv', 'ss.lv'], true)) {
            $this->context->addViolation('flat_estate.history.url.invalid_host');
        }

        $pathParts = explode('/', $parts['path'] ?? '');

        if (count($pathParts) <= 5) {
            $this->context->addViolation('flat_estate.history.url.invalid_path');
        }

        if (
            count($pathParts) > 5 && (
            $pathParts[1] !== 'msg' ||
            !in_array($pathParts[2], ['ru', 'lv']) ||
            $pathParts[3] !== 'real-estate' ||
            $pathParts[4] !== 'flats')
        ) {
            $this->context->addViolation('flat_estate.history.url.only_flats');
        }

    }
}