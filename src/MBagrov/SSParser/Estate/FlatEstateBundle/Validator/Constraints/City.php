<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class City extends Constraint
{
    public $message = 'flat_estate.location.city.invalid';

    public function validatedBy()
    {
        return 'city_validator';
    }

}