<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;


use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DistrictsValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var ArrayCollection $value
         */
        $cityFieldData = $this->context->getRoot()->get('city')->getData();
        if ($cityFieldData !== null && $cityFieldData instanceof ParseObject) {
            if ($value->count()) {
                $cityObjectId = $cityFieldData->getObject()->getId();
                foreach($value as $district) {
                    if ($district->getParentId() !== $cityObjectId) {
                        $this->context->addViolation('flat_estate.location.districts.not_belongs_to_city');
                    }
                }
            }
        } else {
            if ($value->count()) {
                $this->context->addViolation('flat_estate.location.districts.select_city');
            }
        }
    }
}