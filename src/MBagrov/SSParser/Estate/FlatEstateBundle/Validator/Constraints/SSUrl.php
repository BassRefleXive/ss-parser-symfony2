<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class SSUrl extends Constraint
{
    public $message = 'flat_estate.history.url.default';

    public function validatedBy()
    {
        return 'ss_url_validator';
    }

}