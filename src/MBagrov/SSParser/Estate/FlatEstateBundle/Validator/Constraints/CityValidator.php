<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;

use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class CityValidator extends ConstraintValidator
{

    private $citiesParentId;

    public function __construct($citiesParentId)
    {
        $this->citiesParentId = $citiesParentId;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var ParseObject $value
         */
        if ($value === null || !($value instanceof ParseObject)) {
            $this->context->addViolation('flat_estate.location.city.invalid');
        } else {
            if ($value->getObject()->getParentId() !== $this->citiesParentId) {
                $this->context->addViolation('flat_estate.location.city.invalid');
            }
        }
    }
}