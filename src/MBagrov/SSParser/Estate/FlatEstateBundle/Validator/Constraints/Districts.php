<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class Districts extends Constraint
{
    public $message = 'flat_estate.location.districts.invalid';

    public function validatedBy()
    {
        return 'districts_validator';
    }

}