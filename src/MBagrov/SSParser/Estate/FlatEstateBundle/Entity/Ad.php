<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;
use MBagrov\SSParser\Common\CommonBundle\Interfaces\AdInterface;

/**
 * Ad
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Ads", schema="flat_estate",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="index2", columns={"ss_uniq_id"}),
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})
 *     },
 *     indexes={
 *     @ORM\Index(name="fk_Ads_3_idx", columns={"Street_id"}),
 *     @ORM\Index(name="fk_Ads_1_idx", columns={"EstateTypes_id"}),
 *     @ORM\Index(name="fk_Ads_3_idx1", columns={"Objects_id"}), @ORM\Index(name="fk_Ads_4_idx",
 *                                      columns={"ParseObjects_id"}),
 *     @ORM\Index(name="content_hash_idx", columns={"content_hash"})
 *     })
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\AdRepository")
 */
class Ad implements AdInterface
{

    use Timestampable, Uuidable;

    const TYPE_UNKNOWN = -1;
    const TYPE_RENT = 0;
    const TYPE_SELL = 1;

    const PRICE_DAILY = 0;
    const PRICE_MONTHLY = 1;
    const PRICE_TOTAL = 2;

    const STATUS_INITIAL = 0;
    const STATUS_INACTIVE = 1;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ParseObject
     *
     * @ORM\ManyToOne(targetEntity="ParseObject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ParseObjects_id", referencedColumnName="id")
     * })
     */
    private $parseObject;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="rooms", type="integer", nullable=true)
     */
    private $roomsCount;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_type", type="integer", nullable=true)
     */
    private $priceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="integer", nullable=true)
     */
    private $floor;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float", precision=10, scale=0, nullable=true)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="price_square", type="float", precision=10, scale=0, nullable=true)
     */
    private $pricePerSquare;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="decimal", precision=8, scale=5, nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="decimal", precision=8, scale=5, nullable=false)
     */
    private $lon;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * var \DateTime
     *
     * @ORM\Column(name="created_at_provider", type="datetime")
     */
    private $createdAtProvider;

    /**
     * @var string
     *
     * @ORM\Column(name="ss_uniq_id", type="string", length=45, nullable=false)
     */
    private $ssUniqId;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="short_url", type="string", length=255, nullable=false)
     */
    private $shortUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="content_hash", type="string", length=45, nullable=true)
     */
    private $contentHash;
    /**
     * @var EstateType
     *
     * @ORM\ManyToOne(targetEntity="EstateType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="EstateTypes_id", referencedColumnName="id")
     * })
     */
    private $estateType;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var CommonObject
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Object")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Objects_id", referencedColumnName="id")
     * })
     */
    private $object;
    /**
     * @var ArrayCollection|SearchesAd
     *
     * @ORM\OneToMany(targetEntity="SearchesAd", mappedBy="ad")
     */
    private $searchesAds;
    /**
     * @ORM\ManyToMany(targetEntity="Search")
     * @ORM\JoinTable(name="flat_estate.SearchesAds",
     *      joinColumns={@ORM\JoinColumn(name="Ads_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="Searches_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $searches;

    public function __construct()
    {
        $this->searchesAds = new ArrayCollection();
        $this->searches = new ArrayCollection();
        $this->status = self::STATUS_INITIAL;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param string $shortUrl
     *
     * @return $this
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentHash()
    {
        return $this->contentHash;
    }

//    /**
//     * @var Street
//     *
//     * @ORM\ManyToOne(targetEntity="Street", fetch="EXTRA_LAZY")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="Street_id", referencedColumnName="id")
//     * })
//     */
//    private $street;

    /**
     * @param string $contentHash
     *
     * @return $this
     */
    public function setContentHash($contentHash)
    {
        $this->contentHash = $contentHash;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSearches()
    {
        return $this->searches;
    }

    /**
     * @param mixed $searches
     *
     * @return $this
     */
    public function setSearches($searches)
    {
        $this->searches = $searches;

        return $this;
    }

    /**
     * @param Search $search
     *
     * @return $this
     */
    public function addSearch(Search $search)
    {
        $this->searches->add($search);

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ParseObject
     */
    public function getParseObject()
    {
        return $this->parseObject;
    }

    /**
     * @param ParseObject $parseObject
     *
     * @return $this
     */
    public function setParseObject(ParseObject $parseObject)
    {
        $this->parseObject = $parseObject;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getRoomsCount()
    {
        return $this->roomsCount;
    }

    /**
     * @param boolean $roomsCount
     *
     * @return $this
     */
    public function setRoomsCount($roomsCount)
    {
        $this->roomsCount = $roomsCount;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * @param int $priceType
     *
     * @return $this
     */
    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * @return int
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param int $floor
     *
     * @return $this
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param float $area
     *
     * @return $this
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return float
     */
    public function getPricePerSquare()
    {
        return $this->pricePerSquare;
    }

    /**
     * @param float $pricePerSquare
     *
     * @return $this
     */
    public function setPricePerSquare($pricePerSquare)
    {
        $this->pricePerSquare = $pricePerSquare;

        return $this;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     *
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @param float $lon
     *
     * @return $this
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAtProvider()
    {
        return $this->createdAtProvider;
    }

    /**
     * @param \DateTime $createdAtProvider
     *
     * @return $this
     *
     */
    public function setCreatedAtProvider(\DateTime $createdAtProvider)
    {
        $this->createdAtProvider = $createdAtProvider;

        return $this;
    }

    /**
     * @return string
     */
    public function getSsUniqId()
    {
        return $this->ssUniqId;
    }

    /**
     * @param string $ssUniqId
     *
     * @return $this
     */
    public function setSsUniqId($ssUniqId)
    {
        $this->ssUniqId = $ssUniqId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return EstateType
     */
    public function getEstateType()
    {
        return $this->estateType;
    }

    /**
     * @param EstateType $estateType
     *
     * @return $this
     */
    public function setEstateType(EstateType $estateType)
    {
        $this->estateType = $estateType;

        return $this;
    }

//    /**
//     * @return Street
//     */
//    public function getStreet()
//    {
//        return $this->street;
//    }
//
//    /**
//     * @param Street $street
//     *
//     * @return $this
//     */
//    public function setStreet($street)
//    {
//        $this->street = $street;
//
//        return $this;
//    }

    /**
     * @return CommonObject
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param CommonObject $object
     *
     * @return $this
     */
    public function setObject(CommonObject $object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return ArrayCollection|SearchesAd
     */
    public function getSearchesAds()
    {
        return $this->searchesAds;
    }

    /**
     * @param $searchesAds
     *
     * @return $this
     */
    public function setSearchesAds($searchesAds)
    {
        $this->searchesAds = $searchesAds;

        return $this;
    }

    /**
     * @param SearchesAd $searchesAd
     *
     * @return $this
     */
    public function addSearchesAd(SearchesAd $searchesAd)
    {
        $this->searchesAds->add($searchesAd);

        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = ($this->status | $status);

        return $this;
    }

    public function unsetStatus($status)
    {
        $this->status = ($this->status & ~$status);

        return $this;
    }

    public function checkStatusesAny(array $bits)
    {
        $result = false;
        foreach ($bits as $bit) {
            $result = $result || ((bool)((int)$this->status & (int)$bit));
            if ($result) {
                break;
            }
        }

        return $result;
    }

    public function isInitial()
    {
        return $this->checkStatus(self::STATUS_INITIAL);
    }

    /**
     * @param $bit
     *
     * @return bool
     */
    public function checkStatus($bit)
    {
        return (bool)((int)$this->status & (int)$bit);
    }

    public function isInactive()
    {
        return $this->checkStatus(self::STATUS_INACTIVE);
    }


}

