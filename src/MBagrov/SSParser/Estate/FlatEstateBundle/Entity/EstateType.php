<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;

/**
 * EstateType
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="EstateTypes", schema="flat_estate", uniqueConstraints={@ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})})
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\EstateTypeRepository")
 */
class EstateType
{

    use Uuidable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ru", type="string", length=45, nullable=true)
     */
    private $nameRu;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="trans_const", length=100, unique=false, nullable=true)
     */
    private $transConst;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="status", unique=false, nullable=false)
     */
    private $status;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * @param string $nameRu
     *
     * @return $this
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransConst()
    {
        return $this->transConst;
    }

    /**
     * @param string $transConst
     *
     * @return $this
     */
    public function setTransConst($transConst)
    {
        $this->transConst = $transConst;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function __toString()
    {
        return $this->transConst;
    }


}

