<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;

/**
 * SearchesAd
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="SearchesAds", schema="flat_estate",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})
 *     },
 *     indexes={
 *          @ORM\Index(name="fk_SearchesAds_1_idx", columns={"Searches_id"}),
 *          @ORM\Index(name="fk_SearchesAds_2_idx", columns={"Ads_id"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\SearchesAdRepository")
 */
class SearchesAd
{

    use  Uuidable;

    const STATUS_DEFAULT = 0;
    const STATUS_HIDDEN = 1;
    const STATUS_STARRED = 2;
    const STATUS_WILL_CONTACT_SELLER = 4;
    const STATUS_SELLER_CONTACTED = 8;
    const STATUS_WILL_GO_VIEW = 16;
    const STATUS_VIEW_LIKED = 32;
    const STATUS_VIEW_DISLIKED = 64;

    const STATUS_OPENED_MAIL = 128;
    const STATUS_OPENED_ON_SITE = 256;
    const STATUS_RECEIVED_SMS = 512;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var Ad
     *
     * @ORM\ManyToOne(targetEntity="Ad", inversedBy="searchesAds")
     * @ORM\JoinColumn(name="Ads_id", referencedColumnName="id")
     */
    private $ad;

    /**
     * @var Search
     *
     * @ORM\ManyToOne(targetEntity="Search", inversedBy="searchesAds")
     * @ORM\JoinColumn(name="Searches_id", referencedColumnName="id")
     */
    private $search;

    /**
     * @var ArrayCollection|Search
     *
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="searchesAd", fetch="EAGER")
     */
    private $notifications;

    public function __construct()
    {
        $this->notifications = new ArrayCollection();
        $this->status = self::STATUS_DEFAULT;
    }

    /**
     * @return Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @param $ad
     *
     * @return $this
     */
    public function setAd(Ad $ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * @return Search
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param $search
     *
     * @return $this
     */
    public function setSearch(Search $search)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * @return ArrayCollection|Notification
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param $notifications
     *
     * @return $this
     */
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;

        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = ($this->status | $status);

        return $this;
    }

    public function unsetStatus($status)
    {
        $this->status = ($this->status & ~$status);

        return $this;
    }

    public function isHidden()
    {
        return $this->checkStatus(self::STATUS_HIDDEN);
    }

    /**
     * @param $bit
     *
     * @return bool
     */
    public function checkStatus($bit)
    {
        return (bool)((int)$this->status & (int)$bit);
    }

    /**
     * @return bool
     */
    public function isStarred()
    {
        return $this->checkStatus(self::STATUS_STARRED);
    }

    /**
     * @return bool
     */
    public function isWillContactSeller()
    {
        return $this->checkStatus(self::STATUS_WILL_CONTACT_SELLER);
    }

    /**
     * @return bool
     */
    public function isSellerContacted()
    {
        return $this->checkStatus(self::STATUS_SELLER_CONTACTED);
    }

    /**
     * @return bool
     */
    public function isWillGoView()
    {
        return $this->checkStatus(self::STATUS_WILL_GO_VIEW);
    }

    /**
     * @return bool
     */
    public function isViewLiked()
    {
        return $this->checkStatus(self::STATUS_VIEW_LIKED);
    }

    /**
     * @return bool
     */
    public function isViewDisliked()
    {
        return $this->checkStatus(self::STATUS_VIEW_DISLIKED);
    }

    /**
     * @return bool
     */
    public function isOpenedEmail()
    {
        return $this->checkStatus(self::STATUS_OPENED_MAIL);
    }

    /**
     * @return bool
     */
    public function isOpenedOnSite()
    {
        return $this->checkStatus(self::STATUS_OPENED_ON_SITE);
    }

    /**
     * @return bool
     */
    public function isSmsReceived()
    {
        return $this->checkStatus(self::STATUS_RECEIVED_SMS);
    }

    /**
     * @return bool
     */
    public function isViewed()
    {
        return $this->checkStatusesAny([
            self::STATUS_OPENED_MAIL,
            self::STATUS_OPENED_ON_SITE,
            self::STATUS_RECEIVED_SMS,
        ]);
    }

    public function checkStatusesAny(array $bits)
    {
        $result = false;
        foreach ($bits as $bit) {
            $result = $result || ((bool)((int)$this->status & (int)$bit));
            if ($result) {
                break;
            }
        }

        return $result;
    }

}