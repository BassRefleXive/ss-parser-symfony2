<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Notifiable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;

/**
 * Notification
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Notifications",
 *      schema="flat_estate",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"}),
 *          @ORM\UniqueConstraint(name="sms_id_UNIQUE", columns={"sms_id"})
 *      },
 *      indexes={
 *          @ORM\Index(name="fk_Notifications_1_idx", columns={"SearchesAds_id"})
 *      }
 *  )
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\NotificationRepository")
 */
class Notification
{

    use Timestampable, Uuidable, Notifiable;

    const TYPE_SMS = 0;
    const TYPE_EMAIL = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=true)
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255, nullable=true)
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="meta", type="string", nullable=true)
     */
    private $meta;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_id", type="string", nullable=true)
     */
    private $smsId;

    /**
     * @var SearchesAd
     *
     * @ORM\ManyToOne(targetEntity="SearchesAd", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumn(name="SearchesAds_id", referencedColumnName="id")
     */
    private $searchesAd;


    public function __construct()
    {
        $this->status = new NotificationStatus();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     *
     * @return $this
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param string $meta
     *
     * @return $this
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    public function getSmsId()
    {
        return $this->smsId;
    }

    public function setSmsId($smsId): Notification
    {
        $this->smsId = $smsId;

        return $this;
    }

    /**
     * @return SearchesAd
     */
    public function getSearchesAd()
    {
        return $this->searchesAd;
    }

    /**
     * @param SearchesAd $searchesAd
     *
     * @return $this
     */
    public function setSearchesAd(SearchesAd $searchesAd)
    {
        $this->searchesAd = $searchesAd;

        return $this;
    }

}

