<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Serializable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Timestampable;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Search
 *
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @ORM\Table(name="Searches", schema="flat_estate",uniqueConstraints={@ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})},indexes={@ORM\Index(name="fk_Searches_1_idx", columns={"User_id"})})
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\SearchRepository")
 */
class Search
{

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    use Serializable, Timestampable, Uuidable;

    /**
     * @var integer
     *
     * @JMS\Expose
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @JMS\Expose
     * @ORM\Column(name="floor_min", type="integer", nullable=false)
     */
    private $floorMin;

    /**
     * @var integer
     *
     * @JMS\Expose
     * @ORM\Column(name="floor_max", type="integer", nullable=false)
     */
    private $floorMax;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="price_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $priceMin;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="price_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $priceMax;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="rooms_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $roomsMin = 2;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="rooms_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $roomsMax = 3;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="area_min", type="float", precision=10, scale=0, nullable=false)
     */
    private $areaMin;

    /**
     * @var float
     *
     * @JMS\Expose
     * @ORM\Column(name="area_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $areaMax;

    /**
     * @var integer
     *
     * @JMS\Expose
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @JMS\Expose
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type = '0';

    /**
     * @var boolean
     *
     * @JMS\Expose
     * @ORM\Column(name="is_sms", type="integer", nullable=false)
     */
    private $isSms;

    /**
     * @var boolean
     *
     * @JMS\Expose
     * @ORM\Column(name="is_email", type="integer", nullable=false)
     */
    private $isEmail;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\User", inversedBy="flatEstateSearches")
     * @ORM\JoinColumn(name="User_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var ArrayCollection|EstateType
     *
     * @JMS\Expose
     * @ORM\ManyToMany(targetEntity="EstateType")
     * @ORM\JoinTable(name="flat_estate.SearchesEstateTypes",
     *      joinColumns={@ORM\JoinColumn(name="Searches_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="EstateTypes_id", referencedColumnName="id")}
     *      )
     */
    private $estateTypes;

    /**
     * @var ArrayCollection|Object
     *
     * @JMS\Expose
     * @ORM\ManyToMany(targetEntity="MBagrov\SSParser\Common\CommonBundle\Entity\Object")
     * @ORM\JoinTable(name="flat_estate.SearchesObjects",
     *      joinColumns={@ORM\JoinColumn(name="Searches_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="Objects_id", referencedColumnName="id")}
     *      )
     */
    private $objects;

    /**
     * @var ParseObject
     *
     * @JMS\Expose
     * @ORM\OneToOne(targetEntity="ParseObject")
     * @ORM\JoinColumn(name="ParseObjects_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var ArrayCollection|Search
     *
     * @JMS\Expose
     * @ORM\OneToMany(targetEntity="SearchesAd", mappedBy="search", cascade={"persist"})
     */
    private $searchesAds;

    public function __construct()
    {
        $this->floorMin = 2;
        $this->floorMax = 8;
        $this->roomsMin = 2;
        $this->roomsMax = 3;
        $this->areaMin = 30;
        $this->areaMax = 80;
        $this->isEmail = true;
        $this->isSms = true;
        $this->status = true;
        $this->estateTypes = new ArrayCollection();
        $this->objects = new ArrayCollection();
        $this->searchesAds = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|EstateType
     */
    public function getEstateTypes()
    {
        return $this->estateTypes;
    }

    /**
     * @param $estateTypes
     *
     * @return $this
     */
    public function setEstateTypes($estateTypes)
    {
        $this->estateTypes = $estateTypes;

        return $this;
    }

    /**
     * @return ArrayCollection|Object
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * @param $objects
     *
     * @return $this
     */
    public function setObjects($objects)
    {
        $this->objects = $objects;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSearchesAds()
    {
        return $this->searchesAds;
    }

    /**
     * @param $searchesAds
     *
     * @return $this
     */
    public function setSearchesAds($searchesAds)
    {
        $this->searchesAds = $searchesAds;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getFloorMin()
    {
        return $this->floorMin;
    }

    /**
     * @param int $floorMin
     *
     * @return $this
     */
    public function setFloorMin($floorMin)
    {
        $this->floorMin = $floorMin;

        return $this;
    }

    /**
     * @return int
     */
    public function getFloorMax()
    {
        return $this->floorMax;
    }

    /**
     * @param int $floorMax
     *
     * @return $this
     */
    public function setFloorMax($floorMax)
    {
        $this->floorMax = $floorMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMin()
    {
        return $this->priceMin;
    }

    /**
     * @param float $priceMin
     *
     * @return $this
     */
    public function setPriceMin($priceMin)
    {
        $this->priceMin = $priceMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMax()
    {
        return $this->priceMax;
    }

    /**
     * @param float $priceMax
     *
     * @return $this
     */
    public function setPriceMax($priceMax)
    {
        $this->priceMax = $priceMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getRoomsMin()
    {
        return $this->roomsMin;
    }

    /**
     * @param float $roomsMin
     *
     * @return $this
     */
    public function setRoomsMin($roomsMin)
    {
        $this->roomsMin = $roomsMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getRoomsMax()
    {
        return $this->roomsMax;
    }

    /**
     * @param float $roomsMax
     *
     * @return $this
     */
    public function setRoomsMax($roomsMax)
    {
        $this->roomsMax = $roomsMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getAreaMin()
    {
        return $this->areaMin;
    }

    /**
     * @param float $areaMin
     *
     * @return $this
     */
    public function setAreaMin($areaMin)
    {
        $this->areaMin = $areaMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getAreaMax()
    {
        return $this->areaMax;
    }

    /**
     * @param float $areaMax
     *
     * @return $this
     */
    public function setAreaMax($areaMax)
    {
        $this->areaMax = $areaMax;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return (bool)$this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSms()
    {
        return (bool)$this->isSms;
    }

    /**
     * @param boolean $isSms
     *
     * @return $this
     */
    public function setIsSms($isSms)
    {
        $this->isSms = $isSms;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsEmail()
    {
        return (bool)$this->isEmail;
    }

    /**
     * @param boolean $isEmail
     *
     * @return $this
     */
    public function setIsEmail($isEmail)
    {
        $this->isEmail = $isEmail;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return ParseObject
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param ParseObject $city
     *
     * @return $this
     */
    public function setCity(ParseObject $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param SearchesAd $searchesAd
     *
     * @return $this
     */
    public function addSearchesAd(SearchesAd $searchesAd)
    {
        $this->searchesAds->add($searchesAd);

        return $this;
    }

    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getPriceMax() < $this->getPriceMin()) {
            $context->buildViolation('flat_estate.building.floors.max.lessThanMin')
                ->atPath('priceMax')
                ->addViolation();
            $context->buildViolation('flat_estate.building.floors.min.biggerThanMax')
                ->atPath('priceMin')
                ->addViolation();
        }
        if ($this->getFloorMax() < $this->getFloorMin()) {
            $context->buildViolation('flat_estate.building.floors.min.biggerThanMax')
                ->atPath('floorMax')
                ->addViolation();
            $context->buildViolation('flat_estate.building.floors.max.lessThanMin')
                ->atPath('floorMin')
                ->addViolation();
        }
        if ($this->getAreaMax() < $this->getAreaMin()) {
            $context->buildViolation('flat_estate.flat_config.rooms.max.lessThanMin')
                ->atPath('areaMax')
                ->addViolation();
            $context->buildViolation('flat_estate.flat_config.rooms.min.biggerThanMax')
                ->atPath('areaMin')
                ->addViolation();
        }
        if ($this->getRoomsMax() < $this->getRoomsMin()) {
            $context->buildViolation('flat_estate.flat_config.rooms.max.lessThanMin')
                ->atPath('roomsMax')
                ->addViolation();
            $context->buildViolation('flat_estate.flat_config.rooms.min.biggerThanMax')
                ->atPath('roomsMin')
                ->addViolation();
        }
    }

}

