<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;
use MBagrov\SSParser\Common\CommonBundle\Entity\Traits\Uuidable;

/**
 * ParseObject
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="ParseObjects", schema="flat_estate", uniqueConstraints={@ORM\UniqueConstraint(name="uuid_unique_idx", columns={"uuid"})}, indexes={@ORM\Index(name="fk_ParseObjects_1_idx", columns={"Objects_id"})})
 * @ORM\Entity(repositoryClass="MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\ParseObjectRepository")
 */
class ParseObject
{

    use Uuidable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var CommonObject
     *
     * @ORM\OneToOne(targetEntity="\MBagrov\SSParser\Common\CommonBundle\Entity\Object", inversedBy="parseObject")
     * @ORM\JoinColumn(name="Objects_id", referencedColumnName="id")
     */
    private $object;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return CommonObject
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param CommonObject $object
     *
     * @return $this
     */
    public function setObjects(CommonObject $object)
    {
        $this->object = $object;

        return $this;
    }

    public function __toString()
    {
        return $this->getObject()->getTransConst();
    }


}

