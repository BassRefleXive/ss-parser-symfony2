<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\GeocoderException;
use MBagrov\SSParser\Common\CommonBundle\Exceptions\UrlShortenerExteption;
use MBagrov\SSParser\Common\CommonBundle\Interfaces\AdInterface;
use MBagrov\SSParser\Common\CommonBundle\Services\CrawlerService;
use MBagrov\SSParser\Common\CommonBundle\Services\GeocoderService;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use MBagrov\SSParser\Common\CommonBundle\Services\SentryNotifierService;
use MBagrov\SSParser\Common\CommonBundle\Services\UrlShortenerService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\EstateType;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\ParserException;

class AdParserService extends CrawlerService
{

    const STATUS_AD_MUST_BE_PROCESSED = 0;
    const STATUS_AD_NOT_APPROPRIATE = 1;

    private $crawlerConfig;
    private $estateTypeService;
    private $objectService;
    private $geocoderService;
    private $notifierService;

    public function __construct(
        EstateTypeService $estateTypeService,
        ObjectService $objectService,
        GeocoderService $geocoderService,
        SentryNotifierService $notifierService,
        array $crawlerConfig,
        string $torEndpoint
    )
    {
        parent::__construct($torEndpoint);

        $this->crawlerConfig = $crawlerConfig;
        $this->estateTypeService = $estateTypeService;
        $this->objectService = $objectService;
        $this->geocoderService = $geocoderService;
        $this->notifierService = $notifierService;
    }

    public function parse($content, AdInterface $ad = null)
    {
        $this->crawler->clear();
        /**
         * @var Ad $ad
         */
        $this->crawler->add($content);
        $ad->setType($this->getType());

        if (in_array($ad->getType(), [Ad::TYPE_RENT, Ad::TYPE_SELL], true)) {
            $ad->setEstateType($this->getEstateType($ad));
            $ad->setRoomsCount($this->getRooms($ad));
            $ad->setArea($this->getArea($ad));
            $ad->setFloor($this->getFloor($ad));
            $this->parsePrice($ad);
            $this->parseLocation($ad);
            $ad->setText($this->getText($ad));
            $ad->setContentHash($this->getContentHash($ad));

            return self::STATUS_AD_MUST_BE_PROCESSED;
        } else {
            return self::STATUS_AD_NOT_APPROPRIATE;
        }
    }

    private function getType()
    {
        $header = $this->crawler->filterXPath('//h2');
        $type = Ad::TYPE_UNKNOWN;
        if ($header->count()) {
            $text = $header->first()->text();
            $matches = [];
            if (preg_match('/Продают|Сдают/', $text, $matches)) {
                if ($matches[0] === 'Продают') {
                    $type = Ad::TYPE_SELL;
                } elseif ($matches[0] === 'Сдают') {
                    $type = Ad::TYPE_RENT;
                }
            }
        }

        return $type;
    }

    private function getEstateType(Ad $ad)
    {
        $estateType = $this->crawler->filterXPath('//*[@id="tdo_6"]');
        if (!$estateType->count()) {
            $this->notifierService->error([
                'Description' => 'Estate type not found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No estate type found in html in ad with url: ' . $ad->getUrl());
        }
        $text = trim($estateType->first()->text());
        $estateType = $this->estateTypeService->findOneByName($text);
        if (!($estateType instanceof EstateType)) {
            $this->notifierService->error([
                'Description'      => 'No estate type found in html in ad.',
                'Estate type text' => $text,
                'Ad Url'           => $ad->getUrl(),
            ]);
            throw new ParserException('No estate type found in html in ad with url: ' . $ad->getUrl());
        }

        return $estateType;
    }

    private function getRooms(Ad $ad)
    {
        $rooms = $this->crawler->filterXPath('//*[@id="tdo_1"]');
        if (!$rooms->count()) {
            $this->notifierService->error([
                'Description' => 'Rooms count not found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No rooms found in html in ad with url: ' . $ad->getUrl());
        }

        return (int)$rooms->first()->text();
    }

    private function getArea(Ad $ad)
    {
        $area = $this->crawler->filterXPath('//*[@id="tdo_3"]');
        if (!$area->count()) {
            $this->notifierService->error([
                'Description' => 'Area not found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No area found in html in ad with url: ' . $ad->getUrl());
        }

        return (float)$area->first()->text();
    }

    private function getFloor(Ad $ad)
    {
        $floors = $this->crawler->filterXPath('//*[@id="tdo_4"]');
        if (!$floors->count()) {
            $this->notifierService->error([
                'Description' => 'Floor not found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No floors found in html in ad with url: ' . $ad->getUrl());
        }
        $floor = explode('/', $floors->first()->text());
        if (!count($floor)) {
            $this->notifierService->error([
                'Description' => 'Floor cannot split using "/" divider.',
                'Floor text'  => $floors->first()->text(),
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('Failed to split floors with "/" sign in ad with url: ' . $ad->getUrl());
        }

        return (int)$floor[0];
    }

    private function parsePrice(Ad $ad)
    {
        $price = $this->crawler->filterXPath('//*[@id="tdo_8"]');
        if (!$price->count()) {
            $this->notifierService->error([
                'Description' => 'No price found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No price found in html in ad with url: ' . $ad->getUrl());
        }
        $priceText = $price->first()->text();
        $matches = [];
        if (preg_match('/[0-9 ]+€/', $priceText, $matches)) {
            $priceParts = explode(' ', $matches[0]);
            array_pop($priceParts);
            $ad->setPrice((float)implode('', $priceParts));
        } else {
            $this->notifierService->error([
                'Description' => 'No price number found in ad',
                'Price text'  => $priceText,
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No price number found in ad with url: ' . $ad->getUrl());
        }

        $matches = [];
        if (preg_match('/мес.|день/', $priceText, $matches)) {
            if ($matches[0] === 'мес.') {
                $ad->setPriceType(Ad::PRICE_MONTHLY);
            } elseif ($matches[0] === 'день') {
                $ad->setPriceType(Ad::PRICE_DAILY);
            } else {
                $this->notifierService->error([
                    'Description' => 'Unknown price type',
                    'Price text'  => $priceText,
                    'Ad Url'      => $ad->getUrl(),
                ]);
                throw new ParserException('Unknown price type found in ad with url: ' . $ad->getUrl());
            }
        } else {
            $ad->setPriceType(Ad::PRICE_TOTAL);
        }

        $matches = [];
        if (preg_match('/[0-9\]+\.[0-9\ €]+\/м2/', $priceText, $matches)) {
            $priceParts = explode(' ', $matches[0]);
            array_pop($priceParts);
            $ad->setPricePerSquare((float)implode('', $priceParts));
        }

    }

    private function getCoordinates(string $city, string $street, string $country, bool $logZeroResults = false)
    {
        try {
            return $this->geocoderService->getCoordinates(
                $city,
                $street,
                $country,
                $logZeroResults
            );
        } catch (GeocoderException $e) {
            return false;
        }
    }

    private function parseLocation(Ad $ad)
    {
        $district = $this->getDistrict($ad);
        if ($district !== null) {
            $ad->setObject($district);
        }

        $street = $this->getStreet($ad);
        $country = 'Латвия';

        $coordinates = $this->getCoordinates($ad->getParseObject()->getObject()->getNameRu(), $street, $country);
        if (!$coordinates && $ad->getObject()) {
            $coordinates = $this->getCoordinates($ad->getObject()->getNameRu(), $street, $country);
        }
        if (!$coordinates && ($village = $this->getVillage())) {
            $coordinates = $this->getCoordinates($village, $street, $country, true);
        }
        if (!$coordinates) {
            $ad->setLat(0)->setLon(0);
        } else {
            $ad->setLat($coordinates['lat'])->setLon($coordinates['lng']);
        }
    }

    private function getVillage()
    {
        $village = $this->crawler->filterXPath('//*[@id="tdo_368"]');
        if (!$village->count()) {
            return null;
        }
        $text = trim($village->first()->text());

        return $text;
    }

    private function getDistrict(Ad $ad)
    {
        $district = $this->crawler->filterXPath('//*[@id="tdo_856"]');
        if (!$district->count()) {
            return null;
        }
        $text = trim($district->first()->text());
        $district = $this->objectService->findOneByName($text);
        if (!($district instanceof Object)) {
            $this->notifierService->error([
                'Description' => 'No district found in ad.',
                'District'    => $text,
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No district found in database in ad with url: ' . $ad->getUrl());
        }

        return $district;
    }

    private function getStreet(Ad $ad)
    {
        $street = $this->crawler->filterXPath('//*[@id="tdo_11"]');
        if (!$street->count()) {
            $this->notifierService->error([
                'Description' => 'No street found in ad.',
                'Street'      => $street,
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('No street found in html in ad with url: ' . $ad->getUrl());
        }
        $streetText = $street->first()->text();
        $matches = [];
        if (preg_match('/[Карта]/', $streetText, $matches)) {
            $streetParts = explode('[Карта]', $streetText);
            array_pop($streetParts);
            $streetText = implode(' ', $streetParts);
        }

        return trim($streetText);
    }

    private function getText(Ad $ad)
    {
        $text = $this->crawler->filterXPath('//*[@id="msg_div_msg"]');
        if (!$text->count()) {
            $this->notifierService->error([
                'Description' => 'No text found in ad.',
                'Ad Url'      => $ad->getUrl(),
            ]);
            throw new ParserException('Failed reading text in ad with url: ' . $ad->getUrl());
        }
        $text = trim($text->first()->text());
        $matches = [];
        $delimiter = 'Город:';
        if (preg_match('/' . $delimiter . '/', $text, $matches)) {
            $textParts = explode($delimiter, $text);
            array_pop($textParts);
            $text = implode(' ', $textParts);
        }

        return $text;
    }

    public function getContentHash(AdInterface $ad)
    {
        /**
         * @var Ad $ad
         */
        return md5(
            implode('', [
                $ad->getText(),
                $ad->getType(),
                $ad->getEstateType(),
                $ad->getRoomsCount(),
                $ad->getArea(),
                $ad->getFloor(),
                $ad->getObject() !== null
                    ? $ad->getObject()->getId()
                    : '',
                $ad->getParseObject()->getObject()->getId(),
            ])
        );
    }

}