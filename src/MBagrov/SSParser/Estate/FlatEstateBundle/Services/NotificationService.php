<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\Mappings\NotificationStatus;
use MBagrov\SSParser\Common\CommonBundle\Services\UserService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Constants\NotificationEvent;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateException;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateServiceException;
use MBagrov\SSParser\Mailer\MailerBundle\Services\MailerFactory;

class NotificationService
{

    private $notificationRepository;
    private $emailMailer;
    private $apiKey;
    private $userService;
    private $searchesAdsService;
    private $adService;
    private $SMSNotifier;

    public function __construct(
        EntityManager $em,
        MailerFactory $mailerFactory,
        UserService $userService,
        SearchesAdsService $searchesAdsService,
        AdService $adService,
        SMSNotifierService $SMSNotifier,
        string $apiKey
    )
    {
        $this->notificationRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:Notification');
        $this->emailMailer = $mailerFactory->getFlatEstateAdMailer();
        $this->userService = $userService;
        $this->searchesAdsService = $searchesAdsService;
        $this->adService = $adService;
        $this->SMSNotifier = $SMSNotifier;
        $this->apiKey = $apiKey;
    }

    public function notifyClient(Notification $notification)
    {

        $this->create($notification);

        switch ($notification->getType()) {
            case Notification::TYPE_EMAIL: {
                $result = $this->notifyViaEmail($notification);
                break;
            }
            case Notification::TYPE_SMS: {
                $result = $this->notifyViaSms($notification);
                break;
            }
            default: {
                $result = null;
            }
        }

        return $result;
    }

    public function create(Notification $notification)
    {
        $this->notificationRepository->create($notification);
    }

    protected function notifyViaEmail(Notification $notification)
    {
        $result = $this->emailMailer
            ->setNotification($notification)
            ->setAdHistory($this->adService->getSimilarAds($notification->getSearchesAd()->getAd()))
            ->send();

        return $this->updateNotificationResult($notification, $result);
    }

    protected function notifyViaSms(Notification $notification)
    {
        return $this->updateNotificationResult($notification, !!$this->SMSNotifier->send($notification));
    }

    protected function updateNotificationResult(Notification $notification, bool $result): Notification
    {
        $notification->setStatus(
            $notification->getStatus()->setStatus(
                $result
                    ? NotificationStatus::STATUS_INITIAL_SUCCESS
                    : NotificationStatus::STATUS_INITIAL_FAIL)
        );

        $this->notificationRepository->update($notification);

        return $notification;
    }

    public function processEmailWebHookRequest(array $requestData)
    {

        try {
            /**
             * @var Notification $notification
             */
            $notification = $this->notificationRepository->findByUuid($requestData['notificationId']);

            if ($notification === null) {
                throw new FlatEstateServiceException('Unknown notification id received from mailgun.', 400);
            }

            $event = $requestData['event'];
            $eventStatus = NotificationEvent::getEventStatus($event);

            $notificationStatus = $notification->getStatus();

            if ($notificationStatus->checkStatus($eventStatus)) {
                return true;
            }

            $notification->setStatus($notificationStatus->setStatus($eventStatus));
            $notification->setMeta($this->prepareWebHookRequestMeta($notification, $requestData['event'], $requestData));

            if ($eventStatus === NotificationStatus::STATUS_DELIVERED) {
                $this->userService->updateAvailableEmailNotificationsCount($notification->getSearchesAd()->getSearch()->getUser());
            }

            if ($eventStatus === NotificationStatus::STATUS_OPENED) {
                $searchesAd = $notification->getSearchesAd();
                $searchesAd->setStatus(SearchesAd::STATUS_OPENED_MAIL);
                $this->searchesAdsService->update($searchesAd);
            }

            $this->update($notification);

            return true;

        } catch (\Exception $e) {
            throw new FlatEstateException($e->getMessage(), $e->getCode());
        }


    }

    protected function prepareWebHookRequestMeta(Notification $notification, $event, $requestData)
    {

        $meta = json_decode($notification->getMeta(), true);

        if ($meta === null) {
            $meta = [];
        }

        $meta = array_merge($meta, [
            $event => $requestData,
        ]);

        return json_encode($meta);

    }

    public function update(Notification $notification)
    {
        $this->notificationRepository->update($notification);
    }

    public function processSmsWebHookRequest(array $requestData) {

        /**
         * @var Notification $notification
         */
        $notification = $this->notificationRepository->findOneBy([
            'smsId' => $requestData['smsid']
        ]);

        if ($notification === null) {
            throw new FlatEstateServiceException('Unknown sms id received.', 400);
        }

        $eventStatus = NotificationEvent::getEventStatus($requestData['status']);

        if ($notification->getStatus()->checkStatus($eventStatus)) {
            throw new FlatEstateServiceException('Status for event "' . $requestData['status'] . '" already assigned to notification.', 406);
        }

        $notification->setStatus($notification->getStatus()->setStatus($eventStatus));
        $notification->setMeta($this->prepareWebHookRequestMeta($notification, $requestData['status'], $requestData));

        if ($eventStatus === NotificationStatus::STATUS_DELIVERED) {
            $this->userService->updateAvailablePhoneNotificationsCount($notification->getSearchesAd()->getSearch()->getUser());
            $searchesAd = $notification->getSearchesAd();
            $searchesAd->setStatus(SearchesAd::STATUS_RECEIVED_SMS);
            $this->searchesAdsService->update($searchesAd);
        }

        $this->update($notification);

        return true;

    }

}