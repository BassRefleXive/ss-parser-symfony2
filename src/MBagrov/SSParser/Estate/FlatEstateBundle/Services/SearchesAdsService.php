<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SearchesAdsService
{
    private $searchesAdRepository;


    public function __construct(
        EntityManager $em,
        TwigEngine $twigEngine
    )
    {
        $this->searchesAdRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:SearchesAd');
        $this->twigEngine = $twigEngine;
    }

    public function dataTable(Search $search, $requestData)
    {
        /**
         * @var SearchesAd $searchesAd
         */
        $result = $this->searchesAdRepository->getForDataTable($search, $requestData);
        $searchesAds = $result['result'];

        $result = [
            'draw'            => $requestData['draw'],
            'recordsFiltered' => $result['total'],
            'recordsTotal'    => count($searchesAds),
            'data'            => []
        ];

        foreach ($searchesAds as $searchesAd) {

            $ad = $searchesAd->getAd();
            $search = $searchesAd->getSearch();

            $mapLinkHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/list/map_link.html.twig', [
                'ad' => $ad
            ]);

            $linkHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/list/link.html.twig', [
                'sAd' => $searchesAd,
            ]);

            $actionsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/list/actions.html.twig', [
                'sAd' => $searchesAd,
            ]);

            $typeHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/type.html.twig', [
                'type' => $ad->getType()
            ]);

            $districtHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/district.html.twig', [
                'district' => $ad->getObject() instanceof Object ? $ad->getObject()->getTransConst() : ''
            ]);

            $estateTypeHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/estate_type.html.twig', [
                'type' => $ad->getEstateType()->getTransConst()
            ]);

            $floorHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/floor.html.twig', [
                'floor' => $ad->getFloor()
            ]);

            $roomsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/rooms.html.twig', [
                'rooms' => $ad->getRoomsCount()
            ]);

            $areaHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/area.html.twig', [
                'area' => $ad->getArea()
            ]);

            $priceHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Ad/_partials/price.html.twig', [
                'price' => $ad->getPrice()
            ]);

            $result['data'][] = array_merge(
                [
                    $mapLinkHtml,
                    $linkHtml,
                    $actionsHtml,
                    $typeHtml,
                    $districtHtml,
                    $estateTypeHtml,
                    $floorHtml,
                    $roomsHtml,
                    $areaHtml,
                    $priceHtml,

                ],
                !$searchesAd->checkStatusesAny([SearchesAd::STATUS_OPENED_MAIL, SearchesAd::STATUS_OPENED_ON_SITE, SearchesAd::STATUS_RECEIVED_SMS])
                    ? ['DT_RowClass' => 'row-new']
                    : []
            );

        }

        return $result;

    }

    public function getAvailableCount(Search $search)
    {
        return $this->searchesAdRepository->getAvailableEntitiesCount($search);
    }

    public function updateStatus(SearchesAd $searchesAd, array $requestData)
    {
        if ($requestData['toggleStar']) {
            $searchesAd->isStarred() ? $searchesAd->unsetStatus(SearchesAd::STATUS_STARRED) : $searchesAd->setStatus(SearchesAd::STATUS_STARRED);
        }
        if ($requestData['toggleTrash']) {
            $searchesAd->isHidden() ? $searchesAd->unsetStatus(SearchesAd::STATUS_HIDDEN) : $searchesAd->setStatus(SearchesAd::STATUS_HIDDEN);
        }
        if ($requestData['toggleContactSeller']) {
            $searchesAd->isWillContactSeller() ? $searchesAd->unsetStatus(SearchesAd::STATUS_WILL_CONTACT_SELLER) : $searchesAd->setStatus(SearchesAd::STATUS_WILL_CONTACT_SELLER);
        }
        if ($requestData['toggleContactedSeller']) {
            $searchesAd->isSellerContacted() ? $searchesAd->unsetStatus(SearchesAd::STATUS_SELLER_CONTACTED) : $searchesAd->setStatus(SearchesAd::STATUS_SELLER_CONTACTED);
        }
        if ($requestData['toggleGoView']) {
            $searchesAd->isWillGoView() ? $searchesAd->unsetStatus(SearchesAd::STATUS_WILL_GO_VIEW) : $searchesAd->setStatus(SearchesAd::STATUS_WILL_GO_VIEW);
        }
        if ($requestData['toggleViewLiked']) {
            $searchesAd->isViewLiked() ? $searchesAd->unsetStatus(SearchesAd::STATUS_VIEW_LIKED) : $searchesAd->setStatus(SearchesAd::STATUS_VIEW_LIKED);
        }
        if ($requestData['toggleViewDisliked']) {
            $searchesAd->isViewDisliked() ? $searchesAd->unsetStatus(SearchesAd::STATUS_VIEW_DISLIKED) : $searchesAd->setStatus(SearchesAd::STATUS_VIEW_DISLIKED);
        }

        $this->searchesAdRepository->update($searchesAd);

        return [
            'starred'         => $searchesAd->isStarred(),
            'hidden'          => $searchesAd->isHidden(),
            'contactSeller'   => $searchesAd->isWillContactSeller(),
            'contactedSeller' => $searchesAd->isSellerContacted(),
            'goView'          => $searchesAd->isWillGoView(),
            'viewLiked'       => $searchesAd->isViewLiked(),
            'viewDisliked'    => $searchesAd->isViewDisliked(),
        ];
    }

    public function create(SearchesAd $searchesAd)
    {
        $this->searchesAdRepository->create($searchesAd);
    }

    public function update(SearchesAd $searchesAd)
    {
        $this->searchesAdRepository->update($searchesAd);
    }
}