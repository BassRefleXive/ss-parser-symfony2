<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SecurityService
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function isAllowed($entity, $throwException = false, User $user = null)
    {
        if ($user === null) {
            $user = $this->tokenStorage->getToken()->getUser();
        }

        $res = false;

        if ($entity instanceof Search) {
            $res = $this->isSearchBelongsToUser($entity, $user);
        }

        if ($entity instanceof SearchesAd) {
            $res = $this->isSearchBelongsToUser($entity->getSearch(), $user);
        }

        if (!$res && $throwException) {
            throw new AccessDeniedException('Search belongs to another user.');
        }

        return $res;

    }

    protected function isSearchBelongsToUser(Search $search, User $user)
    {
        return $search->getUser()->getId() === $user->getId();
    }

}