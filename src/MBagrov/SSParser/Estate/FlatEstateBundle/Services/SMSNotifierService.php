<?php


namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Services\SMSSenderService;
use MBagrov\SSParser\Common\CommonBundle\Utils\Transliterator;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateServiceException;
use Symfony\Component\Translation\TranslatorInterface;

class SMSNotifierService
{

    private $renderer;
    private $translator;
    private $SMSSenderService;

    public function __construct(\Twig_Environment $renderer, TranslatorInterface $translator, SMSSenderService $SMSSenderService)
    {
        $this->renderer = $renderer;
        $this->translator = $translator;
        $this->SMSSenderService = $SMSSenderService;
    }

    public function send(Notification $notification)
    {
        $text = $this->createText($notification->getSearchesAd()->getAd());

        $notification->setText($text);

        $result = $this->SMSSenderService->send($notification, $notification->getSearchesAd()->getSearch()->getUser());

        dump($result);
        if ($result && $result = $this->processResponse($result)) {
            dump($result);
            $notification->setSmsId($result->getSmsId());
        }

        return $result;

    }

    private function processResponse(string $response) {
        $matches = [];

        $result = false;

        if (preg_match('/phone:\s*(\+371[0-9]+).*status:\s*([A-z]+).*smsid:\s*([A-z0-9]+)/i', $response, $matches)) {
            $result = new class ($matches[2], $matches[3])
            {
                private $status;
                private $smsId;

                public function __construct($status, $smsId)
                {
                    $this->status = $status;
                    $this->smsId = $smsId;
                }

                public function getStatus(): string
                {
                    return $this->status;
                }

                public function getSmsId(): string
                {
                    return $this->smsId;
                }
            };
        }

        return $result;
    }

    private function createText(Ad $ad)
    {
        $locale = $this->translator->getLocale();

        if (!in_array($locale, [Transliterator::LGN_EN, Transliterator::LGN_CYR, Transliterator::LGN_LV])) {
            throw new FlatEstateServiceException('Unknown translator locale. Cannot transliterate SMS message.', 500);
        }

        $result = $this->renderer->render('@MBagrovSSParserEstateFlatEstate/Notification/sms_notification.html.twig', [
            'ad' => $ad,
        ]);

        $result = trim($result);
        $result = Transliterator::transliterate($result, $locale, Transliterator::LGN_EN);

        return $result;
    }

}