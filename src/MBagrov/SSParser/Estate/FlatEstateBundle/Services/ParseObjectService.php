<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use Doctrine\ORM\EntityManager;

class ParseObjectService
{

    private $parseObjectRepository;

    public function __construct(EntityManager $em)
    {
        $this->parseObjectRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:ParseObject');
    }

    public function findAll()
    {
        return $this->parseObjectRepository->findAll();
    }

    public function getChoices()
    {
        return $this->parseObjectRepository->getParseObjectChoices();
    }

    public function findOneById($id) {
        return $this->parseObjectRepository->findOneById($id);
    }

}