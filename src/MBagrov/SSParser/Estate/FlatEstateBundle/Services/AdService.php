<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;

use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use MBagrov\SSParser\Common\CommonBundle\Services\SentryNotifierService;
use MBagrov\SSParser\Common\CommonBundle\Services\UrlShortenerService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\AdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateServiceException;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\ParserException;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdService
{

    private $adRepository;
    private $twigEngine;
    private $adParserService;
    private $objectService;
    private $crawlerConfig;
    private $urlGenerator;
    private $urlShortenerService;
    private $notifierService;

    public function __construct(
        EntityManager $em,
        TwigEngine $twigEngine,
        AdParserService $adParserService,
        ObjectService $objectService,
        UrlGeneratorInterface $urlGenerator,
        UrlShortenerService $urlShortenerService,
        SentryNotifierService $notifierService,
        array $crawlerConfig
    )
    {
        $this->adRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:Ad');
        $this->twigEngine = $twigEngine;
        $this->adParserService = $adParserService;
        $this->objectService = $objectService;
        $this->crawlerConfig = $crawlerConfig;
        $this->urlGenerator = $urlGenerator;
        $this->urlShortenerService = $urlShortenerService;
        $this->notifierService = $notifierService;
    }

    public function getBySSUniqueIdList(array $ids)
    {
        return new AdCollection($this->adRepository->findBySSUniqueIdList($ids));
    }

    public function processNewAd(Ad $ad)
    {

        $content = $this->getAdContent($ad);

        try {
            return $this->adParserService->parse($content, $ad);
        } catch (ParserException $e) {
            $this->notifierService->captureException($e);

            return AdParserService::STATUS_AD_NOT_APPROPRIATE;
        }

    }

    protected function getAdContent(Ad $ad)
    {

        if ($this->crawlerConfig['estate']['flats']['demo']) {
            $fileName = $this->crawlerConfig['estate']['flats']['demo_data_dir'] . 'html/' . $ad->getSsUniqId() . '.html';
            if (file_exists($fileName)) {
                $content = file_get_contents($fileName);
            } else {
                $content = $this->adParserService->getResourceContents($ad->getUrl());
                file_put_contents($fileName, $content);
            }
        } else {
            $content = $this->adParserService->getResourceContents($ad->getUrl());
        }

        return $content;
    }

    public function processFakeHistoryRequest()
    {
        $ads = $this->adRepository->findLatestAdWithHistory();
        if (!$ads) {
            return false;
        }

        $ad = $ads[mt_rand(0, count($ads) - 1)];

        return [
            'ad'         => $ad,
            'similarAds' => $this->getSimilarAds($ad),
        ];
    }

    public function processHistoryRequest(string $url)
    {

        try {
            $ad = new Ad();

            $parsedUrl = parse_url($url);
            $pathParts = explode('/', $parsedUrl['path']);
            if ($pathParts[2] !== 'ru') {
                $pathParts[2] = 'ru';
            }

            $citySlug = $pathParts[5];

            $city = $this->objectService->findOneByPath($citySlug . '/');

            if (!($city instanceof Object)) {
                throw new FlatEstateServiceException('Unknown city. Cannot determine city by slug. Slug: ' . $citySlug, 400);
            }

            $ad->setUrl($parsedUrl['scheme'] . '://' . $parsedUrl['host'] . implode('/', $pathParts))
                ->setSsUniqId('temporary_dummy_ss_unique_id')
                ->setParseObject($city->getParseObject());

            $content = $this->getAdContent($ad);

            if ($this->adParserService->parse($content, $ad) === AdParserService::STATUS_AD_NOT_APPROPRIATE) {
                return false;
            }

            return [
                'ad'         => $ad,
                'similarAds' => $this->getSimilarAds($ad),
            ];

        } catch (\Exception $e) {
            return false;
        }
    }

    public function getSimilarAds(Ad $ad)
    {
        return $this->adRepository->findSimilarAds($ad);
    }

    public function processAdUrlRequest(Ad $ad)
    {
        try {

            $adToParse = clone $ad;

            $content = $this->getAdContent($adToParse);

            if ($this->adParserService->parse($content, $adToParse) === AdParserService::STATUS_AD_NOT_APPROPRIATE) {
                return false;
            }

            if ($ad->getContentHash() !== $adToParse->getContentHash()) {
                return false;
            }

        } catch (\Exception $e) {
            return false;
        }

        return true;

    }

    public function storeCollection(AdCollection $adCollection)
    {
        $this->adRepository->storeBatch($adCollection);
        $adCollection->setShortUrl($this->urlGenerator, $this->urlShortenerService);
        $this->adRepository->storeBatch($adCollection);
    }

    /**
     * @param int $id
     *
     * @return Ad
     */
    public function findOneById($id)
    {
        return $this->adRepository->findOneById($id);
    }

    /**
     * @param string $uuid
     *
     * @return Ad
     */
    public function findOneByUuid($uuid)
    {
        return $this->adRepository->findOneByUuid($uuid);
    }

    public function update(Ad $ad)
    {
        $this->adRepository->update($ad);
    }

}