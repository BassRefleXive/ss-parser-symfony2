<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use MBagrov\SSParser\Common\CommonBundle\Services\ObjectService;
use MBagrov\SSParser\Common\CommonBundle\Services\RssParserService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\AdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Collections\SearchesAdCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd;
use MBagrov\SSParser\Estate\FlatEstateBundle\Exceptions\FlatEstateServiceException;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object as CommonObject;

class SearchService
{

    private $searchRepository;
    private $tokenStorage;
    private $twigEngine;
    private $objectService;
    private $adService;
    private $rssParserService;
    private $searchesAdsService;
    private $crawlerConfig;

    public function __construct(
        EntityManager $em,
        TokenStorage $tokenStorage,
        TwigEngine $twigEngine,
        ObjectService $objectService,
        AdService $adService,
        RssParserService $rssParserService,
        SearchesAdsService $searchesAdsService,
        $crawlerConfig
    )
    {
        $this->searchRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:Search');
        $this->tokenStorage = $tokenStorage;
        $this->twigEngine = $twigEngine;
        $this->objectService = $objectService;
        $this->adService = $adService;
        $this->rssParserService = $rssParserService;
        $this->searchesAdsService = $searchesAdsService;
        $this->crawlerConfig = $crawlerConfig;
    }

    public function findOneById($id)
    {
        return $this->searchRepository->findOneById($id);
    }

    public function create(Search $search)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $search->setUser($user);
        $this->searchRepository->create($search);

        return $search;
    }

    public function update(Search $search)
    {
        $this->searchRepository->update($search);
    }

    public function findAll()
    {
        return $this->searchRepository->findAll();
    }

    public function dataTable($requestData)
    {
        /**
         * @var Search $search
         * @var User $currentUser
         */
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $searches = $this->searchRepository->getForDataTable($currentUser, $requestData);

        $result = [
            'draw'            => $requestData['draw'],
            'recordsFiltered' => $currentUser->getFlatEstateSearches()->count(),
            'recordsTotal'    => $currentUser->getFlatEstateSearches()->count(),
            'data'            => [],
        ];

        foreach ($searches as $search) {
            $statusHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/status_icon.html.twig', [
                'isSms'      => $search->getIsSms(),
                'isEmail'    => $search->getIsEmail(),
                'canBeSms'   => $search->getUser()->getPhoneNotifications() > 0,
                'canBeEmail' => $search->getUser()->getEmailNotifications() > 0,
                'status'     => $search->getStatus(),
            ]);
            $adsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/ads.html.twig', [
                'search' => $search,
                'count'  => $this->searchesAdsService->getAvailableCount($search),
            ]);
            $cityHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/city.html.twig', [
                'city' => $search->getCity()->getObject(),
            ]);
            $districtsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/districts.html.twig', [
                'districts' => $search->getObjects(),
            ]);
            $estateTypesHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/estate_types.html.twig', [
                'estateTypes' => $search->getEstateTypes(),
            ]);
            $floorsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/floors.html.twig', [
                'min' => $search->getFloorMin(),
                'max' => $search->getFloorMax(),
            ]);
            $roomsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/rooms.html.twig', [
                'min' => $search->getRoomsMin(),
                'max' => $search->getRoomsMax(),
            ]);
            $areaHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/area.html.twig', [
                'min' => $search->getAreaMin(),
                'max' => $search->getAreaMax(),
            ]);
            $priceHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/price.html.twig', [
                'min' => $search->getPriceMin(),
                'max' => $search->getPriceMax(),
            ]);
            $typeHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/type.html.twig', [
                'type' => $search->getType(),
            ]);
            $actionsHtml = $this->twigEngine->render('@MBagrovSSParserEstateFlatEstate/Search/_partials/list/actions.html.twig', [
                'search' => $search,
            ]);
            $result['data'][] = array_merge(
                [
                    $statusHtml,
                    $adsHtml,
                    $typeHtml,
                    $cityHtml,
                    $districtsHtml,
                    $estateTypesHtml,
                    $floorsHtml,
                    $roomsHtml,
                    $areaHtml,
                    $priceHtml,
                    $actionsHtml,

                ],
                !$search->getStatus()
                    ? ['DT_RowClass' => 'row-disabled']
                    : []
            );

        }

        return $result;

    }

    public function toggleNotification(Search $search, $type)
    {
        if (!in_array($type, [0, 1])) {
            throw new FlatEstateServiceException('Incorrect type.', 500);
        }
        $user = $this->tokenStorage->getToken()->getUser();
        if ($search->getUser() !== $user) {
            throw new FlatEstateServiceException('Cannot change search of another user.', 500);
        }
        switch ($type) {
            case 0: {
                $search->setIsSms(!$search->getIsSms());
                break;
            }
            case 1: {
                $search->setIsEmail(!$search->getIsEmail());
                break;
            }
        }
        $this->searchRepository->update($search);
    }

    /**
     * @param $uuid
     *
     * @return Search
     */
    public function findOneByUuid($uuid)
    {
        return $this->searchRepository->findByUuid($uuid);
    }

    public function findForParse()
    {
        $searches = $this->searchRepository->getForParse();

        return new SearchCollection($searches);
    }

    public function searchNewAds(SearchCollection $searches)
    {
        $cities = $this->objectService->getUniqueCities($searches);

        $result = new AdCollection();

        /**
         * @var CommonObject $city
         */
        foreach ($cities as $city) {
            $url = $this->objectService->buildUrl($city, ObjectService::URL_TYPE_RSS);

            if ($this->crawlerConfig['estate']['flats']['demo']) {
                $fileName = $this->crawlerConfig['estate']['flats']['demo_data_dir'] . 'rss/' . $city->getUuid() . '.xml';
                if (file_exists($fileName)) {
                    $rss = file_get_contents($fileName);
                } else {
                    $rss = $this->rssParserService->getResourceContents($url);
                    file_put_contents($fileName, $rss);
                }
            } else {
                $rss = $this->rssParserService->getResourceContents($url);
            }

            // Get all ads from rss
            $ads = $this->rssParserService->parse($rss)->setCity($city);
            // Get array of ss unique ids of those ads
            $ssUniqueIds = $ads->getSSUniqueIdArray();
            // Fetch from database all ads with these ids.
            $existingAds = $this->adService->getBySSUniqueIdList($ssUniqueIds);
            // Filter RSS ads with ads from database. In ads collection will be only ads, which are not present in database.
            $ads = $ads->filterUnknownAds($existingAds);

            $result = $result->merge($ads);
        }

        return $result;

    }

    public function addSearchesToAds(SearchCollection $searches, AdCollection $ads)
    {
        $searchesAds = new SearchesAdCollection();
        /**
         * @var Ad $ad
         */
        foreach ($ads as $ad) {
            /**
             * @var Search $search
             */
            foreach ($searches as $search) {
                if ($search->getType() !== Ad::TYPE_UNKNOWN && $search->getType() !== $ad->getType()) {
                    continue;
                }
                if ($search->getCity()->getId() !== $ad->getParseObject()->getId()) {
                    continue;
                }
                if ($search->getObjects()->count() && !$search->getObjects()->contains($ad->getObject())) {
                    continue;
                }
                if (!$search->getEstateTypes()->contains($ad->getEstateType())) {
                    continue;
                }
                if ($search->getFloorMin() > $ad->getFloor() || $search->getFloorMax() < $ad->getFloor()) {
                    continue;
                }
                if ($search->getPriceMin() > $ad->getPrice() || $search->getPriceMax() < $ad->getPrice()) {
                    continue;
                }
                if ($search->getAreaMin() > $ad->getArea() || $search->getAreaMax() < $ad->getArea()) {
                    continue;
                }
                if ($search->getRoomsMin() > $ad->getRoomsCount() || $search->getRoomsMax() < $ad->getRoomsCount()) {
                    continue;
                }
                $searchesAd = new SearchesAd();
                $searchesAd->setAd($ad)
                    ->setSearch($search);
                $searchesAds->add($searchesAd);
                $ad->addSearchesAd($searchesAd);
                $search->addSearchesAd($searchesAd);
            }
        }

        return $searchesAds;
    }

}