<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Services;


use Doctrine\ORM\EntityManager;

class EstateTypeService
{

    private $estateTypeRepository;

    public function __construct(EntityManager $em)
    {
        $this->estateTypeRepository = $em->getRepository('MBagrovSSParserEstateFlatEstateBundle:EstateType');
    }

    public function findAll()
    {
        return $this->estateTypeRepository->findAll();
    }

    public function findOneById($id)
    {
        return $this->estateTypeRepository->findOneById($id);
    }

    public function findOneByName($name)
    {
        return $this->estateTypeRepository->findOneBy([
            'nameRu' => $name
        ]);
    }

}