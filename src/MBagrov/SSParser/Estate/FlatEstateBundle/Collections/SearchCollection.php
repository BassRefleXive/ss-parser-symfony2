<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search;

class SearchCollection extends ArrayCollection
{
    public function filterForCrawler()
    {
        $filtered = [];
        return $this->filter(function(Search $search) use (&$filtered) {
            if (!in_array(($cityId = $search->getCity()->getId()), $filtered, true)) {
                $filtered[] = $cityId;
                return true;
            }
            return false;
        });
    }
}