<?php

namespace MBagrov\SSParser\Estate\FlatEstateBundle\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use MBagrov\SSParser\Common\CommonBundle\Entity\Object;
use MBagrov\SSParser\Common\CommonBundle\Services\UrlShortenerService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdCollection extends ArrayCollection
{

    public function filterUnknownAds(AdCollection $adCollection)
    {
        return $this->filter(function (Ad $ad) use ($adCollection) {
            foreach ($adCollection as $existingAd) {
                if ($ad->getSsUniqId() === $existingAd->getSsUniqId()) {
                    return false;
                }
            }

            return true;
        });
    }

    public function getSSUniqueIdArray()
    {
        $ids = [];
        $this->map(function (Ad $ad) use (&$ids) {
            $ids[] = $ad->getSsUniqId();
        });

        return $ids;
    }

    public function merge(AdCollection $adCollection)
    {
        $adCollection->forAll(function ($k, Ad $v) {
            return $this->add($v);
        });

        return $this;
    }

    public function setCity(Object $city)
    {
        $this->forAll(function ($k, Ad $v) use ($city) {
            $v->setParseObject($city->getParseObject());

            return true;
        });

        return $this;
    }

    public function setShortUrl(UrlGeneratorInterface $urlGenerator, UrlShortenerService $urlShortenerService)
    {
        $this->forAll(function ($k, Ad $v) use ($urlGenerator, $urlShortenerService) {
            /**
             * @ToDo integrate not google url shortener
             */
//            $v->setShortUrl(
//                $urlShortenerService->getShortUrl(
//                    $urlGenerator->generate(
//                        'flat_estate.ad.show',
//                        [
//                            'id' => $v->getUuid(),
//                        ],
//                        UrlGeneratorInterface::ABSOLUTE_URL
//                    )
//                )
//            );
            $v->setShortUrl(
                $urlGenerator->generate(
                    'flat_estate.ad.show',
                    [
                        'id' => $v->getUuid(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            );

            return true;
        });

        return $this;
    }

}