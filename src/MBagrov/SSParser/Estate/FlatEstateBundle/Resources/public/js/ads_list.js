var AdsList = (function () {

    var tableInitialized = false;
    var map;
    var markers;
    var markersObjects = {};
    var currentMarker = null;
    var redMarkerIcon;
    var blueMarkerIcon;
    var table = $('#ads_list');

    var dataTable = function () {
        if (table.length) {

            table.dataTable({
                "processing": true,
                "serverSide": true,
                "searching": true,
                "lengthChange": false,
                "pageLength": 10,
                "orderMulti": true,
                "ajax": Routing.generate('flat_estate.search.ads', {id: table.data('id')}),
                "info": false,
                "dom": 't<"row view-pager"<"col-sm-12"<"text-right"p>>>',
                "language": {
                    "processing": null,
                    "paginate": {
                        "previous": '',
                        "next": ''
                    }
                },
                "order": [],
                "columnDefs": [
                    {"orderable": false, "targets": 0},
                    {"orderable": false, "targets": 1},
                ]
            });

            table.on('draw.dt', function () {
                $('.search_ads_block').fadeIn();
                $('.loader').hide();
                tableInitialized = true;
                tooltips();
                redrawMarkers();
                bindMoveToMarkerAdHover();
                bindShowAddInfo(table);
                initActions();
            });

            table.api().columns().every( function () {
                var that = this;

                $('[data-filter-trigger]', this.footer()).on('click', function(ev) {
                    ev.preventDefault();
                    ev.stopPropagation();

                    const target = $(ev.currentTarget);

                    const type = target.data('type');
                    const id = target.data('id');

                    if (type === 'id') {

                        var ids = that.search().length ? JSON.parse(that.search()) : undefined;

                        if (!ids) {
                            that.search(JSON.stringify([id])).draw();
                            target.find('.fa-times-circle-o').css('opacity', 1);
                            target.find('.fa-check').css('opacity', 0);
                        } else {
                            if ($.inArray(id, ids) === -1) {
                                ids.push(id);
                                that.search(JSON.stringify(ids)).draw();
                                target.find('.fa-times-circle-o').css('opacity', 1);
                                target.find('.fa-check').css('opacity', 0);
                            } else {
                                ids.splice(ids.indexOf(id), 1);
                                that.search(JSON.stringify(ids)).draw();
                                target.find('.fa-times-circle-o').css('opacity', 0);
                                target.find('.fa-check').css('opacity', 1);
                            }
                        }

                    }

                    if (type === 'bit') {
                        var searchVal = that.search().length ? that.search() : 0;
                        var isUnsetStatus = checkStatus(searchVal, id)
                        searchVal = isUnsetStatus ? unsetStatus(searchVal, id) : setStatus(searchVal, id);

                        if (isUnsetStatus) {
                            target.find('.fa-times-circle-o').css('opacity', 1);
                            target.find('.fa-check').css('opacity', 0);
                        } else {
                            target.find('.fa-times-circle-o').css('opacity', 0);
                            target.find('.fa-check').css('opacity', 1);
                        }
                        that.search(searchVal).draw();
                    }

                });

            } );


            table.on('error.dt', 'none');
        }
    };

    var checkStatus = function(status, bit) {
        return !!(parseInt(status) & parseInt(bit));
    };

    var setStatus = function(status, bit) {
        return (status | bit);
    };

    var unsetStatus = function(status, bit) {
        return (status & ~bit);
    };

    var initActions = function () {
        $('[data-star-trigger]').on('click', function (ev) {
            ev.preventDefault();
            var target = $(ev.currentTarget);
            const id = target.data('id');
            $.ajax({
                url: Routing.generate('flat_estate.search.ad.edit', {id: id}),
                method: 'PUT',
                data: {
                    toggleStar: true,
                },
                success: function (data) {
                    if (data.hasOwnProperty('data') && data.data.hasOwnProperty('starred')) {
                        tableInitialized && table.api().ajax.reload(null, false);
                        if (data.data.starred) {
                            target.find('.fa-star-o').css('opacity', 0);
                            target.find('.fa-star').css('opacity', 1);
                            target.find('.unstar').css('opacity', 0);
                            target.find('.star').css('opacity', 1);
                        } else {
                            target.find('.fa-star-o').css('opacity', 1);
                            target.find('.fa-star').css('opacity', 0);
                            target.find('.unstar').css('opacity', 1);
                            target.find('.star').css('opacity', 0);
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
        $('[data-trash-trigger]').on('click', function (ev) {
            ev.preventDefault();

            var target = $(ev.currentTarget);
            const id = target.data('id');
            const searchId = target.data('searchid');
console.log(target, id, searchId)
            if (confirm(TranslationMessages.confirmAdHide)) {
                $.ajax({
                    url: Routing.generate('flat_estate.search.ad.edit', {id: id}),
                    method: 'PUT',
                    data: {
                        toggleTrash: true,
                    },
                    success: function (data) {
                        if (data.hasOwnProperty('data') && data.data.hasOwnProperty('hidden')) {
                            if (data.data.hidden) {
                                tableInitialized && table.api().ajax.reload(null, false);
                            }
                            if (!tableInitialized) {
                                alert(TranslationMessages.adHidden);
                                setTimeout(function () {
                                    window.location = Routing.generate('flat_estate.search.show', {id: searchId})
                                }, 1000);
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

        });
        $('[data-change-status-trigger]').on('click', function (ev) {
            ev.preventDefault();
            ev.stopPropagation();
            var target = $(ev.currentTarget);
            const id = target.data('id');
            const type = target.data('type');
            const types = {
                0: {
                    data: { toggleContactSeller: true },
                    result: 'contactSeller',
                },
                1: {
                    data: { toggleContactedSeller: true },
                    result: 'contactedSeller',
                },
                2: {
                    data: { toggleGoView: true },
                    result: 'goView',
                },
                3: {
                    data: { toggleViewLiked: true },
                    result: 'viewLiked',
                },
                4: {
                    data: { toggleViewDisliked: true },
                    result: 'viewDisliked',
                },
            };
            $.ajax({
                url: Routing.generate('flat_estate.search.ad.edit', {id: id}),
                method: 'PUT',
                data: types[type].data,
                success: function (data) {
                    if (data.hasOwnProperty('data') && data.data.hasOwnProperty(types[type].result)) {
                        if (data.data[types[type].result]) {
                            target.find('.fa-times-circle-o').css('opacity', 0);
                            target.find('.fa-check').css('opacity', 1);
                        } else {
                            target.find('.fa-times-circle-o').css('opacity', 1);
                            target.find('.fa-check').css('opacity', 0);
                        }
                    }
                },
                error: function (error) { console.log(error); }
            });
        });
    };

    var mapInit = function () {
        if ($('#map').length) {
            map = L.map('map').setView([56.9713958, 23.9887389], 11);
            markers = new L.FeatureGroup();
            var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
            var osm = new L.TileLayer(osmUrl, {
                minZoom: 8,
                maxZoom: 18,
                attribution: osmAttrib
            });
            map.addLayer(osm);
            map.doubleClickZoom.disable();

            redMarkerIcon = L.icon({
                iconUrl: '/img/marker-red.png',
                iconSize: [18, 32],
                iconAnchor: [18, 32]
            });

            blueMarkerIcon = L.icon({
                iconUrl: '/img/marker-blue.png',
                iconSize: [18, 32],
                iconAnchor: [18, 32],
            });
        }
    };

    var redrawMarkers = function () {
        var ads = table.find('.map_marker_link');
        var lat;
        var lon;
        var currentAd;
        markers.clearLayers();
        markersObjects = {};
        map.removeLayer(markers);
        if (ads.length) {
            $.each(ads, function (adIdx, ad) {
                currentAd = $(ad);
                lat = currentAd.data('lat');
                lon = currentAd.data('lon');
                if (lat != 0 && lon != 0) {
                    var adId = currentAd.data('id');
                    var marker = L.marker([parseFloat(lat), parseFloat(lon)], {icon: blueMarkerIcon});
                    marker.adId = adId;
                    markers.addLayer(marker);
                    markersObjects[adId] = {
                        marker: marker,
                        link: currentAd.parent().parent().find('.ad_link')
                    }
                }
            });
            map.fitBounds(markers.getBounds());
            map.addLayer(markers);


            markers.on('click', function (e) {
                showAdInfo(markersObjects[e.layer.adId].link);
            });
        }
    };

    var bindMoveToMarkerAdHover = function () {
        $('.map_marker_link').on('click', function (e) {
            if (currentMarker !== null) {
                currentMarker.setIcon(blueMarkerIcon);
            }
            var target = $(e.target);
            var lat = target.data('lat');
            var lon = target.data('lon');
            if (lat != 0 && lon != 0) {
                var marker = markersObjects[target.data('id')].marker;
                marker.setIcon(redMarkerIcon);
                currentMarker = marker;
                map.panTo(new L.LatLng(lat, lon))
            } else {
                $('#ad_info_modal_content').html('<div class="alert alert-danger fade in alert-dismissable">' +
                    '<strong>Oh snap!</strong> This ad flat location cannot be shown on map. Sorry.</div>');
                $('#modal-ad_info').modal('show');
            }
        });
    };

    var bindShowAddInfo = function (parentEl) {
        $('.ad_link', parentEl).on('click', function (e) {
            e.preventDefault();
            showAdInfo($(e.target));
        });
    };

    var showAdInfo = function (link) {
        $.ajax({
            url: link.attr('href'),
            method: 'GET',
            success: function (response) {
                $('#ad_info_modal_content').html(response);
                $('#modal-ad_info').modal('show');
                //alert(response);
            },
            error: function () {
                alert('Something went wrong while loading ad data.');
            }
        });
    };

    var tooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    return {
        initDataTable: function () {
            dataTable();
        },
        initMap: function () {
            mapInit();
        },
        initActions: function () {
            initActions();
        },
        initTooltips: function () {
            tooltips();
        },
    }

}());