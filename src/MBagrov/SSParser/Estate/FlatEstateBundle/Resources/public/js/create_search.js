var CreateFlatSearch = (function () {

    var tooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    var initDistrictsSelector = function (el) {
        if (el.find('option').length > 1) {
            el.closest('.hidden').removeClass('hidden');
        }
        el.select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: '100%',
            tags: true,
            closeOnSelect: false
        });
    };


    var rebuildDistrictsSelector = function (objectId) {
        var districtsSelectBlock = $('.district-selector-block');
        if (objectId > 0) {
            $.ajax({
                url: Routing.generate('flat_estate.search.city.districts', {searchId: 0, cityId: objectId}),
                method: 'GET',
                success: function (data) {
                    if (data.length) {
                        districtsSelectBlock.find('.input').html(data);
                        initDistrictsSelector(districtsSelectBlock.find('.input').find('select'));
                    } else {
                        districtsSelectBlock.find('.input').html('<select name="search[objects][]" multiple></select>');
                        initDistrictsSelector(districtsSelectBlock.find('.input').find('select'));
                        districtsSelectBlock.addClass('hidden');
                    }
                }
            });
        } else {
            districtsSelectBlock.addClass('hidden');
        }
    };

    var citySelector = function () {
        $('#search_city').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: '100%'
        }).on('change', function (e) {
            rebuildDistrictsSelector($(e.target).val());
        });
    };

    var buildingSeriesSelector = function () {
        $('#search_estateTypes').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: '100%',
            tags: true,
            closeOnSelect: false
        });
    };

    var floorsSelector = function () {
        var minFloor = $('#search_floorMin');
        var maxFloor = $('#search_floorMax');
        $('#floors_min').text(minFloor.val());
        $('#floors_max').text(maxFloor.val());
        $('#floor-selector-slider').slider({
            min: 1,
            max: 25,
            range: true,
            values: [minFloor.val(), maxFloor.val()],
            slide: function (event, ui) {
                $('#floors_min').text(ui.values[0]);
                $('#floors_max').text(ui.values[1]);
                minFloor.val(ui.values[0]);
                maxFloor.val(ui.values[1]);
            }
        });
    };

    var roomsSelector = function () {
        var minRooms = $('#search_roomsMin');
        var maxRooms = $('#search_roomsMax');
        $('#rooms_min').text(minRooms.val());
        $('#rooms_max').text(maxRooms.val());
        $('#rooms-selector-slider').slider({
            min: 1,
            max: 6,
            range: true,
            values: [minRooms.val(), maxRooms.val()],
            slide: function (event, ui) {
                $('#rooms_min').text(ui.values[0]);
                $('#rooms_max').text(ui.values[1]);
                minRooms.val(ui.values[0]);
                maxRooms.val(ui.values[1]);
            }
        });
    };

    var areaSelector = function () {
        var minArea = $('#search_areaMin');
        var maxArea = $('#search_areaMax');
        $('#area_min').text(minArea.val());
        $('#area_max').text(maxArea.val());
        $('#area-selector-slider').slider({
            min: 20,
            max: 300,
            range: true,
            values: [minArea.val(), maxArea.val()],
            slide: function (event, ui) {
                $('#area_min').text(ui.values[0]);
                $('#area_max').text(ui.values[1]);
                minArea.val(ui.values[0]);
                maxArea.val(ui.values[1]);
            }
        });
    };

    var validation = function () {

        $.validator.addMethod('greaterThan',
            function (value, element, param) {
                var $otherElement = $(param);
                return parseInt(value, 10) > parseInt($otherElement.val(), 10);
            }
        );

        $('.flat_estate_create_new_search').validate({

            rules: {
                'search[type]': {
                    required: true
                },
                'search[priceMin]': {
                    required: true,
                    min: 1,
                    number: true
                },
                'search[priceMax]': {
                    required: true,
                    min: 1,
                    number: true,
                    greaterThan: '#search_priceMin'
                },
                'search[city]': {
                    required: true
                },
                'search[estateTypes]': {
                    required: true
                },
                'search[floorMin]': {
                    min: 1,
                    max: 25,
                    required: true,
                    number: true
                },
                'search[floorMax]': {
                    min: 1,
                    max: 25,
                    required: true,
                    number: true
                },
                'search[roomsMin]': {
                    min: 1,
                    max: 6,
                    required: true,
                    number: true
                },
                'search[roomsMax]': {
                    min: 1,
                    max: 6,
                    required: true,
                    number: true
                },
                'search[areaMin]': {
                    min: 20,
                    max: 300,
                    required: true,
                    number: true
                },
                'search[areaMax]': {
                    min: 20,
                    max: 300,
                    required: true,
                    number: true
                }
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
    };

    return {
        initTooltips: function () {
            tooltips();
        },
        initSelect2: function () {
            citySelector();
            buildingSeriesSelector();
            initDistrictsSelector($('.district-selector-block').find('select'));
        },
        initSliders: function () {
            floorsSelector();
            roomsSelector();
            areaSelector();
        },
        initValidation: function () {
            validation();
        }
    }

}());