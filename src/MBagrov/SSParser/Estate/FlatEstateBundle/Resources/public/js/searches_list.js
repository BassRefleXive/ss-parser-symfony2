var SearchesList = (function () {

    var dataTable = function () {
        var table = $('#searches_list');
        //console.log('data table init');
        table.dataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "lengthChange": false,
            "pageLength": 10,
            "orderMulti": true,
            "ajax": Routing.generate('flat_estate.search.results'),
            "info": false,
            "dom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-right"p>>>',
            "language": {
                "processing": null,
                "paginate": {
                    "previous": '',
                    "next": ''
                }
            },
            "order": [],
            "columnDefs": [
                {"orderable": false, "targets": 10},
                {"width": "50px", "targets": 10},
                {"orderable": false, "targets": 0},
                {"width": "1%", "targets": 0},
                {"className": "text-center", "targets": 0}
            ]
        });
        table.on('init.dt', function () {
            $('.datatable_block').fadeIn();
            $('.loader').hide();
            toggleNotifications({
                type: 'table',
                table: table,
            });
        });
        table.on('draw.dt', function () {
            tooltips();
        });
        table.on('error.dt', 'none');


    };

    var tooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    var toggleNotifications = function (obj) {
        $(document).on('click', '.toggle-notifications', function (e) {
            e.preventDefault();

            $.ajax({
                method: 'PUT',
                url: $(e.target).attr('href'),
                success: function () {
                    if (obj.type === 'table') {
                        obj.table.api().ajax.reload(function () {

                        }, false);
                    } else if (obj.type === 'reload') {
                        window.location.reload();
                    }
                },
                error: function () {
                    alert('Something went wrong. Please try again later.');
                }
            });

        });
    };

    return {
        initDataTable: function () {
            dataTable();
        },
        initToggleNotifications: function () {
            toggleNotifications({
                type: 'reload',
            });
        },
    }

}());