<?php

namespace MBagrov\SSParser\Frontend\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchCategoriesController extends Controller
{

    public function indexAction()
    {
        return $this->render('MBagrovSSParserFrontendFrontendBundle:categories:index.html.twig');
    }

}