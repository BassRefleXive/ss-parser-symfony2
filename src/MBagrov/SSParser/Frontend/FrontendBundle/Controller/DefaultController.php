<?php

namespace MBagrov\SSParser\Frontend\FrontendBundle\Controller;

use Doctrine\ORM\EntityManager;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad;
use MBagrov\SSParser\Estate\FlatEstateBundle\Repositories\AdRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /**
         * @var EntityManager $em
         * @var Ad $ad
         * @var AdRepository $adRepository
         */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userRepository = $em->getRepository('\MBagrov\SSParser\Common\CommonBundle\Entity\User');
        $searchesRepository = $em->getRepository('\MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search');

        $adRepository = $em->getRepository('\MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad');

        dump($adRepository->sayHello());

        $flatEstateSearches = $userRepository->find(27)->getFlatEstateSearches();

//        dump($flatEstateSearches->first()->getSearchesAds()->first()->getAd()->first()->getObject());
//        dump($flatEstateSearches->first());
        $ad = $searchesRepository->find(4)->getSearchesAds()->first()->getAd();
//        dump($ad->getFloor());
        return $this->render('MBagrovSSParserFrontendFrontendBundle:Default:index.html.twig', array('name' => 'test'));
    }
}
