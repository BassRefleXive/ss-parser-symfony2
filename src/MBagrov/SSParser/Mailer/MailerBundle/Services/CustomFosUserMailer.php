<?php


namespace MBagrov\SSParser\Mailer\MailerBundle\Services;


use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;

class CustomFosUserMailer implements MailerInterface
{

    private $mailerFactory;

    public function __construct(MailerFactory $mailerFactory)
    {
        $this->mailerFactory = $mailerFactory;
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $mailer = $this->mailerFactory->getRegisterMailer();
        $mailer->setUser($user)->send();
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        $mailer = $this->mailerFactory->getResetPasswordMailer();
        $mailer->setUser($user)->send();
    }

}