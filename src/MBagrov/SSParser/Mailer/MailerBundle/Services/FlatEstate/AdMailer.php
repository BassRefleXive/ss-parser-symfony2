<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services\FlatEstate;


use Swift_Mailer;
use PhpCollection\Sequence;
use Symfony\Component\Translation\TranslatorInterface;
use MBagrov\SSParser\Mailer\MailerBundle\Services\MailerService;
use MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification;
use MBagrov\SSParser\Mailer\MailerBundle\Exceptions\ValueNotFoundException;

class AdMailer extends MailerService
{
    /** @var  Notification */
    private $notification;
    private $adHistory;

    public function __construct(Swift_Mailer $mailer, \Twig_Environment $renderer, TranslatorInterface $translator, array $config)
    {
        parent::__construct($mailer, $renderer, $translator, $config);
        $this->adHistory = [];
    }

    public function setNotification(Notification $notification): AdMailer
    {
        $this->notification = $notification;

        return $this;
    }

    public function setAdHistory(array $history): AdMailer
    {
        $this->adHistory = $history;

        return $this;
    }

    public function buildMessages(): Sequence
    {
        if (!$this->notification instanceof Notification) {
            throw new ValueNotFoundException('Notification must be set.', 500);
        }

        $searchesAd = $this->notification->getSearchesAd();
        $search = $searchesAd->getSearch();

        $message = \Swift_Message::newInstance();

        $message->getHeaders()->addTextHeader('X-Mailgun-Variables', json_encode([
            'notificationId' => $this->notification->getUuid(),
            'type'           => $this->notification->getType(),
        ]));

        $message->setSubject($this->translator->trans($this->config['subject'], [], 'flat_estate'))
            ->setFrom($this->config['from'])
            ->setTo($search->getUser()->getEmail())
            ->setBody($this->renderer->render('@MBagrovSSParserMailerMailer/email/flat_estate/ad.html.twig', [
                'message'    => $message,
                'contacts'   => $this->config['contacts'],
                'search'     => $search,
                'ad'         => $searchesAd->getAd(),
                'searchesAd' => $searchesAd,
                'toEmbed'    => $this->getCommonAssets(),
                'history'    => $this->adHistory,
            ]), 'text/html');

        return new Sequence([$message]);
    }

}