<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services;


use Mailgun\Mailgun;
use MBagrov\SSParser\Common\CommonBundle\Services\SentryNotifierService;
use Swift_Events_EventListener;
use Swift_Mime_Message;
use Swift_Transport;

class MailgunTransport implements Swift_Transport
{
    private $domain;
    /**
     * @var Mailgun
     */
    private $mailgun;

    private $notifierService;

    public function __construct($domain, $apiKey, SentryNotifierService $notifierService)
    {
        $this->domain = $domain;
        $this->mailgun = new Mailgun($apiKey);
        $this->notifierService = $notifierService;
    }

    /**
     * @inheritdoc
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        if (null === $message->getHeaders()->get('To')) {
            throw new \Swift_TransportException(
                'Cannot send message without a recipient'
            );
        }
        $postData = $this->prepareRecipients($message);
        $sendResult = $this->mailgun->sendMessage($this->domain, $postData, $message->toString());
        $result = true;
        if ($sendResult->http_response_code !== 200) {
            $result = false;
            $responseBody = '';
            foreach($sendResult->http_response_body->items as $errorItem) {
                $responseBody = sprintf('Message id: %s, message: %s', $errorItem->message_id, $errorItem->message);
            }
            $this->notifierService->error([
                'Message' => 'Mailer transport error. Mail send failed.',
                'statusCode' => $sendResult->http_response_code,
                'responseBody' => $responseBody,
            ]);
        }
        return $result;
    }

    protected function prepareRecipients(Swift_Mime_Message $message)
    {
        $headerNames = ['from', 'to', 'bcc', 'cc'];
        $messageHeaders = $message->getHeaders();
        $postData = [];
        foreach ($headerNames as $name) {
            /** @var \Swift_Mime_Headers_MailboxHeader $h */
            $h = $messageHeaders->get($name);
            $postData[$name] = $h === null ? [] : $h->getAddresses();
        }

        $postData['to'] = array_merge($postData['to'], $postData['bcc'], $postData['cc']);
        unset($postData['bcc']);
        unset($postData['cc']);

        $messageHeaders->removeAll('bcc');

        return $postData;
    }

    /**
     * @inheritdoc
     */
    public function registerPlugin(Swift_Events_EventListener $plugin)
    {
    }

    /**
     * Not used.
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * Not used.
     */
    public function start()
    {
    }

    /**
     * Not used.
     */
    public function stop()
    {
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     *
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

}