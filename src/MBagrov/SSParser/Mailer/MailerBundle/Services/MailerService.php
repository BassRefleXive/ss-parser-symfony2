<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services;


use PhpCollection\Sequence;
use Swift_Image;
use Swift_Mailer;
use Symfony\Component\Translation\TranslatorInterface;

abstract class MailerService
{
    protected $mailer;
    protected $renderer;
    protected $translator;
    protected $config;

    public function __construct(Swift_Mailer $mailer, \Twig_Environment $renderer, TranslatorInterface $translator, array $config)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->config = $config;
        $this->translator = $translator;

        $this->config['assets.common'] = __DIR__ . '/../Resources/assets/images/';
    }

    protected function getCommonAssets(): array
    {
        return [
            'headerLogo' => Swift_Image::fromPath($this->config['assets.common'] . 'logo-small.png'),
        ];
    }

    public final function send()
    {
        $messages = $this->buildMessages();
        return $this->sendMessages($messages);
    }

    private function sendMessages(Sequence $messages)
    {
        return !$messages->map(function (\Swift_Message $message) {
//            echo $message->getBody();
//            $message->setTo('mihails.bagrovs@gmail.com');
            return $this->mailer->send($message) > 0;
        })->contains(false);
    }


    abstract protected function buildMessages(): Sequence;
}