<?php


namespace MBagrov\SSParser\Mailer\MailerBundle\Services;

use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\DisabledSearchesMailer;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\NotificationsEndedMailer;
use Swift_Mailer;
use Symfony\Component\Translation\TranslatorInterface;
use MBagrov\SSParser\Mailer\MailerBundle\Services\FlatEstate\AdMailer;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\RegisterMailer;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\ResetPasswordMailer;
use MBagrov\SSParser\Mailer\MailerBundle\Services\Common\AdsIgnoredMailer;

/**
 * Class MailerFactory
 *
 * @package SuiteStory\Mailer\Services
 */
class MailerFactory
{

    private $mailersConfig;
    private $mailer;
    private $renderer;
    private $translator;

    public function __construct(Swift_Mailer $mailer, \Twig_Environment $renderer, TranslatorInterface $translator, array $mailersConfig)
    {
        $this->mailersConfig = $mailersConfig;
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->translator = $translator;
    }

    public function getFlatEstateAdMailer(): AdMailer
    {
        return new AdMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['flat_estate_ad']);
    }

    public function getAdsIgnoredMailer(): AdsIgnoredMailer
    {
        return new AdsIgnoredMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['ads_ignored']);
    }

    public function getNotificationsEndedMailer(): NotificationsEndedMailer
    {
        return new NotificationsEndedMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['notifications_ended']);
    }

    public function getDisabledSearchesMailer(): DisabledSearchesMailer
    {
        return new DisabledSearchesMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['disabled_searches']);
    }

    public function getRegisterMailer(): RegisterMailer
    {
        return new RegisterMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['register']);
    }

    public function getResetPasswordMailer(): ResetPasswordMailer
    {
        return new ResetPasswordMailer($this->mailer, $this->renderer, $this->translator, $this->mailersConfig['reset_password']);
    }

}