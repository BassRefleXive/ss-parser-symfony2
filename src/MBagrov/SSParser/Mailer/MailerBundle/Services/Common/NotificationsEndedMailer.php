<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services\Common;


use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\NotificationsEndedNotification;
use MBagrov\SSParser\Mailer\MailerBundle\Exceptions\ValueNotFoundException;
use MBagrov\SSParser\Mailer\MailerBundle\Services\MailerService;
use PhpCollection\Sequence;

class NotificationsEndedMailer extends MailerService
{
    private $notificationsEndedNotification;

    public function setNotificationsEndedNotification(NotificationsEndedNotification $notification): NotificationsEndedMailer
    {
        $this->notificationsEndedNotification = $notification;

        return $this;
    }

    protected function buildMessages(): Sequence
    {
        if (!$this->notificationsEndedNotification instanceof NotificationsEndedNotification) {
            throw new ValueNotFoundException('Ignored ad notification must be set.', 500);
        }

        $message = \Swift_Message::newInstance();

        $message->getHeaders()->addTextHeader('X-Mailgun-Variables', json_encode([
            'notificationId' => $this->notificationsEndedNotification->getUuid(),
            'type'           => NotificationTypeEnum::NOTIFICATION_TYPE_NOTIFICATIONS_ENDED,
        ]));

        $message->setSubject($this->translator->trans($this->config['subject'], [], 'common'))
            ->setFrom($this->config['from'])
            ->setTo($this->notificationsEndedNotification->getUser()->getEmail())
            ->setBody($this->renderer->render('@MBagrovSSParserMailerMailer/email/common/notifications_ended.html.twig', [
                'message'        => $message,
                'contacts'       => $this->config['contacts'],
                'toEmbed'        => $this->getCommonAssets(),
            ]), 'text/html');

        return new Sequence([$message]);
    }
}