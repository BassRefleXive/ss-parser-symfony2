<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services\Common;


use PhpCollection\Sequence;

class ResetPasswordMailer extends UserMailer
{
    public function buildMessages(): Sequence
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($this->translator->trans($this->config['subject'], [], 'common'))
            ->setFrom($this->config['from'])
            ->setTo($this->getUser()->getEmail())
            ->setBody($this->renderer->render('@MBagrovSSParserMailerMailer/email/common/reset_password.html.twig', [
                'message'  => $message,
                'contacts' => $this->config['contacts'],
                'user'     => $this->getUser(),
                'toEmbed'  => $this->getCommonAssets(),
            ]), 'text/html');

        return new Sequence([$message]);
    }

}