<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services\Common;


use FOS\UserBundle\Model\UserInterface;
use MBagrov\SSParser\Mailer\MailerBundle\Services\MailerService;
use MBagrov\SSParser\Mailer\MailerBundle\Exceptions\ValueNotFoundException;

abstract class UserMailer extends MailerService
{
    private $user;

    public function setUser(UserInterface $user): UserMailer
    {
        $this->user = $user;

        return $this;
    }

    protected function getUser(): UserInterface
    {
        if (!$this->user instanceof UserInterface) {
            throw new ValueNotFoundException('User must be set.', 500);
        }

        return $this->user;
    }

}