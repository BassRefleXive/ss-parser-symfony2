<?php

namespace MBagrov\SSParser\Mailer\MailerBundle\Services\Common;


use MBagrov\SSParser\Common\CommonBundle\Entity\Doctrine\Type\NotificationTypeEnum;
use MBagrov\SSParser\Common\CommonBundle\Entity\Notification\IgnoredAdsNotification;
use MBagrov\SSParser\Mailer\MailerBundle\Exceptions\ValueNotFoundException;
use MBagrov\SSParser\Mailer\MailerBundle\Services\MailerService;
use PhpCollection\Sequence;

class AdsIgnoredMailer extends MailerService
{
    private $ignoredAdsNotification;

    public function setIgnoredAdsNotification(IgnoredAdsNotification $notification): AdsIgnoredMailer
    {
        $this->ignoredAdsNotification = $notification;

        return $this;
    }

    protected function buildMessages(): Sequence
    {
        if (!$this->ignoredAdsNotification instanceof IgnoredAdsNotification) {
            throw new ValueNotFoundException('Ignored ad notification must be set.', 500);
        }

        $message = \Swift_Message::newInstance();

        $message->getHeaders()->addTextHeader('X-Mailgun-Variables', json_encode([
            'notificationId' => $this->ignoredAdsNotification->getUuid(),
            'type'           => NotificationTypeEnum::NOTIFICATION_TYPE_IGNORED_ADS,
        ]));

        $message->setSubject($this->translator->trans($this->config['subject'], [], 'common'))
            ->setFrom($this->config['from'])
            ->setTo($this->ignoredAdsNotification->getUser()->getEmail())
            ->setBody($this->renderer->render('@MBagrovSSParserMailerMailer/email/common/ads_ignored.html.twig', [
                'message'        => $message,
                'contacts'       => $this->config['contacts'],
                'adsCount'       => $this->ignoredAdsNotification->getTotalAds(),
                'missedAdsCount' => $this->ignoredAdsNotification->getIgnoredAds(),
                'missedPercent'  => $this->ignoredAdsNotification->getPercentDifference(),
                'toEmbed'        => $this->getCommonAssets(),
            ]), 'text/html');

        return new Sequence([$message]);
    }
}