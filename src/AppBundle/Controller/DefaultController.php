<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $pricingConfig = $this->getParameter('pricing_config');
        $regBonusConfig = $this->getParameter('registration');

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
            'minPrices' => [
                'sms' => $pricingConfig['sms']['price']['min'] * 100,
                'email' => $pricingConfig['email']['price']['min'] * 100,
            ],
            'regBonus' => [
                'sms' => $regBonusConfig['free_sms'],
                'email' => $regBonusConfig['free_email'],
            ]
        ]);
    }
}
