<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Locale;
use MBagrov\SSParser\Common\CommonBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LocalesController extends Controller
{
    public function setAction($locale, Request $request)
    {
        $localeObject = new Locale();
        $localeObject->setLocale($locale);
        $validator = $this->get('validator');
        $errors = $validator->validate($localeObject);
        if (count($errors) > 0) {
            return $this->render('locales/validation.html.twig', array(
                'errors' => $errors,
            ));
        }

        $request->getSession()->set('_locale', $locale);

        $user = $this->get('security.token_storage')->getToken()->getUser();
//        mihails.bagrovs@gmail.com

        if ($user instanceof User) {
            $user->setLocale($locale);
            $this->get('common.services.user')->update($user);
        }

        $referrer = $request->headers->get('referer');
        $redirectTo = $referrer === null ? $this->get('router')->generate('index') : $referrer;

        return $this->redirect($redirectTo);
    }
}