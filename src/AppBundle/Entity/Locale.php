<?php

namespace AppBundle\Entity;


/**
 * Class Locale
 * @package AppBundle\Entity
 */
class Locale
{
    /**
     * @var string
     */
    protected $locale;

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }
}