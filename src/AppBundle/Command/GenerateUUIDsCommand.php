<?php

namespace AppBundle\Command;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Rhumsaa\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class GenerateUUIDsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('generate:uuid')
            ->setDescription('Generate new UUID for database entities.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $availableEntitiesMap = [
                'Objects (Common)'            => 'MBagrov\SSParser\Common\CommonBundle\Entity\Object',
                'Searches (Flat estate)'      => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Search',
                'Ads (Flat estate)'           => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Ad',
                'EstateTypes (Flat estate)'   => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\EstateType',
                'Notifications (Flat estate)' => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\Notification',
                'ParseObjects (Flat estate)'  => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\ParseObject',
                'SearchesAds (Flat estate)'   => 'MBagrov\SSParser\Estate\FlatEstateBundle\Entity\SearchesAd',
            ];

            $helper = $this->getHelper('question');

            $question = new ChoiceQuestion(
                'Please select entity to generate UUIDs for. Or choose "All" to generate UUIDs for all entities.',
                array_merge(['All'], array_keys($availableEntitiesMap)),
                0
            );

            /**
             * @var EntityManager $em
             */
            $em = $this->getContainer()->get('doctrine')->getEntityManager();

            $question->setErrorMessage('Entity %s is invalid.');

            $entity = $helper->ask($input, $output, $question);

            $output->writeln('<question>You have just selected: ' . $entity . '</question>');

            $entitiesToUpdate = [];
            $stats = [];

            if ($entity === 'All') {
                foreach ($availableEntitiesMap as $entityType) {
                    /**
                     * @var EntityRepository $entityRepository
                     */
                    $entityRepository = $em->getRepository($entityType);

                    $allEntities = $entityRepository->findAll();
                    $entitiesToUpdate = array_merge($entitiesToUpdate, $allEntities);

                    $stats[$entityType] = count($allEntities);

                    $output->writeln('<comment>Will be updated ' . count($allEntities) . ' of ' . $entityType . ' entities.</comment>');
                }
            } else {
                /**
                 * @var EntityRepository $entityRepository
                 */
                $entityRepository = $em->getRepository($availableEntitiesMap[$entity]);

                $entitiesToUpdate = $entityRepository->findAll();

                $stats[$availableEntitiesMap[$entity]] = count($entitiesToUpdate);

                $output->writeln('<comment>Will be updated ' . count($entitiesToUpdate) . ' of ' . $availableEntitiesMap[$entity] . ' entities.</comment>');
            }


            $output->writeln('<comment>Total will be updated ' . count($entitiesToUpdate) . ' entities.</comment>');

            $progress = new ProgressBar($output, count($entitiesToUpdate));

            $progress->start();

            foreach ($entitiesToUpdate as $entity) {
                $this->genUUID($em, $entity);
                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');
            foreach ($stats as $entityName => $count) {
                $output->writeln('<info>Total ' . $count . ' ' . $entityName . ' entities updated.</info>');
            }

        } catch (\Exception $e) {
            $output->writeln('<error>An error occurred. Text: ' . $e->getMessage() . '</error>');
        }

    }

    protected function genUUID(EntityManager $em, $entity)
    {
        $entity->setUuid((string)Uuid::uuid4());
        $em->flush();
    }

}