SS Parser Frontend on Symfony2
==============================

Install:
--------

For php dependencies management composer is used.

#### Installing composer

```sh
$ curl -sS https://getcomposer.org/installer | php
$ mv composer.phar /usr/local/bin/composer
```

#### Installing dependencies

```sh
$ composer install
```

For frontend building ad dependency management bower and gulp are used.

#### Installing nodejs
```sh
$ apt-get install nodejs
$ ln -s /usr/bin/nodejs /usr/bin/node
```
#### Installing npm
```sh
$ apt-get install npm
$ npm install npm -g
```
#### Installing gulp
```sh
$ npm install --global gulp
```
#### Installing bower
```sh
$ npm install -g bower
```
#### Installing git (is required for bower)
```sh
$ apt-get install git
```
#### Go to project directory and execute
```sh
$ npm install --save-dev gulp
$ npm install --save-dev gulp-sass
$ npm install --save-dev gulp-copy
$ npm install --save-dev gulp-minify-css
$ npm install --save-dev gulp-concat
$ npm install --save-dev gulp-uglify
$ npm install --save-dev gulp-rename
$ npm install --save-dev gulp-concat-css
$ npm install --save-dev gulp-clean
$ npm install --save-dev gulp-obfuscate
```
#### To install libraries in the project directory execute
```sh
$ bower install --allow-root
```
#### To install collect all styles, scripts, fonts and images run
```sh
$ gulp install
```
#### Gulp watch also is supported. To watch styles and scripts changes run
```sh
$ gulp watch
```