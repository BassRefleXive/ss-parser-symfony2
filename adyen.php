<?php


$url = 'https://test.adyen.com/hpp/directory.shtml';

$hmacKey = "9A5D29FA27FA7AEA64267C1AC6AA5363C21BF17D75A158D61F910AC9B2DE3029";

$params = [
    'merchantReference' => 'Test_directory_lookup',
    'merchantAccount'   => 'PrivateCOM014',
    'countryCode'       => 'LV',
    'currencyCode'      => 'EUR',
    'paymentAmount'     => '2000',
    'sessionValidity'   => '2015-12-25T10:31:06Z',
    'skinCode'          => 'nvY3Cwv6',

];

$escapeval = function ($val) {
    return str_replace(':', '\\:', str_replace('\\', '\\\\', $val));
};

ksort($params, SORT_STRING);

$signData = implode(":", array_map($escapeval, array_merge(array_keys($params), array_values($params))));

$merchantSig = base64_encode(hash_hmac('sha256', $signData, pack("H*", $hmacKey), true));
$params['merchantSig'] = $merchantSig;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Adyen Payment</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body>
<form name="adyenForm" action="https://test.adyen.com/hpp/select.shtml" method="post">

    <?php
    foreach ($params as $key => $value){
        echo '        <input type="hidden" name="' .htmlspecialchars($key,   ENT_COMPAT | ENT_HTML401 ,'UTF-8').
            '" value="' .htmlspecialchars($value, ENT_COMPAT | ENT_HTML401 ,'UTF-8') . '" />' ."\n" ;
    }
    ?>
    <input type="submit" value="Submit" />
</form>
</body>
</html>

//$context = stream_context_create([
//    'http' => [
//        'method' => 'POST',
//        'header'=>'Content-Type: application/x-www-form-urlencoded',
//        'content' => http_build_query($params)
//    ]
//]);
//
//echo file_get_contents($url, null, $context);