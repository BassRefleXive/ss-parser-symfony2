#!/bin/bash
set -e

composer install
npm install natives@1.1.6
npm install --global bower
npm install --global gulp
npm install
bower install --allow-root
gulp install
chmod 764 app/console
chmod 777 -R app/cache
chmod 777 -R app/logs

service cron start

exec "$@"
